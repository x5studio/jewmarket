<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

/** @var CMain $APPLICATION */

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();

if (is_null($request->get('product_id'))) {
    return;
}

$APPLICATION->IncludeComponent(
    'xfive:oneClickBuy',
    '',
    [
        "MAX_ORDER_COUNT" => 10,
        "STATUS_ID" => "OC",
        "USER_GROUP_ID" => array(2, 5),
        "PAY_SYSTEM" => 1,
        "DELIVERY" => 3,
        'AJAX_MODE' => 'Y',  // режим AJAX
        'AJAX_OPTION_SHADOW' => 'N', // затемнять область
        'AJAX_OPTION_JUMP' => 'N', // скроллить страницу до компонента
        'OFFERS_CART_PROPERTIES' => \XFive\Main\Settings::getInstance()->get("OFFERS_CART_PROPERTIES")
    ]
);
