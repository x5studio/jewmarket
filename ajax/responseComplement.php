<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

/** @var CMain $APPLICATION */

use Bitrix\Main\Context;

$arParams=array();

CAjax::Init();
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'catalog.section');

global $theme,$ElementID;
$column = $theme->Option()->get('detail_column');

$ajax = false;
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']))
{
	$ajax = true;
}

if($ajax && isset($_REQUEST['ajax_add2basket']) && !empty($_REQUEST['ajax_add2basket']) && isset($_REQUEST['elementId']) && 0 < intval($_REQUEST['elementId']))
{
	$APPLICATION->RestartBuffer();


	$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "popup", array(
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"COLUMNS_LIST" => array(
			0 => "NAME",
			2 => "PRICE",
			3 => "QUANTITY",
			4 => "SUM",
			5 => "PROPS"
		),
		"AJAX_MODE" => "N",
		"ELEMENT_ID" => intval($_REQUEST['ELEMENT_ID']),
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
		"BASKET_URL" => "/personal/cart/",
		"HIDE_COUPON" => "N",
		"QUANTITY_FLOAT" => "N",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"TEMPLATE_THEME" => "site",
		"SET_TITLE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"OFFERS_PROPS" => \XFive\Main\Settings::getInstance()->get("OFFER_TREE_PROPS"),
	),
		$component
	);
	die;
}

$basketAction = (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION'] : array());

$ajax_element = false;

if($ajax && 'Y' == $_REQUEST['ajax_quickview'])
{
	$APPLICATION->RestartBuffer();
	$ajax_quickview = true;
}

ob_start();

$ElementID=$APPLICATION->IncludeComponent("bitrix:catalog.element", "catalog", Array(
	"IBLOCK_TYPE" => "1c_catalog",	// Тип инфоблока
	"IBLOCK_ID" => "10",	// Инфоблок
	"PROPERTY_CODE" => "",	// Свойства
	"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"SET_CANONICAL_URL" => "Y",	// Устанавливать канонический URL
	"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
	"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
	"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
	"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "Y",	// Устанавливать статус 404
	"PRICE_CODE" => \XFive\Main\Settings::getInstance()->get("CATALOG_PRICES"),	// Тип цены
	"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
	"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
	"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
	"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
	"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
	"PRODUCT_PROPERTIES" => "N",	// Характеристики товара
	"LINK_IBLOCK_TYPE" => "",	// Тип инфоблока, элементы которого связаны с текущим элементом
	"LINK_IBLOCK_ID" => "",	// ID инфоблока, элементы которого связаны с текущим элементом
	"LINK_PROPERTY_SID" => "",	// Свойство, в котором хранится связь
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL на страницу, где будет показан список связанных элементов
	"SHOW_DEACTIVATED" => "N",	// Показывать деактивированные товары
	"OFFERS_CART_PROPERTIES" => "",
	"OFFERS_FIELD_CODE" => "",
	"OFFERS_PROPERTY_CODE" => "",
	"OFFERS_SORT_FIELD" => "sort",
	"OFFERS_SORT_ORDER" => "asc",
	"OFFERS_SORT_FIELD2" => "sort",
	"OFFERS_SORT_ORDER2" => "asc",
	"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],	// ID элемента
	"ELEMENT_CODE" => "",	// Код элемента
	"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
	"SECTION_CODE" => "",	// Код раздела
	"CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
	"CURRENCY_ID" => "RUB",	// Валюта, в которую будут сконвертированы цены
	"HIDE_NOT_AVAILABLE" => "N",
	"USE_ELEMENT_COUNTER" => "Y",	// Использовать счетчик просмотров
	"ADD_PICT_PROP" => "MORE_PHOTO",	// Дополнительная картинка основного товара
	"LABEL_PROP" => "-",	// Свойство меток товара
	"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
	"OFFER_TREE_PROPS" => false,
	"PRODUCT_SUBSCRIPTION" => "N",	// Разрешить оповещения для отсутствующих товаров
	"SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки
	"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
	"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
	"MESS_BTN_BUY" => "В корзину",	// Текст кнопки "Купить"
	"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
	"MESS_BTN_ADDED_TO_BASKET" => "В корзине",
	"MESS_BTN_SUBSCRIBE" => false,	// Текст кнопки "Уведомить о поступлении"
	"MESS_BTN_COMPARE" => "Сравнение",
	"MESS_BTN_DETAIL" => "Подробнее",
	"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
	"USE_COMMENTS" => "Y",	// Включить отзывы о товаре
	"USE_REVIEW" => "Y",
	"FORUM_ID" => "1",
	"BLOG_USE" => "Y",	// Использовать комментарии
	"ADD_DETAIL_TO_SLIDER" => "Y",	// Добавлять детальную картинку в слайдер
	"DISPLAY_NAME" => "Y",	// Выводить название элемента
	"PROP_CODE_ARTNUMBER" => false,
	"FILTER_NAME" => "arrFilter",
	"PROP_CODE_DOCUMENTATIONS" => "",
	"USE_ZOOM" => "Y",
	"CAROUSEL_DOTS_VERTICAL" => "Y",
	"SKU_VIEW" => "LIST2",
	"PARAMS" => $arParams,
	"AJAX" => $ajax,
	"AJAX_QUICKVIEW" => $ajax_quickview,
	"USE_CATALOG_CREDIT" => "Y",
	"LINK_CATALOG_CREDIT" => "credit/",
	"DISPLAY_PROPERTIES_CHARACTERISTICS" => "",
	"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
	"ADD_TO_BASKET_ACTION" => "ADD" /*$basketAction*/,	// Показывать кнопки добавления в корзину и покупки
	"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
	"COMPARE_PATH" => "",
	"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
	"USE_VOTE_RATING" => "Y",	// Включить рейтинг товара
	"VOTE_DISPLAY_AS_RATING" => "rating",	// В качестве рейтинга показывать
	"USE_FAVORITES" => "Y",
	"USE_GIFTS_DETAIL" => "Y",	// Показывать блок "Подарки" в детальном просмотре
	"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",	// Показывать блок "Товары к подарку" в детальном просмотре
	"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки
	"GIFTS_SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
	"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",	// Количество элементов в блоке "Подарки" в строке в детальном просмотре
	"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",	// Скрыть заголовок "Подарки" в детальном просмотре
	"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",	// Текст метки "Подарка" в детальном просмотре
	"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",	// Текст заголовка "Подарки"
	"GIFTS_SHOW_NAME" => "Y",	// Показывать название
	"GIFTS_SHOW_IMAGE" => "Y",	// Показывать изображение
	"GIFTS_MESS_BTN_BUY" => "Выбрать",	// Текст кнопки "Выбрать"
	"ONECLICK_USE_MASK" => "Y",
	"ONECLICK_MASK_PHONE" => "+7 (999) 999-9999",
	"USE_ONECLICK" => "Y",
	"ONECLICK_USE_CAPTCHA" => "N",
	"ONECLICK_MESS_TITLE" => "Купить в один клик",
	"ONECLICK_OK_TEXT" => "Спасибо за заказ! В ближайшее время наш менеджер свяжется с вами.",
	"ONECLICK_INCLUDE_FIELDS" => array(
		0 => "NAME",
		1 => "EMAIL",
		2 => "PHONE",
		3 => "MESSAGE",
	),
	"ONECLICK_EMAIL_TO" => "sale@assfat.ru",
	"ONECLICK_REQUIRED_FIELDS" => array(
		0 => "NAME",
		1 => "PHONE",
	),
	"ONECLICK_EVENT_MESSAGE_ID" => "",
	"USE_STORE" => "Y",
	"STORE_PATH" => "/about/store/#store_id#",
	"MAIN_TITLE" => "Наличие на складах",
	"USE_MIN_AMOUNT" => "Y",
	"MIN_AMOUNT" => "Y",
	"STORES" => "",
	"SHOW_EMPTY_STORE" => "Y",
	"SHOW_GENERAL_STORE_INFORMATION" => "N",
	"USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"FIELDS" => array(
		0 => "PHONE",
		1 => "SCHEDULE",
		2 => "EMAIL",
		3 => "",
	),
	"SIGNED_PARAMS" => $signedParams
),
	false
);
$catalogElement = ob_get_contents();
ob_end_clean();


if($ajax_quickview) {
	echo $catalogElement;
	die;
}


echo $catalogElement;