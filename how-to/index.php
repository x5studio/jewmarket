<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Как узнать размер кольца");
?>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p>
                <strong>Вариант 1</strong><br>
                Возьмите карандаш, лист бумаги и&nbsp;обведите кольцо девушки по&nbsp;внутреннему контуру. Затем с&nbsp;помощью
                линейки измерьте полученный диаметр кольца.<br>
            </p>
        </div>
    </div>

    <div class="skazka-howto-ring-wrapper">
        <h2 class="size-txt">Размер:</h2>
        <div class="bx-wrapper" style="max-width: 1080px; margin: 0px auto;">
            <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 180px;">
                <ul class="skazka-howto-ring-slider"
                    style="width: 1415%; position: relative; transition-duration: 0s; transform: translate3d(-20px, 0px, 0px);">
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">15</div>
                        <a href="/catalog/rings/size-15/">
                            <img src="https://www.silverskazka.ru/upload/img/s/020469-01-02_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-15sm">Ø15 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">15.5</div>
                        <a href="/catalog/rings/size-15_5/">
                            <img src="https://www.silverskazka.ru/upload/img/s/022612-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-15_5sm">Ø15.5 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">16</div>
                        <a href="/catalog/rings/size-16/">
                            <img src="https://www.silverskazka.ru/upload/img/s/029425-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-16sm">Ø16 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">16.5</div>
                        <a href="/catalog/rings/size-16_5/">
                            <img src="https://www.silverskazka.ru/upload/img/s/028263-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-16_5sm">Ø16.5 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">17</div>
                        <a href="/catalog/rings/size-17/">
                            <img src="https://www.silverskazka.ru/upload/img/s/028262-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-17sm">Ø17 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">17.5</div>
                        <a href="/catalog/rings/size-17_5/">
                            <img src="https://www.silverskazka.ru/upload/img/s/018300-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-17_5sm">Ø17.5 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">18</div>
                        <a href="/catalog/rings/size-18/">
                            <img src="https://www.silverskazka.ru/upload/img/s/024182-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-18sm">Ø18 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">18.5</div>
                        <a href="/catalog/rings/size-18_5/">
                            <img src="https://www.silverskazka.ru/upload/img/s/020302-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-18_5sm">Ø18.5 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">19</div>
                        <a href="/catalog/rings/size-19/">
                            <img src="https://www.silverskazka.ru/upload/img/s/018238-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-19sm">Ø19 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">19.5</div>
                        <a href="/catalog/rings/size-19_5/">
                            <img src="https://www.silverskazka.ru/upload/img/s/029439-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-19_5sm">Ø19.5 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">20</div>
                        <a href="/catalog/rings/size-20/">
                            <img src="https://www.silverskazka.ru/upload/img/s/020985-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-20sm">Ø20 мм</div>
                    </li>
                    <li style="float: left; list-style: none; position: relative; width: 90px;">
                        <div class="ring-size">20.5</div>
                        <a href="/catalog/rings/?size=20.5-21">
                            <img src="https://www.silverskazka.ru/upload/img/s/016884-01-01_0.jpg" class="img-responsive center-block">
                        </a>
                        <div class="size-line size-20_5sm">Ø20.5 мм</div>
                    </li>
                </ul>
            </div>
            <div class="bx-controls bx-has-controls-direction">
                <div class="bx-controls-direction">
                    <a class="bx-prev disabled" href="">Prev</a>
                    <a class="bx-next disabled" href="">Next</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6">
            <p>
                <strong>Вариант 2</strong><br>
                Более оригинальный метод узнать размер пальца&nbsp;— это использовать измеритель ниже:<br>
                Скачайте это изображение к&nbsp;себе на&nbsp;компьютер, распечатайте на&nbsp;принтере и&nbsp;вырежьте
                его, как показано на&nbsp;схеме.
            </p>
            <p style="padding: 35px;">
                <a href="/img/howto/sizescale.png" target="_blank">
                    <img src="https://www.silverskazka.ru/img/howto/sizescale.png" alt="размер колеца" class="img-responsive center-block">
                </a>
            </p>
            <p>В&nbsp;итоге у&nbsp;вас должен получиться вот такой аппарат:</p>
            <p style="padding: 35px; padding-left: 100px;">
                <img src="https://www.silverskazka.ru/img/howto/yoursize.png" alt="размер колеца" class="img-responsive">
            </p>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">
            <p>
                <strong>Вариант 3</strong><br>
                Диаметр пальца можно измерить при помощи нитки или тонкой веревочки. После того, как вы определите
                размер пальца, сравните полученную длину нитки со значениями из таблицы типичных размеров колец:<br>
            </p>
            <table style="text-align:center;" class="table table-striped small-rows">
                <tbody>
                <tr>
                    <th style="text-align:center;width:150px;">Длина нитки, мм</th>
                    <th style="text-align:center;width:150px;">Размер кольца</th>
                </tr>
                <tr>
                    <td>47.12</td>
                    <td>
                        <a href="/catalog/rings/size-15/" class=" skazka-catalog-size- size-value-catalog">
                            15
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>48.69</td>
                    <td>
                        <a href="/catalog/rings/size-15_5/" class=" skazka-catalog-size- size-value-catalog">
                            15.5
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>50.27</td>
                    <td>
                        <a href="/catalog/rings/size-16/" class=" skazka-catalog-size- size-value-catalog">
                            16
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>51.84</td>
                    <td>
                        <a href="/catalog/rings/size-16_5/" class=" skazka-catalog-size- size-value-catalog">
                            16.5
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>53.41</td>
                    <td>
                        <a href="/catalog/rings/size-17/" class=" skazka-catalog-size- size-value-catalog">
                            17
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>54.98</td>
                    <td>
                        <a href="/catalog/rings/size-17_5/" class=" skazka-catalog-size- size-value-catalog">
                            17.5
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>56.55</td>
                    <td>
                        <a href="/catalog/rings/size-18/" class=" skazka-catalog-size- size-value-catalog">
                            18
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>58.12</td>
                    <td>
                        <a href="/catalog/rings/size-18_5/" class=" skazka-catalog-size- size-value-catalog">
                            18.5
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>59.69</td>
                    <td>
                        <a href="/catalog/rings/size-19/" class=" skazka-catalog-size- size-value-catalog">
                            19
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>61.26</td>
                    <td>
                        <a href="/catalog/rings/size-19_5/" class=" skazka-catalog-size- size-value-catalog">
                            19.5
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>62.83</td>
                    <td>
                        <a href="/catalog/rings/size-20/" class=" skazka-catalog-size- size-value-catalog">
                            20
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>64.40</td>
                    <td>
                        <a href="/catalog/rings/size-20_5/" class=" skazka-catalog-size- size-value-catalog">
                            20.5
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>65.97</td>
                    <td>
                        <a href="/catalog/rings/size-21/" class=" skazka-catalog-size- size-value-catalog">
                            21
                        </a>
                    </td>
                </tr>
                <!--tr><td>67.54</td><td><a href="/catalog/rings/?size=20.5-21" class=" skazka-catalog-size- size-value-catalog">21.5</a></td></tr>
                <tr><td>69.12</td><td><a href="/catalog/rings/?size=20.5-21" class=" skazka-catalog-size- size-value-catalog">22</a></td></tr-->
                </tbody>
            </table>
            <p></p>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>