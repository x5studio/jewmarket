<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Гарантия");
$APPLICATION->SetTitle("Гарантия");
?><div class="bx_page">
	<p>
		Обращаем ваше внимание на то, что при получении и оплате заказа покупатель в присутствии курьера обязан проверить комплектацию и внешний вид изделия на предмет отсутствия физических дефектов (царапин, трещин, сколов и т.п.) и полноту комплектации. После отъезда курьера претензии по этим вопросам не принимаются.
	</p>
 <b>Гарантийное обслуживание не производится, если:</b>
	<ul>
		<li>изделие имеет следы механического повреждения или вскрытия</li>
		<li>были нарушены условия эксплуатации, транспортировки или хранения </li>
		<li>проводился ремонт лицами, не являющимися сотрудниками авторизованного сервисного центра</li>
	</ul>
	<p>
		Подробное описание условий предоставления гарантии вы можете найти в документации к приобретенному товару и/или на нашем сайте.
	</p>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>