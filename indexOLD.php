<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Купить серебряные изделия в интернет-магазине «Ювелир Маркет»! Ювелирные изделия из серебра 925 оптом");
?>
<?
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 1, "IBLOCK_ID" => 29, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
   while ($arSect = $Section->GetNext())
   {
	   $sect = $arSect["ID"];
   }
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_slider", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "main_slider",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "slider",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	),
	false
);?>

		
		<div class="panel_catalog">
	<div class="title1">Популярные ювелирные украшения</div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"main_block1", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "main_block1",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "timestamp_x",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "29",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "CML2_ARTICLE",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "VSTAVKA",
			1 => "RAZMER",
			2 => "CML2_ARTICLE",
			3 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "VSTAVKA",
			1 => "RAZMER",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "5",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "ACTUAL",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $sect,
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
);?>
		</div>
		
		<div class="panel_small" id="filled">
			<img src="<?echo SITE_TEMPLATE_PATH?>/images/channel.png">
			<div>
				<h3>Коко Шанель</h3>
				<h4>Париж, 7 марта 1923 год</h4>
				<h5>
				 Мужчина, способный
				на поступки, обречен
				быть любимым.</h5>
			</div>
			

		</div>
		
		<div class="panel_cut">
	<div class="title1">Новинки</div>
	<?$panel_cut = 20;?>		
			<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"main_block1", 
	array(
		"PANEL_CUT" => "Y",
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "main_block1",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilterMain",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "29",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "CML2_ARTICLE",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "VSTAVKA",
			1 => "RAZMER",
			2 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "VSTAVKA",
			1 => "RAZMER",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "3",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "ACTUAL",
		),
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => "298",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
);?>
		</div>

		<style>.txtonpage{color: black;font-family: Arial,Helvetica,sans-serif;font-size: 15px;margin-bottom: 10px;}.txtonpage h1 {color: #173b6b;float: left;font-family: Arial,Helvetica,sans-serif;font-size: 38px;font-weight: 700;
padding: 20px 0 0;text-align: left;width: 100%;margin:0;}.txtonpage p, .txtonpage ul {font-size: 12px;font-weight: normal;}</style>
		<div class="panel_category txtonpage"  style="height:270px;">
			<h1>Изысканные изделия из серебра 925 пробы</h1>
			<h3>Купить стильные  серебряные изделия в интернет-магазине «Ювелир Маркет»<h3>
			<p>Наша жизнь наполнена огромным количеством событий, встреч, интересных знакомств. Яркие моменты память способна хранить долгие годы, а обычные будничные впечатления мы очень быстро забываем. Чтобы создать вокруг себя комфортное и удобное пространство, мы стремимся к тому, чтобы нас окружала красивая мебель, одежда, ювелирные изделия, а судьба дарила нам приятные сюрпризы, любовь близких, встречи с друзьями, общение с умными и интересными собеседниками.</p>
			<p><strong>Ювелирные изделия из серебра</strong> во все времена были важной частью жизни человека. Прикасаясь к благородному металлу, мы чувствуем, как меняется к нам отношение окружающих. Ювелирные украшения помогают нам обратить на себя внимание, получить подтверждение любви, закрепить в памяти важное для нас событие. Изделия из серебра дарят нам уверенность в себе, дают возможность почувствовать свою значимость. Наше окружение может по достоинству оценить наш вкус, ведь красивые ювелирные изделия не только подчеркивают наш статус, но и красноречиво свидетельствуют о наших чувствах и настроении.</p>
		</div>
		
		<div class="panel_category txtonpage" style="height:400px;">
			<h2>Выбрать и заказать изделия из серебра в магазине «Ювелир Маркет»</h2>
			<p>Ювелирные изделия из серебра имеют огромную армию поклонников, предпочитающих серебро другим металлам. Одним из его замечательных свойств является то, что с серебром прекрасно сочетаются практически все драгоценные и полудрагоценные камни. Это качество благородного металла с успехом применяют мастера компании «Ювелир Маркет». В своей работе они используют:</p>
			<ul>
				<li>жемчуг,</li>
				<li>янтарь,</li>
				<li>изумруд,</li>
				<li>аметист,</li>
				<li>гранат,</li>
				<li>сапфир,</li>
				<li>рубин,</li>
				<li>другие камни.</li>
			</ul>
			<p>Они создают чудесные украшения &mdash; серьги, цепочки, цепи, православные крестики, браслеты, кольца, печатки из серебра с натуральными камнями. Все они представлены на сайте нашей компании, где можно выбрать и заказать любое ювелирное украшение.</p>
			<p>У нас вы сможете подобрать ювелирные изделия  в подарок для женщин, для мужчин, коллег по работе, для деловых партнеров, для родных и близких. В каждой нашей коллекции всегда представлены изделия с позолотой, из чернёного серебра, позолоченная церковная утварь и украшения, родированное серебро и многое другое. Если вы ищете серебряные женские или мужские украшения для любимых, то проще всего их выбрать и приобрести интернет-магазине компании «Ювелир Маркет».</p>
		</div>
		
		<div class="panel_category txtonpage" style="height:410px;">
			<h2>Простота и удобство оформления заказов в интернет-магазине «ЮвелирМаркет»<h2>
			<p>Компания «Ювелир Маркет» делает все возможное для того, чтобы наши клиенты не только имели возможность дешево приобрести эксклюзивные ювелирные изделия, но и могли оперативно и безопасно (с комфортом) оформить заказ. </p>
			<p><strong>Заказать изделия из серебра в интернет-магазине «Ювелир Маркет» максимально просто и удобно!</strong> Мы предлагаем несколько удобных вариантов, каждый клиент вправе выбрать любой из них:</p>
			<ul>
				<li>оформить заказ без регистрации непосредственно на сайте,</li>
				<li>заказать обратный звонок на ваш номер,</li>
				<li>зарегистрироваться на сайте и оплатить заказ онлайн.</li>
			</ul>
			<p>Проще всего заказать ювелирные серебряные изделия в нашем интернет-магазине, воспользовавшись услугой «Обратный звонок». Наш менеджер перезвонит на ваш номер, и вы сможете уточнить все детали оплаты и доставки вашего заказа.</p>
			<p>Также вы можете совершить покупку без регистрации на сайте. Для этого достаточно поместить товар в корзину,  выбрать способ доставки и оплаты, а также указать ваши контактные данные. После этого наши менеджеры свяжутся с вами, уточнят адрес доставки, состав заказа и позаботятся о том, чтобы заказ был быстро отправлен по вашему адресу.</p>
			<p>Зарегистрировавшись на сайте, вы сможете не только самостоятельно выбирать и добавлять ювелирные украшения в корзину без заполнения  всех полей формы заказа, но и оплачивать покупки онлайн. Зарегистрированные пользователи могут самостоятельно вносить изменения в актуальную контактную информацию, в том числе менять адрес доставки. Кроме того, на сайте сохраняется история заказов, вы имеете возможность отследить статус текущего заказа в любое удобное время.</p>
		</div>

		<div class="panel_category">
			<h4>Нас выбирают</h4>
			<div class="categ"><img src="<?echo SITE_TEMPLATE_PATH?>/images/ring.png"><h3>более<br>
55 000<br>
моделей</h3></div>
			<div class="categ"><img src="<?echo SITE_TEMPLATE_PATH?>/images/ribbon.png"><h3>гарантия<br>
качества и<br>
возможность<br>
возврата</h3></div>
			<div class="categ"><img src="<?echo SITE_TEMPLATE_PATH?>/images/price.png"><h3>интересная<br>
ценовая<br>
политика</h3></div>
			<div class="categ"><img src="<?echo SITE_TEMPLATE_PATH?>/images/location.png"><h3> доставка<br>
			в любой регион<br>
			России</h3></div>
		</div>

		<div class="panel_category txtonpage" style="height:290px;">
			<h2>Купить серебряные изделия с доставкой по России<h2>
			<p>Мы уверены, что среди огромного ассортимента интернет-магазина «Ювелир Маркет» вы сумели подобрать оригинальные изделия из серебра для себя, своих близких или друзей. Мы позаботились не только о том, чтобы у вас был богатый выбор, но и о том, чтобы вы без проблем могли быстро получить свой заказ в любом населенном пункте Российской Федерации. При оформлении заказа вы можете выбрать любой из предлагаемых способов доставки. Мы предлагаем своим клиентам следующие варианты:</p>
			<ul>
				<li>доставка курьерскими службами;</li>
				<li>доставка «Почтой России»;</li>
				<li>получение посылок в одном из постаматов системы «PickPoint»</li>
			</ul>
			<p>Возможно, вы предпочитаете быструю доставку из рук в руки &mdash; в этом случае ваш заказ может быть доставлен вам курьерскими службами DPD и (либо) DHL. В отдаленные уголки страны посылку доставит «Почта России». Ну, а если вы не желаете быть ограниченными расписанием работы почтовых отделений, то оптимальный выбор для вас &mdash; система постаматов. Мы предоставляем клиентам возможность самостоятельно сравнить эти способы доставки серебряных изделий и выбрать тот, который является наиболее удобным.</p>
		</div>

		<div class="panel_category txtonpage" style="height:520px;">
			<h2>Продажа ювелирных изделий в городах России, в Германии, Беларуси и Казахстане<h2>
			<p>Компания «Ювелир Маркет» является одним из крупнейших продавцов изделий из серебра. Если у вас нет желания и возможности посещать ювелирные магазины в поисках интересных украшений, пожалуй, самым оптимальным вариантом для вас может стать покупка в нашем интернет-магазине «Ювелир Маркет». У нас вы найдете огромный ассортимент изделий из серебра 925 пробы. Быстро и безопасно заказать и получить заказ могут наши клиенты во многих городах страны. В этот перечень входят: </p>
			<ul>
				<li>Москва;</li>
				<li>Санкт-Петербург (СПб);</li>
				<li>Екатеринбург;</li>
				<li>Самара;</li>
				<li>Пермь;</li>
				<li>Новосибирск;</li>
				<li>Нижний Новгород;</li>
				<li>Казань;</li>
				<li>Челябинск;</li>
				<li>Омск;</li>
				<li>Ростов-на-Дону;</li>
				<li>Красноярск;</li>
				<li>Воронеж;</li>
				<li>Волгоград;</li>
				<li>Саратов;</li>
				<li>Краснодар;</li>
				<li>Тюмень;</li>
				<li>Сочи;</li>
				<li>Уфа.</li>
			</ul>
			<p>При необходимости курьерские и почтовые службы могут доставить ваш заказ в любой населенный пункт Российской Федерации. Ювелирные украшения нашей компании также можно приобрести онлайн и за пределами России. Наш интернет-магазин осуществляет продажу ювелирных изделий в Германии, а также предлагает полный модельный ряд наших украшений жителям Минска, Астаны, Алматы (Алма-Ата). </p>
		</div>

		<div class="panel_category txtonpage" style="height:400px;">
			<h2>Изделия из серебра 925 пробы оптом<h2>
			<p>Компания «Ювелир Маркет» работает с 2007 года. Наш главный офис находится в Санкт-Петербурге, а производственные мощности расположены в Санкт-Петербурге и Костроме. 
У нас вы всегда можете купить серебряные изделия оптом. Мы не только предлагаем низкие цены, но также обеспечиваем ряд дополнительных преимуществ, благодаря которым партнерство выгодно для обеих сторон. Среди главных преимуществ нашей компании можно назвать следующее:
</p>
			<ul>
				<li>огромное количество (более 55 тысяч) изделий в наличии;</li>
				<li>выездная демонстрация готовой продукции для оптовых покупателей;</li>
				<li>наличие большого количества изделий на один артикул;</li>
				<li>большой склад ювелирных изделий;</li>
				<li>минимальный срок производства оптовых партий (до двух недель).</li>
			</ul>
			<p>Наши деловые партнеры получают возможность оформить заказ и купить изделия из серебра оптом в личном онлайн-кабинете на нашем сайте. Зарегистрированные оптовые покупатели могут самостоятельно выбирать и добавлять в корзину необходимое количество украшений, оплачивать товар онлайн, а также отслеживать текущее состояние своего заказа непосредственно на сайте.</p>
			<p>Если вы ищете надежного оптового поставщика, предлагающего ювелирные изделия оптом, наша компания приглашает вас к сотрудничеству. Мы обеспечиваем:</p>
			<ul>
				<li>высокое качество и широкий ассортимент изделий;</li>
				<li>обмен или возврат продукции;</li>
				<li>оперативность оформления заказов;</li>
				<li>гибкие схемы финансовых взаиморасчётов.</li>
			</ul>
		</div>

		<div class="panel_small" id="empty">
			<h4>Подписка</h4>
			<span>Хотите получать полезные советы о выборе и умении правильно носить украшения, стильном образе жизни, а также акциях и скидках в нашем интернет-магазине?
Подпишитесь на нашу
еженедельную рассылку</span>
<?$APPLICATION->IncludeComponent(
	"bitrix:sender.subscribe", 
	"subscribe", 
	array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "subscribe",
		"CONFIRMATION" => "N",
		"SET_TITLE" => "N",
		"SHOW_HIDDEN" => "N",
		"USE_PERSONALIZATION" => "Y"
	),
	false
);?>
			<!--<form id="form_subscribe">
			<input type="text" placeholder="ваш эл.ящик" required id="eml">
			<input type="submit" value="Подписаться" id="sub">
			</form> -->
		</div>

		</div>
		<div class="panel_cut" id="empty2">

		</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>