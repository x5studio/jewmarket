<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 02.06.2017
 * Time: 16:53
 */

namespace XFive\Patterns;

trait Singleton
{
    protected static $instance;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}