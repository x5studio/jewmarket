<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 02.05.2017
 * Time: 13:13
 */

/**
 * Класс в котором будут собраны вызовы всех событий, чтобы не писать простыню в init.php
 */

namespace XFive\Main;


class EventManager
{
    public function initEvents()
    {
        $bxEventManager = \Bitrix\Main\EventManager::getInstance();

        $bxEventManager->addEventHandler(
            "main",
            "OnPageStart",
            array(
                "XFive\Main\Application",
                "onPageStart",
            )
        );

        $bxEventManager->addEventHandler(
            "iblock",
            "\Bitrix\Iblock\PropertyTable::OnAfterAdd",
            array(
                "XFive\Iblock\Properties",
                "onPropChange",

            )
        );

        $bxEventManager->addEventHandler(
            "iblock",
            "\Bitrix\Iblock\PropertyTable::OnAfterUpdate",
            array(
                "XFive\Iblock\Properties",
                "onPropChange",
            )
        );

        $bxEventManager->addEventHandler(
            "catalog",
            "OnSuccessCatalogImport1C",
            array(
                "XFive\Sale\Import1C",
                "OnSuccessCatalogImport1C",
            )
        );

        $bxEventManager->addEventHandler(
            "catalog",
            "OnProductAdd",
            array(
                "XFive\Catalog\Product",
                "UpdateProductQuantity",
            )
        );

        $bxEventManager->addEventHandler(
            "catalog",
            "OnProductUpdate",
            array(
                "XFive\Catalog\Product",
                "UpdateProductQuantity",
            )
        );
    }
}