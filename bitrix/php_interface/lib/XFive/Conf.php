<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 25.09.2017
 * Time: 8:13
 */

namespace XFive;


class Conf
{
    const PROPERTY_HIT_CODE = "HIT";
    const PROPERTY_SPECIALOFFER_CODE = "SPECIALOFFER";
    const PROPERTY_DISCOUNT_CODE = "SKIDKA";
    const PROPERTY_NEW_PRODUCT_CODE = "NEWPRODUCT";
    const PROPERTY_VIDEO_CODE = "VIDEO_1";
    const PROPERTY_OFFER_VIDEO_CODE = "__1";
}