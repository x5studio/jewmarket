<?

/*
20171118
 */

namespace XFive\Catalog;
use Bitrix\Main\Context;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("catalog");

class CKomplekt
{
	public static $IBLOCK_ID = 10; //iblock dlya komplektov
	public static $error_text = "no errors";
	public static $glue_XmlId = "-";

	static function fail($text){
		self::$error_text = $text;
		return false;
	}

	static function getDiscountByKomplektKod($komplekt_kod)
	{
		$temp = explode("_", $komplekt_kod);
		if (!isset($temp[1])) return self::fail("getDiscountByKomplektKod: Неправильное название вида комплекта");
		$discount_percent = $temp[1];
		return $discount_percent;
	}

	static function getPrice($idProduct,$idPriceType){

		$dbPrices = \Bitrix\Catalog\PriceTable::getList(array(
			"select" => array("ID","PRODUCT_ID",
				"CATALOG_GROUP_ID", "PRICE"
			),
			"filter" => array(
				"PRODUCT_ID" => $idProduct,
				"CATALOG_GROUP_ID" => $idPriceType,
			),
			"order" => array("PRICE" => "DESC"),
			"limit"=>1
		));
		if ($price = $dbPrices->fetch()) {
			return $price;
		}
		return self::fail("getKomplektPriceAsProduct");
	}

	static function calcKomplektPrice($discount_percent,$idPriceType,$elementsIds){

		$dbPrices = \Bitrix\Catalog\PriceTable::getList(array(
			"select" => array("ID","PRODUCT_ID",
				"CATALOG_GROUP_ID", "PRICE"
			),
			"filter" => array(
				"PRODUCT_ID" => $elementsIds,
				"CATALOG_GROUP_ID" => $idPriceType,
			),
			"order" => array("PRICE" => "DESC"),
			"limit"=>100000
		));
		$PRICES = array();
		$total_summa = 0;

		while ($price = $dbPrices->fetch()) {
			$PRICES[ $price['PRODUCT_ID'] ] = $price;

			$total_summa += $price['PRICE'];
		}
		$KOMPLEKT_PRICE = round($total_summa-($total_summa*$discount_percent/100));
		return $KOMPLEKT_PRICE;
	}

	static function getKomplektXmlId($komplekt_kod,$price,$elementsIds){
		return $komplekt_kod.self::$glue_XmlId.implode(self::$glue_XmlId,$elementsIds).self::$glue_XmlId."price".$price;

	}

	static function getName($elementsIds){
		return "Комплект ".implode("_",$elementsIds);
	}


	static function getPriceType(){
		$price_types = \XFive\Main\Settings::getInstance()->get("CATALOG_PRICES");
		$dbPriceType = \CCatalogGroup::GetList(
			array(),
			array("NAME" => $price_types[0])
		);
		if(!($arPriceType = $dbPriceType->Fetch()))
		{
			return fail("не удалось получить тип цены по коду");
		}
		return $arPriceType;
	}



	static function getOrAddKomplekt($idUser,$komplekt_kod, $elementsIds){

		asort($elementsIds);
		$discount_percent = self::getDiscountByKomplektKod($komplekt_kod);
		if($discount_percent === false) return false;

		$arPriceType = self::getPriceType();

		$komplekt_price = self::calcKomplektPrice($discount_percent,$arPriceType['ID'],$elementsIds);
		$xml_id = self::getKomplektXmlId($komplekt_kod,$komplekt_price,$elementsIds);
		$symbol_code = $xml_id;
		$existingKomplekt = self::getKomplektIfExist($xml_id);
		if(is_array($existingKomplekt)){
			return $existingKomplekt;
		}

		$name = self::getName($elementsIds);
		$arKomplekt = self::addAndGetKomplekt($komplekt_kod,$idUser,$name,$symbol_code,$xml_id,$arPriceType,$komplekt_price,$elementsIds);
		if(!is_array($arKomplekt)) return false;

		return $arKomplekt;
	}



	static function getKomplektIfExist($xml_id){
		$arSelect = Array("ID","NAME","XML_ID","DETAIL_PAGE_URL");
		$arFilter = Array("IBLOCK_ID"=>self::$IBLOCK_ID,"XML_ID"=>$xml_id,"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($komplekt = $res->GetNext())
		{
			return array("KOMPLEKT"=>$komplekt);
		}
		return false;
	}

	static function checkElementsForKomplektKod($komplekt_kod,$elementsIds){

		$db_res = \CCatalogProduct::GetList(
			array(),
			array("ID" => $elementsIds),
			false,
			false,
			array("ID","TYPE")
		);
		$productsId = array();
		while ($ar_res = $db_res->Fetch())
		{
			if($ar_res['TYPE'] == 4){
				 $productByOffer = \CCatalogSKU::GetProductInfo($ar_res['ID']);
				$productsId[] = $productByOffer['ID'];
			} else $productsId[] = $ar_res['ID'];
		}

		$arSelect = Array("ID"); //
		$arFilter = Array("ID"=>$productsId,"ACTIVE"=>"Y","PROPERTY_".COMPLEMENT_PROP_NAME => $komplekt_kod );
		//print_r($arFilter);
		$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$correctElemsId = array();
		while($row = $res->GetNext())
		{
			$correctElemsId[] = $row['ID'];
		}
		if(count($elementsIds)!=count($correctElemsId)) return self::fail("checkElementsForKomplektKod: ne vse elementy peredannogo massiva vhodyat v komplekt");

		return true;
	}

	static function addAndGetKomplekt($komplekt_kod,$idUser,$name,$symbol_kod,$xml_id,$arPriceType,$KOMPLEKT_PRICE,$elementsIds){

		if(!self::checkElementsForKomplektKod($komplekt_kod,$elementsIds)) return false;

		//$arPriceType = self::getPriceType();
		//sozdaem element
		$el = new \CIBlockElement;
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $idUser, // элемент изменен текущим пользователем
			"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
			"IBLOCK_ID"      => self::$IBLOCK_ID,
			//"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $name,
			"CODE"			 => $symbol_kod,
			"ACTIVE"         => "Y",            // активен
			"PREVIEW_TEXT"   => "комплект",
			"DETAIL_TEXT"    => "комплект",
			"XML_ID"	=> $xml_id
		);

		if($PRODUCT_ID = $el->Add($arLoadProductArray)){

		} else return self::fail("addAndGetKomplekt");
		//sozdaem tsenu

		$arFields = array(
			"ID" => $PRODUCT_ID,
			"TYPE" => \CCatalogProductSet::TYPE_SET
		);



		if(!\CCatalogProduct::Add($arFields)) return self::fail("failed while adding product");
		//sozdaem tsenu

		$arFields = Array(
			"CURRENCY"         => "RUB",       // валюта
			"PRICE"            => $KOMPLEKT_PRICE,      // значение цены
			"CATALOG_GROUP_ID" => $arPriceType['ID'],           // ID типа цены
			"PRODUCT_ID"       => $PRODUCT_ID,  // ID товара
		);
// добавляем
		$idAddedPrice = \CPrice::Add( $arFields );
		if(!$idAddedPrice) return self::fail("Ошибка при добавлении цены комплекта");
		//sozdaem noviy komplekt

		$ITEMS = array();
		foreach($elementsIds as $element_id){
			$ITEMS[] = array(
				"ACTIVE" => "Y", // активность записи
				"ITEM_ID" => $element_id, // ID товара который добавляется в набор
				"QUANTITY" => 1 // количество товара
			);
		}

		$arFields = array("TYPE" => 1, //тип 1 - комплект, 2 - набор
			"SET_ID" => 0, //указывает на то что набор добавляется к этому товару
			"ITEM_ID" => $PRODUCT_ID, // ID товара
			"ITEMS" => $ITEMS
		);

		if(\CCatalogProductSet::add($arFields)){

			\CCatalogProductSet::recalculateSetsByProduct($PRODUCT_ID); //пересчет остатков комлпекта, на основании товаров , входящих в комплект

			//getting added complement
			$arSelect = Array("ID","NAME","XML_ID","DETAIL_PAGE_URL");
			$arFilter = Array("IBLOCK_ID"=>self::$IBLOCK_ID,"ID"=>$PRODUCT_ID,"ACTIVE"=>"Y");
			$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($komplekt = $res->GetNext())
			{
				return array("PRICE_ID"=>$idAddedPrice,"KOMPLEKT"=>$komplekt);
			}

			return self::fail("failed while getting added complement");
		}
		else
			return self::fail("failed while adding complement");
	}


	static function changeKomplektForBasketId($idUser,$basketId,$elementsIds){
		Loader::includeModule('sale');
		//poluchit zapis tablitsy i vitsepit xml_id
		$basket_row = \CSaleBasket::GetByID($basketId);
		if(!$basket_row) return self::fail("changeKomplektForBasketId: \CSaleBasket::GetByID ");
		//poluchit iz xml_id kod

		$t = explode(self::$glue_XmlId,$basket_row['PRODUCT_XML_ID']);
		//die(json_encode(array('basketId'=>$basketId,'basket_row'=>$basket_row)));
		if(count($t)<2) return self::fail("changeKomplektForBasketId: xml kod komplekta v korzine nekorrekten ".json_encode(array('basketId'=>$basketId,'basket_row'=>$basket_row)));
		$komplekt_kod = $t[0];

		$arRes = self::getOrAddKomplekt($idUser,$komplekt_kod,$elementsIds);
		if(!is_array($arRes)) return false;

		$price_type = self::getPriceType();

		$arProductPrice = self::getPrice($arRes['KOMPLEKT']['ID'],$price_type['ID']);

		if($arRes['KOMPLEKT']['XML_ID']!=$basket_row['PRODUCT_XML_ID']){ //esli sovpadayut, to komplekt ne pomenyalsya
			if(!self::updateBasket($basketId,$arRes['KOMPLEKT'],$arProductPrice)) return false;
		}

		$SUMMA = $arProductPrice['PRICE']*$basket_row['QUANTITY'];

		return array("KOMPLEKT"=>$arRes['KOMPLEKT'],"PRICE"=>$arProductPrice,"SUMMA"=>$SUMMA);

	}

	static function updateBasket($basketId,$newKomplekt,$arProductPrice){

		$arFields = array(
			"PRODUCT_ID" => $arProductPrice['PRODUCT_ID'],
			"PRICE" => $arProductPrice['PRICE'],
			"BASE_PRICE" => $arProductPrice['PRICE'],
			"PRODUCT_PRICE_ID" => $arProductPrice['ID'],
			"NAME" => $newKomplekt['NAME'],
			"PRODUCT_XML_ID" => $newKomplekt['XML_ID'],
			"DETAIL_PAGE_URL" => $newKomplekt['DETAIL_PAGE_URL']

		);
		//die(json_encode($arFields));
		//die(json_encode(array('newKomplekt+'=>$newKomplekt)));
		if(!\CSaleBasket::Update($basketId, $arFields)) return self::fail("updateBasket");
		return true;
	}

	/**метод, формирующий js-код для изменения кнопки Добавить в корзину на В корзине для тех комплектов, которые добавлены в корзину.
	 * Использую в footer.php, чтобы отрабатывал на всех страницах, где есть комплекты
	 * @return string
	 */
	static function getBasketKomplektJs(){
		$arBasketItemsID = array();

		$dbBasketItems = \CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
			),
			array(
				"FUSER_ID" => \CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
			),
			false,
			false,
			array("ID", "MODULE","PRODUCT_ID", "QUANTITY","PRICE",)
		);
		while ($arItems = $dbBasketItems->Fetch())
		{
			//print_r($arItems);
			$arBasketItemsID[] = $arItems['PRODUCT_ID'];
		}

		$arBasketKomplekts=array();
		if(is_array($arBasketItemsID)&&$arBasketItemsID) {
			$arSelect = Array("ID","CODE");
			$arFilter = Array("IBLOCK_ID" => 10, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $arBasketItemsID);
			$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			while ($ob = $res->Fetch()) {
				//print_r($ob);
				$t = explode('-',$ob['CODE']);
				$arBasketKomplekts[] = $t[0];
			}


		}
		//print_r($arBasketKomplekts);

		$js ="";
		foreach($arBasketKomplekts as $komplekt_code){
			$js.=" $('#komplektbutton".$komplekt_code."').removeClass('add2basket').attr('data-inbasket','true').attr('title','В корзине'); "; //
			$js.=" $('#komplektbutton".$komplekt_code.">span').html('В корзине'); ";
		}
		return $js;
	}




}