<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["READY"]=="Y" || $arResult["DELAY"]=="Y" || $arResult["NOTAVAIL"]=="Y" || $arResult["SUBSCRIBE"]=="Y")
{
?>
<!-- <div class="ajax_basket_shadow"> -->
	<div id="ajax_basket">
	<div class="cross3"></div>
<?
	if ($arResult["READY"]=="Y")
	{
		?>
<form class="parent_id">
		<table class="sale_basket_small" cellspacing="0" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Наименование</td>
			<td>Комплект</td>
			<td>Количество</td>
			<td>Сумма</td>
		</tr>
		<?foreach ($arResult["ITEMS"] as &$v):?>
			<tr class="parent_row" data-role="product_<?=$v["PRODUCT_ID"]?>">

				<td>
					<div class="delete"></div>
				</td>
				<td>
					<img width="50px" height="50px" src="<?=$v["DETAIL_PICTURE"]?>"/>
				</td>
				<td>
					<div class="title"><?echo $v["NAME"]?></div>
					<div class="article"></div>
				</td>
				<td style="width: 140px;">
					<?foreach( $v["KOMPLEKT"] as $elem):?>
					<?if ($elem["ACTIVITY"] == true) {$style = "green";} else {$style = "";}?>
						<div class="img_detail <?=$style?>">
							<img src="<?=$elem["DETAIL_PICTURE"]?>" width="50px"/>
							<?if ($elem["ACTIVITY"] == true):?>
								<div class="cart_checked"></div>
							<?endif?>
							<div class="sub_detail">
								<div class="title"><a href="<?=$elem["DETAIL_PAGE_URL"]?>"><?=$elem["NAME"]?></a></div>
								<div class="article"><?=$elem["ARTICLE"]?></div>
										<?if ($elem["ACTIVITY"] == true):?>
											<span>В корзине</span>
										<?endif?>
								<div class="sub_img">
									<a href="<?=$elem["DETAIL_PAGE_URL"]?>">
										<img src="<?=$elem["DETAIL_PICTURE"]?>"/>
									</a>
								</div>
							</div>
						</div>
					<?endforeach?>
				</td>
				<td>
					<div class="input_top_down">
						<input class="sel" name="product_<?echo $v["PRODUCT_ID"]?>" value="<?echo $v["QUANTITY"]?>">
												<div class="sel_block">
													<a class="cart_plus"></a>
													<a class="cart_minus"></a>
												</div>
					</div>
				</td>
				<td>
					<div class="price"><span title="<? echo ($v["PRICE_FORMATED"]*1)?>"><?echo ($v["PRICE_FORMATED"]*$v["QUANTITY"])?></span> руб</div>
					<?$sum = $sum + ($v["PRICE_FORMATED"]*$v["QUANTITY"]);?>
				</td>

			</tr>
		<?endforeach?>
		<?if (isset($v))
			unset($v);?>
		<tr class="underline">
			<td>ИТОГО</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><span class="all_sum"><span><?echo $sum?></span> руб</span></td>
			<td>&nbsp;</td>
		</tr>
		</table>
		<div class="return_buy">Продолжить покупки</div>
		<!-- <div class="close">
			<div class="close_mess">
				<div class="mess">
				Сохранить измения в корзине?
				</div>
				<br />
				<a class="close_buy2">Да</a>
				<a class="close_close2">Нет</a>
							
			</div>
		</div> -->
		<div class="end_dialog_shadow">
			<div class="close_mess">
				<div class="mess">
				Сохранить измения в корзине?
				</div>	
				<br />
				<a class="close_buy2">Да</a>
				<a class="close_close2">Нет</a>				
			</div>
		</div>
		<!-- <input class="return_buy" name="submit" type="submit" value="Продолжить покупки"> -->
		<div class="order_buy"><a href="/personal/order/make/">Оформить заказ</a></div>
		

		</form>
		
		<table>
				
<?
		if ('' != $arParams["PATH_TO_BASKET"])
		{
			?><tr><td align="center"><form method="get" action="<?=$arParams["PATH_TO_BASKET"]?>">
			<input class="hide_submit" type="submit" value="<?= GetMessage("TSBS_2BASKET") ?>">
			</form></td></tr><?
		}
		if ('' != $arParams["PATH_TO_ORDER"])
		{
			?><tr><td align="center"><form method="get" action="<?= $arParams["PATH_TO_ORDER"] ?>">
			<input class="order_buy_submit" type="submit" value="<?= GetMessage("TSBS_2ORDER") ?>">
			</form></td></tr><?
		}
	}
?>
</table>
	</div>
<!--	</div> -->
	<?
}
?>
<script>

</script>