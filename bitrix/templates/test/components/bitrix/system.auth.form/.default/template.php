<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	{?>
	<div class="autherror">
	<?//ShowMessage($arResult['ERROR_MESSAGE']);?>
	</div>
	<script>
		$(".bxreg_shadow").show();
	</script>
	<?
	}?>
<div class="bx-system-auth-form">

<?if($arResult["FORM_TYPE"] == "login"):?>
<?
/*   CAjax::Init(); 
   $bxajaxid = CAjax::GetComponentID('bitrix:system.auth.form', ''); 
   if (strlen($_POST['AJAX_CALL']) && $_POST['AJAX_CALL']=='Y' && $_POST['auth_secret']=='Y' && $_POST['TYPE']=='AUTH') {
	$APPLICATION->RestartBuffer();
   }
   $dir = $APPLICATION->GetCurDir();*/
?>
<a class="profile" id="throw_reg" title="Войдите или зарегистрируйтесь">Вход / Регистрация</a>
<div class="bxreg_shadow">
<div class="bxreg_window">
<div class="cross2"></div>
<form class="WorkArea_AuthBlock" id="WAAUTH" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']):?>
<div id="throw_exception" style="top:-50px; padding: 0px;"><?ShowMessage($arResult['ERROR_MESSAGE']);?></div>
<?else:?>
<div id="throw_exception" class="throw_hidd"></div>
<?endif;?>
<?/*$APPLICATION->IncludeComponent(
	"bitrix:system.auth.registration",
	"flat",
	Array(
		"AUTH" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"REQUIRED_FIELDS" => array(),
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL"),
		"SUCCESS_PAGE" => "",
		"USER_PROPERTY" => array(),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y"
	)
);*/?>
<?if($arResult["BACKURL"] <> ''):?>
 	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input id="wa_input_auth" type="hidden" name="AUTH_FORM" value="Y" />
	<input id="wa_input_type" type="hidden" name="TYPE" value="AUTH" />
<!--<input type="hidden" name="backurl" value="/about/index.php">
<input type="hidden" name="AUTH_FORM" value="Y">	
<input type="hidden" name="TYPE" value="REGISTRATION"> -->
	<table width="95%" id="WAAUTH_DELIGATE">
		<tr>
			<td colspan="2">
			<!-- <span class=""><?=GetMessage("AUTH_LOGIN")?></span><br /> -->
			<span class="">Войти или зарегистрироваться</span><br />
			<div id="wa_user_isset" style="display: none"></div>
			<input class="wa_main_field" id="wa_user_login" type="text" name="USER_LOGIN" maxlength="50" value="<?//=$arResult["USER_LOGIN"]?>" size="17" placeholder="E-mail:"/></td>
		</tr>
		<tr>
			<td colspan="2">
			<input class="wa_main_field" id="wa_user_pass1" type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" placeholder="<?=GetMessage("AUTH_PASSWORD")?>"/>
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_PASSWORD")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
</script>
<?endif?>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input id="wa_user_pass2" class="wa_main_field wa_hidd" type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" placeholder="Повторный ввод пароля" autocomplete="off" />
			</td>
		</tr>
<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
<!--		<tr>
			<td valign="top"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label></td>
		</tr> -->
<?endif?>

<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
		<tr class="wa_aureg">
			<td><a id="wa_auth_btn" class="wa_auth_btn">Войти</a><input id="wa_submit" type="submit" class="wa_authhidd" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>"/></td>
			<td><a id="wa_reg_btn" class="wa_auth_btn disabar">Зарегистрироваться</a></td>
		</tr>
<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
<!--		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a></noindex><br /></td>
		</tr>
<?endif?>

		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex></td>
		</tr>
-->
	</table>
<div class="WorkArea_social">
<?if($arResult["AUTH_SERVICES"]):?>
		<tr>
			<td colspan="2">
				<div class="bx-auth-lbl">Войти через социальную сеть</div>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
			</td>
		</tr>
<?endif?>
</div>
</form>
<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>
<?
elseif($arResult["FORM_TYPE"] == "otp"):
?>
<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />
	<table width="95%">
		<tr>
			<td colspan="2">
			<?echo GetMessage("auth_form_comp_otp")?><br />
			<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
		</tr>
<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
		<tr>
			<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
		</tr>
<?endif?>
		<tr>
			<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
		</tr>
		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
		</tr>
	</table>
</form>

</div>
</div>
<?
else:
?>
<form action="<?=$arResult["AUTH_URL"]?>" class="ifreg">
	<p>Вы вошли как:&nbsp;<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><i class="fa fa-user fa-2"></i> <span class="header_auth_login" title="admin">
	<?
	if (!empty($arResult["USER_NAME"]))
		{	
		echo $arResult["USER_NAME"];
		}
		else
		{
			echo $arResult["USER_LOGIN"];
		}
	?>
	</span></a></p>
	<p>
	<p>
	<a href="<?=$arResult["PROFILE_URL"]?>" class="header_auth_partner" title="<?=GetMessage("AUTH_PROFILE")?>">Личный кабинет</a>
		<?foreach ($arResult["GET"] as $key => $value):?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<span class="exit"><i class="header_red fa fa-power-off"></i><input type="submit" class="header_auth_partner" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" /></span>
	</p>
<!--	<a href="<?=$arResult["PROFILE_URL"]?>" class="header_auth_partner" title="<?=GetMessage("AUTH_PROFILE")?>">Личный кабинет</a> <a href="/index.php?logout=yes" title="Выход"><i class="header_red fa fa-power-off"></i>Выход</a>
	</p>
	<table width="95%">
		<tr>
			<td align="center">
				<?=$arResult["USER_NAME"]?><br />
				[<?=$arResult["USER_LOGIN"]?>]<br />
				<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
			</td>
		</tr>
		<tr>
			<td align="center">
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />
			</td>
		</tr>
	</table> --> 
</form>
<?endif?>
</div>
<script>
/*function wa_wright_register (wamode, waFightForTheGlory)
{
	if (waFightForTheGlory)
	{
		$('#wa_input_type').attr('value','REGISTRATION');
		$("#wa_submit").attr('type','submit');
		$("#WAAUTH").submit();
		
	}
	else
	{
		$('#wa_input_type').attr('value','AUTH');
		$("#wa_submit").attr('type','submit');
		$("#WAAUTH").submit();
	}
}
function wa_error_pass (wapass,waevpass) 
{
	var str = waevpass.val();	
	if (str.length < 6)
	{
		$('#wa_user_pass1').addClass('wa_field_error');
		$('#throw_exception').text('Пароль должен быть не менее 6 символов');
		$('#throw_exception').removeClass('throw_hidd');
		
	}
	else
	{
		console.log(wapass);
		if (wapass == 'wa_auth_btn')
		{	
			var waAuthTogetherMU = false;
			wa_wright_register(wapass, waAuthTogetherMU);
		}
		else
		{
			var doublepass = $('#wa_user_pass2');	
			if (str != doublepass.val())
			{
				$('#wa_user_pass1').val('');
				$('#wa_user_pass2').val('');
				$('#throw_exception').text('Пароли не совпадают. Попробуйте еще раз');
				$('#throw_exception').removeClass('throw_hidd');
			}
			else
			{
				var waAuthTogetherMU = true;
				wa_wright_register(wapass, waAuthTogetherMU);
			}
		}
	}
}
function wa_error_login (walogin)
{
	var waloginval = walogin.val();

	var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
	if ( pat = pattern.test(waloginval))
	{
		return true;
	}
	else
	{
		return false;
	}
}
function wa_error_separate (waerr)
{
	var waaghid = waerr;
	var errlogin = $('#wa_user_login');
	var errpass1 = $('#wa_user_pass1');
	if (errlogin.val() != '')
	{
		if (!wa_error_login(errlogin))
		{
			 errlogin.addClass('wa_field_error');
			 $('#throw_exception').text('Неправильный логин.  Пример customer@awesomemail.com');
			 $('#throw_exception').removeClass('throw_hidd');
		}
		else
		{
			wafirsttest = true;
			if (wa_error_pass(waaghid, errpass1))
			{
				
			}
			else
			{
				return false
			}
		}
	}
	else
	{
		 errlogin.addClass('wa_field_error');
		 $('#throw_exception').text('Введите логин.  Пример customer@awesomemail.com');
		 $('#throw_exception').removeClass('throw_hidd');
	}
}
function waMetalUnion (waWillBeTrue) {
	var waSwordAndSorcery = waWillBeTrue;
	var waid = waSwordAndSorcery.attr('id');
	var wahid = $(".wa_hidd");
	var wacl = waWillBeTrue;
	if ( wacl.hasClass('disabar'))
		{
			$(".wa_auth_btn").addClass('disabar');
			wacl.removeClass('disabar');
			if (waid == 'wa_reg_btn') {wahid.show()} else (wahid.hide());
		}
		else
		{
			
			if( fwd = wa_error_separate(waid))
			{
				console.log(fwd);
			}
		}	
}
$(".wa_auth_btn").on("click", function() {
	if (!$("#throw_exception").hasClass('throw_hidd')) {$("#throw_exception").addClass('throw_hidd')};
	var waSwordAndSorcery = $(this);
	var waid = waSwordAndSorcery.attr('id');
	var wahid = $(".wa_hidd");
	var wacl = waSwordAndSorcery;
	if ( wacl.hasClass('disabar'))
		{
			$(".wa_auth_btn").addClass('disabar');
			wacl.removeClass('disabar');
			if (waid == 'wa_reg_btn') {wahid.show()} else (wahid.hide());
		}
		else
		{
			
			if( fwd = wa_error_separate(waid))
			{
				console.log(fwd);
			}
		}	
});

$('.wa_main_field').on('click', function () {
	if ($(this).hasClass('wa_field_error'))
	{
	$(this).removeClass('wa_field_error');
	$('#throw_exception').addClass('throw_hidd');		
	}	
});
$('.bx-system-auth-form .cross2').on('click', function() {
	$('.bxreg_shadow').fadeOut(300);
})
document.onkeyup = function (e) {
	    e = e || window.event;
	    if (e.keyCode === 13) {
			if ($('#wa_reg_btn').hasClass('disabar'))
			{
				waMetalUnion($('#wa_auth_btn'));
			}
			else
			{
				waMetalUnion($('#wa_reg_btn'));
			}
	       return false;
	    }
	    
	}*/
function waRun (wa)
{
		var type = wa.attr('id');
		if (type == 'wa_reg_btn')
			{
				
			}
			else
			{
				
			}
			
};

waAuth = {
	buttHeart : function (wa) {
		var id = wa.attr('id');
		var pass2 =  $('#wa_user_pass2');
		var waLogin = $('#wa_user_login');
		if (id == 'wa_reg_btn')
			{
				if (pass2.hasClass('wa_hidd'))
					{
						pass2.removeClass('wa_hidd');
						pass2.val('');
					}
				wa.removeClass('disabar');
				$('#wa_auth_btn').addClass('disabar');
				if (!waLogin.hasClass('wa_accept'))
					{
						if (!waAuth.checkUser(waLogin))
						{
						 waAuth.throwException(waLogin,true);
						 $('#throw_exception').text('Введите логин.  Пример customer@awesomemail.com');
						}
						else
						{
							console.log(waAuth.throwException(false,false));
						}					
					}
				waAuth.checkUser(wa);
			}
			else
			{
				if (!pass2.hasClass('wa_hidd'))
					{
						pass2.addClass('wa_hidd');
					}	
				wa.removeClass('disabar');
				$('#wa_reg_btn').addClass('disabar');				
			}
		return true;
	},
	checkSubmit(wa)
	{
		
	},
	clickCheck(wa)
	{
		var id = wa.attr('id');
		var pass2 =  $('#wa_user_pass2');
		var waLogin = $('#wa_user_login');
		if (id == 'wa_reg_btn')
			{
				if (pass2.hasClass('wa_hidd'))
					{
						pass2.removeClass('wa_hidd');
						pass2.val('');
					}	
				wa.removeClass('disabar');
				$('#wa_auth_btn').addClass('disabar');					
			}
			else
			{
				if (!pass2.hasClass('wa_hidd'))
					{
						pass2.addClass('wa_hidd');
					}	
				wa.removeClass('disabar');
				$('#wa_reg_btn').addClass('disabar');					
			}
	},
	throwException : function (wa, error)
	{
		var hidd = 'throw_hidd';
		if (!error) // убираем хинт  
		{
			$('#throw_exception').addClass(hidd);
			return;
		}
		else
		{
		wa.removeClass('wa_accept');
		wa.addClass('wa_field_error');
		$('#throw_exception').removeClass(hidd);
		}

	},
	checkPass : function (wa) 
	{
		var pass1 = $('#wa_user_pass1');
		var pass2 = $('#wa_user_pass2');

		if (!pass2.hasClass('wa_hidd')) //процесс регистрации
			{
				if (!pass1.val())
				{
					waAuth.throwException(pass1,true);
					$('#throw_exception').text('Введите пароль');
					return false;
				}		
				else
				{
					var str = pass1.val();
					if (str.length < 6)
					{
						waAuth.throwException(pass1,true);
						$('#throw_exception').text('Длина пароля должна быть не менее 6 символов');				
						return false;
					}
					else
					{
						waAuth.throwException(false,false);
						pass1.removeClass('wa_field_error');
						if (!pass2.val())
							{
								waAuth.throwException(pass2,true);
								$('#throw_exception').text('Повторите пароль');		
								return false
							}
							else
							{
								if (pass2.val() != pass1.val())
								{
									waAuth.throwException(pass2,true);
									$('#throw_exception').text('Не верно введен повтор пароля');		
									return false
								}
								else
								{
									waAuth.throwException(false,false);
									pass2.removeClass('wa_field_error');
									$('#wa_input_type').attr('value','REGISTRATION');
									$("#wa_submit").attr('type','submit');
									return true
								}
							}
					}
				}
			}
			else
			{
				if (!pass1.val())
				{
					waAuth.throwException(pass1,true);
					$('#throw_exception').text('Введите пароль');
					return false
				}
				else
				{
					$('#wa_input_type').attr('value','AUTH');
					$("#wa_submit").attr('type','submit');	
				return true					
				}
			}
	},
	
	checkUserDB: function (wa)
	{	
			var waLogin = wa.val();
			obj = { userLogin : waLogin}
			BX.ajax.post (
				"/jewcat/checkuser.php",
				obj,
				returnedAuth
			)
			function returnedAuth(data){
				if (data == 'false')
				{
					/*window.res = 0;*/
					var res = 0;
			/*		wa.addClass('wa_accept');
					console.log('А мы тебя знаем');	*/
					return false;				
				}
				else
				{
					/*window.res = 1;	*/
					var res = 1;					
					/*	wa.addClass('wa_accept');
					var pass2 =  $('#wa_user_pass2');
					if (pass2.hasClass('wa_hidd'))
						{
							pass2.removeClass('wa_hidd');
							$('#wa_auth_btn').addClass('disabar');
							$('#wa_reg_btn').removeClass('disabar');
							
						}*/
					return true;				
				}
			}
		console.log(window.res);
		/*if (result.responseText)
			{
				return true
			}
			else
			{
				return false;
			}*/
	},
	checkUser: function (wa) // функция проверки логина
	{
		var waValue = wa.val();
		var waRes = false;
			var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			var email = pattern.test(waValue); // проверка логина по паттерну e-mail
			if (email)
			{
				// waAuth.checkUserDB(wa);
			/*	if (waAuth.checkUserDB(wa))
				{	
					wa.addClass('wa_accept');
					console.log('А мы тебя знаем');
				}
				else
				{*/
				wa.addClass('wa_accept');
				/*	wa.addClass('wa_accept');
					var pass2 =  $('#wa_user_pass2');
					if (pass2.hasClass('wa_hidd'))
						{
							pass2.removeClass('wa_hidd');
							$('#wa_auth_btn').addClass('disabar');
							$('#wa_reg_btn').removeClass('disabar');
							
						}*/
					return true;
			/*	}*/
			}
			else
			{
				console.log('неудачно');
				return false
			}
		
	}
}
$(".wa_main_field").on('focusout',function () {
	var waFocusId = $(this).attr('id');
	var waValue = $(this).val();
	if (waFocusId == 'wa_user_login') // если фокус пропал с поля логина
	{
			if (waValue)
			{
					if (!waAuth.checkUser($(this))) // return функции проверки логина
						{
						 waAuth.throwException($(this),true);
						 $('#throw_exception').text('Введите логин.  Пример customer@awesomemail.com');
						}
						else
						{
							waAuth.throwException(false,false)
						}
			}
	}
});
$(".wa_auth_btn").on('click',function (et) {
	var btn = $(this);
	var btnType = $(this).attr('id');
	if (btn.hasClass('disabar'))
		{
			waAuth.clickCheck(btn);
		}
		else
		{
			$('#WAAUTH').submit();
		}
});
$('.wa_main_field').on('click', function () {
	var waClickId = $(this).attr('id');
	var waT = $(this);
	waT.removeClass('wa_accept');
	waT.removeClass('wa_field_error');
});
/*$('#WAAUTH').keydown(function(){

	alert(eventObject.which);
});*/
$('.bxreg_window .cross2').on('click',function ()
{
	$('.bxreg_shadow').hide();
});
$('#WAAUTH').submit(function (e) {

		var waValue = $("#wa_user_login").val();
		if (waValue)
			{
				if (!$("#wa_user_login").hasClass('wa_accept'))
					{
						waAuth.throwException($("#wa_user_login"),true);
						 $('#throw_exception').text('Введите логин.  Пример customer@awesomemail.com');	
						return false;
					}
					else
					{
						waAuth.throwException(false,false);
						if (!waAuth.checkPass(true))
						{
							return false;
						}
						else
						{
							return true;
						}
					}
			}
			else
			{
						waAuth.throwException($("#wa_user_login"),true);
						 $('#throw_exception').text('Введите логин.  Пример customer@awesomemail.com');		
						return false;
			}
		/*	return false;*/
});
</script>