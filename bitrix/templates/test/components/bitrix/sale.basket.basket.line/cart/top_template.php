<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>


	<?if ($arParams['SHOW_AUTHOR'] == 'Y'):?>
		<div class="bx-basket-block">
			<i class="fa fa-user"></i>
			<?if ($USER->IsAuthorized()):
				$name = trim($USER->GetFullName());
				if (! $name)
					$name = trim($USER->GetLogin());
				if (strlen($name) > 15)
					$name = substr($name, 0, 12).'...';
				?>
				<a href="<?=$arParams['PATH_TO_PROFILE']?>"><?=$name?></a>
				&nbsp;
				<a href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
			<?else:?>
				<a href="<?=$arParams['PATH_TO_REGISTER']?>?login=yes"><?=GetMessage('TSB1_LOGIN')?></a>
				&nbsp;
				<a href="<?=$arParams['PATH_TO_REGISTER']?>?register=yes"><?=GetMessage('TSB1_REGISTER')?></a>
			<?endif?>
		</div>
	<?endif?>
<?
$url = $_SERVER["REQUEST_URI"];
if (strpos($url,'cart') != false)
{
	$smb = 'smb_false';
}
else
{
	$smb = 'smb_true';
}
?>
	<div class="basket">

		<span>В корзине <a href="<?=$arParams['PATH_TO_BASKET']?>">
		<?if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')):?>
			<?=$arResult['NUM_PRODUCTS']?>
		<?endif?>		
		</a>
		товаров
		</span>
		<a href="<?echo SITE_DIR?>personal/cart/" class="button <?//=$smb?>">Корзина</a>
	</div>

