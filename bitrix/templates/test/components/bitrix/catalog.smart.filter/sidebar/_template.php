<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);
?>


<?
/*foreach($arResult["ITEMS"] as $key=>$arItem)
{
	echo '<pre>';
	print_r($arItem);
	echo '</pre>';
}*/
?>
	<?
$curDir = explode('/',$APPLICATION->GetCurDir());
$bread[0]["PATH"] = $curDir[0].'/';
$bread[0]["NAME"] = "Главная страница";
$bread[1]["PATH"] = $bread[0]["PATH"].'catalog/jewelry/';
$bread[1]["NAME"] = "Ювелирный каталог";
$curType = $curDir[3];
$curSubType = $curDir[4];
	$firstSec = array();
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 2, "IBLOCK_ID" => 10, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
	   if (!empty($curType) && $curType == $arSect["CODE"])
		{
			$bread[2]["NAME"] = $arSect["NAME"];
			$bread[2]["PATH"] = $bread[1]["PATH"]."".$arSect["CODE"]."/";
		}
        $firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
		$firstSec[$i]["CODE"] = $arSect["CODE"];
				$Sec1 = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 3, "SECTION_ID" => $arSect["ID"]), array("ID","NAME","CODE"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				   if (!empty($curSubType) && $curSubType == $arSect1["CODE"])
					{
						$bread[3]["NAME"] = $arSect1["NAME"];
						$bread[3]["PATH"] = $bread[2]["PATH"]."".$arSect1["CODE"]."/";
					}
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
					$firstSec[$i]["SUB"][$j]["CODE"] = $arSect1["CODE"];
			   }		   
   }
  
?>
<div class="breadcrumb">
	<div class="bread_inn">
	<?$arr_count = count($bread);?>
		<a href="<?=$bread[0]["PATH"]?>"><span><?=$bread[0]["NAME"]?></span></a>
		<?if ($arr_count == 2):?>
		<span><?=$bread[1]["NAME"]?></span>
		<?else:?>
			<a href="<?=$bread[1]["PATH"]?>"><span><?=$bread[1]["NAME"]?></span></a>
		<?endif?>
		<?if (!empty($curType)):?>
			<?if ($arr_count == 3):?>
			<span><?=$bread[2]["NAME"]?></span>
			
			<?else:?>
			<a href="<?=$bread[2]["PATH"]?>"><span><?=$bread[2]["NAME"]?></span></a>	
			<?endif?>
			<?if (!empty($curSubType)):?>
				<?if ($arr_count == 4):?>
					<span><?=$bread[3]["NAME"]?></span>
				<?else:?>
					
					<a href="<?=$bread[3]["PATH"]?>"><span><?=$bread[3]["NAME"]?></span></a>
				<?endif;?>
			<?endif?>
		<?endif?>
	</div>
</div>
<div class="content">
	<div class="workarea">
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.small", 
	"ajax_basket", 
	array(
		"COMPONENT_TEMPLATE" => "ajax_basket",
		"PATH_TO_BASKET" => "/personal/basket.php",
		"PATH_TO_ORDER" => "/personal/order.php",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N"
	),
	false
);?>	
<?
/*	$bcCat = '';
	$bcCat .= '<div class="breadcrumb"><div class="bread_inn">';
	$arr_count = count($bread);
	$bcCat .= '<a href="'.$bread[0]["PATH"].'"><span>'.$bread[0]["NAME"].'</span></a>';
		if ($arr_count == 2)
		{
			$bcCat .= '<span>'.$bread[1]["NAME"].'</span>';
		}
		else
		{
			$bcCat .= '<a href="'.$bread[1]["PATH"].'"><span>'.$bread[1]["NAME"].'</span></a>';
		}
		if (!empty($curType))
		{
			if ($arr_count == 3)
			{
				$bcCat .= '<span>'.$bread[2]["NAME"].'</span>';
			}
			else
			{
				$bcCat .= '<a href="'.$bread[2]["PATH"].'"><span>'.$bread[2]["NAME"].'</span></a>';
			}
			if (!empty($curSubType))
			{
				if ($arr_count == 4)
				{
					$bcCat .= '<span>'.$bread[3]["NAME"].'</span>';
				}
				else
				{
					$bcCat .= '<a href="'.$bread[3]["PATH"].'"><span>'.$bread[3]["NAME"].'</span></a>';
				}
			}
		}
	$bcCat .= '</div></div>';*/?>
<?/*$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"catalog_bc", 
	array(
		"COMPONENT_TEMPLATE" => "catalog_bc",
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	),
	$component
);*/?>
<!-- <a href="/catalog/jewelry/bracelets/">Ссылка</a> -->
<div class="bx_filter <?=$templateData["TEMPLATE_CLASS"]?> <?if ($arParams["FILTER_VIEW_MODE"] == "horizontal"):?>bx_horizontal<?endif?>">
<div class="bx_filter_section">
<!-- <div class="bx_filter_title"><?echo GetMessage("CT_BCSF_FILTER_TITLE")?></div> -->
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">

<?foreach($arResult["HIDDEN"] as $arItem):?>
	<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
<?endforeach;
//prices
foreach($arResult["ITEMS"] as $key=>$arItem)
{
	/*$key = $arItem["ENCODED_ID"];*/

	if(isset($arItem["PRICE"])):
		if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
			continue;

		$precision = 2;
		if (Bitrix\Main\Loader::includeModule("currency"))
		{
			$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
			$precision = $res['DECIMALS'];
		}
		?>
		<div class="bx_filter_parameters_box active">
			<span class="bx_filter_container_modef"></span>
			<div class="bx_filter_parameters_box_title" onclick="smartFilter.hideFilterProps(this)"><?=$arItem["NAME"]?></div>
			<div class="bx_filter_block">
				<div class="bx_filter_parameters_box_container">
					<div class="bx_filter_parameters_box_container_block">
						<div class="bx_filter_input_container">
							<input
								class="min-price"
								type="text"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								/>
						</div>
					</div>
					<div class="bx_filter_parameters_box_container_block">
						<div class="bx_filter_input_container">
							<input
								class="max-price"
								type="text"
								name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								/>
						</div>
					</div>
					<div style="clear: both;"></div>

					<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
						<?
						$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
						$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
						$price1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
						$price2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
						$price3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
						$price4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
						$price5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
						?>
						<div class="bx_ui_slider_part p1"><span><?=$price1?></span></div>
						<div class="bx_ui_slider_part p2"><span><?=$price2?></span></div>
						<div class="bx_ui_slider_part p3"><span><?=$price3?></span></div>
						<div class="bx_ui_slider_part p4"><span><?=$price4?></span></div>
						<div class="bx_ui_slider_part p5"><span><?=$price5?></span></div>

						<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
						<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
						<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
						<div class="bx_ui_slider_range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
							<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
							<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
						</div>
					</div>
					<div style="opacity: 0;height: 1px;"></div>
				</div>
			</div>
		</div>
	<?
	$arJsParams = array(
		"leftSlider" => 'left_slider_'.$key,
		"rightSlider" => 'right_slider_'.$key,
		"tracker" => "drag_tracker_".$key,
		"trackerWrap" => "drag_track_".$key,
		"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
		"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
		"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
		"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
		"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
		"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
		"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
		"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
		"precision" => $precision,
		"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
		"colorAvailableActive" => 'colorAvailableActive_'.$key,
		"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
	);
	?>
		<script type="text/javascript">
			BX.ready(function(){
				window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
			});
		</script>
	<?endif;
}?>
<?
//not prices
/*$it = -1;
$arItem = $arResult["ITEMS"][138];

$curDir = explode('/',$APPLICATION->GetCurDir());
$curType = $curDir[3];
$flag_over = false;*/?>
<!-- <div class="jew_type">
<div class="bx_filter_parameters_box_title" onclick=""><span></span><?=$arItem["NAME"]?></div>
<?foreach($firstSec as $value):?>
	<div class="parent">
		<a href="/catalog/jewelry/<?=$value["CODE"]?>/"><input <?echo ($value["CODE"] == $curType) ? 'checked' : '';?> type="checkbox"><?=$value["NAME"]?></a>
		<?if ($value["CODE"] == $curType):?>
		
		<?
		$flag_over = true;
		$section_rule = $value["ID"];

  				$arFilter = array("IBLOCK_ID" => 6);
				$arSelect = array("ID","IBLOCK_ID","NAME","PROPERTY_JM_FILTER_PROPS","PROPERTY_JM_FILTER_STOP_LIST","PROPERTY_JM_FILTER_MIN");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
							$flag = false;
							$arFields = $ob->GetFields();
							foreach ($arFields["PROPERTY_JM_FILTER_PROPS_VALUE"] as $key => $value)
								{
									if ($value == $section_rule)
										{
											foreach ($arFields["PROPERTY_JM_FILTER_STOP_LIST_VALUE"] as $kk => $stop)
											{
												$JSONResult["STOP"][] = $stop;
											}
											foreach ($arFields["PROPERTY_JM_FILTER_MIN_VALUE"] as $kk => $min)
											{
												$JSONResult["MIN"][] = $min;
											}
										}
								}
							
						}
		?>
			<div class="children">
	<div class="bx_filter_parameters_box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
	<span class="bx_filter_container_modef"></span>

	<?if ($arItem["FILTER_HINT"] <> ""):?>
		<div class="bx_filter_parameters_box_hint" id="item_title_hint_<?echo $arItem["ID"]?>"></div>
		<script type="text/javascript">
			new top.BX.CHint({
				parent: top.BX("item_title_hint_<?echo $arItem["ID"]?>"),
				show_timeout: 10,
				hide_timeout: 200,
				dx: 2,
				preventHide: true,
				min_width: 250,
				hint: '<?= CUtil::JSEscape($arItem["FILTER_HINT"])?>'
			});
		</script>
	<?endif?>
	<div class="bx_filter_block">
	<div class="bx_filter_parameters_box_container">
			<?foreach($arItem["VALUES"] as $val => $ar):?>
				<?$count++;?>
				<?if ($count == 6):?>
				<?$spoil_flag = true;?>
				<div class="spoil_title">Еще...</div>
				<div class="spoil">
				<?endif?>
				<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
										<span class="bx_filter_input_checkbox">
											<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												
												onclick="smartFilter.click(this)"
												/>
											<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
										</span>
				</label>
			<?endforeach;?>
			<?if ($spoil_flag == true) echo '</div><div class="spoil_close">Скрыть</div>';?>
	</div>
	</div>
	</div>				
			</div>
		<?endif;?>
	</div>
<?endforeach;?>
</div> -->




<?
//print_r($arItem);
//foreach($arResult["ITEMS"][138] =)
$fl = -1;
foreach($arResult["ITEMS"] as $key=>$arItem)
{
	$fl++;
if ($fl == 2 )
{
$curDir = explode('/',$APPLICATION->GetCurDir());
$curType = $curDir[3];
$curSubType = $curDir[4];
?>
<div class="jew_type">
<div class="bx_filter_parameters_box_title" onclick=""><span></span>Тип изделия</div>
<?foreach($firstSec as $value):?>
<?$flag_over = false;?>
	<div class="parent">
	<?$href = ($value["CODE"] == $curType) ? '/catalog/jewelry/' : '/catalog/jewelry/'.$value["CODE"].'/';?>
		<a class="jew_parent" href="<?=$href?>"><input <?echo ($value["CODE"] == $curType) ? 'checked' : '';?> type="checkbox"><?=$value["NAME"]?></a>
		<?if ($value["CODE"] == $curType) $flag_over = true;?>
		<?if ($flag_over):?>
		<div class="children">
			<div class="bx_filter_parameters_box ">
				<div class="bx_filter_block">
					<div class="bx_filter_parameters_box_container">
						<?foreach ($value["SUB"] as $item):?>
							<label class="bx_filter_param_label">
								<span class="bx_filter_input_checkbox">
									<input <?echo ($item["CODE"] == $curSubType) ? 'checked' : '';?> type="checkbox">
									<a href="/catalog/jewelry/<?=$value["CODE"]?>/<?=$item["CODE"]?>/" class="bx_filter_param_text"><?=$item["NAME"]?></a>
								</span>
							</label>
						<?endforeach?>
					</div>
				</div>
			</div>
		</div>
		<?endif?>
	</div>	
<?endforeach;?>
</div>
<?}
$count = 0;
$spoil_flag = false;
	if(
		empty($arItem["VALUES"])
		|| isset($arItem["PRICE"])
	)
		continue;

	if (
		$arItem["DISPLAY_TYPE"] == "A"
		&& (
			$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
		)
	)
		continue;
	?>
<?if ($fl > 2):?>
	<div class="bx_filter_parameters_box <?//=$show;?> <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
	<span class="bx_filter_container_modef"></span>
	<div class="bx_filter_parameters_box_title" onclick=""><span></span><?=$arItem["NAME"]?></div>
	<?if ($arItem["FILTER_HINT"] <> ""):?>
		<div class="bx_filter_parameters_box_hint" id="item_title_hint_<?echo $arItem["ID"]?>"></div>
		<script type="text/javascript">
			new top.BX.CHint({
				parent: top.BX("item_title_hint_<?echo $arItem["ID"]?>"),
				show_timeout: 10,
				hide_timeout: 200,
				dx: 2,
				preventHide: true,
				min_width: 250,
				hint: '<?= CUtil::JSEscape($arItem["FILTER_HINT"])?>'
			});
		</script>
	<?endif?>
	<div class="bx_filter_block">
	<div class="bx_filter_parameters_box_container">
	<?
	$arCur = current($arItem["VALUES"]);
	switch ($arItem["DISPLAY_TYPE"])
	{
		case "A"://NUMBERS_WITH_SLIDER
			?>
			<div class="bx_filter_parameters_box_container_block">
				<div class="bx_filter_input_container">
					<input
						class="min-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div>
			</div>
			<div class="bx_filter_parameters_box_container_block">
				<div class="bx_filter_input_container">
					<input
						class="max-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div>
			</div>
			<div style="clear: both;"></div>

			<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
				<?
				$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
				$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
				$value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
				$value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
				$value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
				$value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
				$value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
				?>
				<div class="bx_ui_slider_part p1"><span><?=$value1?></span></div>
				<div class="bx_ui_slider_part p2"><span><?=$value2?></span></div>
				<div class="bx_ui_slider_part p3"><span><?=$value3?></span></div>
				<div class="bx_ui_slider_part p4"><span><?=$value4?></span></div>
				<div class="bx_ui_slider_part p5"><span><?=$value5?></span></div>

				<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
				<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
				<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
				<div class="bx_ui_slider_range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
					<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
					<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
				</div>
			</div>
		<?
		$arJsParams = array(
			"leftSlider" => 'left_slider_'.$key,
			"rightSlider" => 'right_slider_'.$key,
			"tracker" => "drag_tracker_".$key,
			"trackerWrap" => "drag_track_".$key,
			"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
			"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
			"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
			"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
			"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
			"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
			"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
			"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
			"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
			"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
			"colorAvailableActive" => 'colorAvailableActive_'.$key,
			"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
		);
		?>
			<script type="text/javascript">
				BX.ready(function(){
					window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
				});
			</script>
			<?
			break;
		case "B"://NUMBERS
			?>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container">
					<input
						class="min-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div></div>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container">
					<input
						class="max-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div></div>
			<?
			break;
		case "G"://CHECKBOXES_WITH_PICTURES
			?>
			<?foreach ($arItem["VALUES"] as $val => $ar):?>
			<input
				style="display: none"
				type="checkbox"
				name="<?=$ar["CONTROL_NAME"]?>"
				id="<?=$ar["CONTROL_ID"]?>"
				value="<?=$ar["HTML_VALUE"]?>"
				<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
				/>
			<?
			$class = "";
			if ($ar["CHECKED"])
				$class.= " active";
			if ($ar["DISABLED"])
				$class.= " disabled";
			?>
			<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label dib<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
										<span class="bx_filter_param_btn bx_color_sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
			</label>
		<?endforeach?>
			<?
			break;
		case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
			?>
			<?foreach ($arItem["VALUES"] as $val => $ar):?>
			<input
				style="display: none"
				type="checkbox"
				name="<?=$ar["CONTROL_NAME"]?>"
				id="<?=$ar["CONTROL_ID"]?>"
				value="<?=$ar["HTML_VALUE"]?>"
				<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
				/>
			<?
			$class = "";
			if ($ar["CHECKED"])
				$class.= " active";
			if ($ar["DISABLED"])
				$class.= " disabled";
			?>
			<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
										<span class="bx_filter_param_btn bx_color_sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
										<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
											if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
												?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
											endif;?></span>
			</label>
		<?endforeach?>
			<?
			break;
		case "P"://DROPDOWN
			$checkedItemExist = false;
			?>
			<div class="bx_filter_select_container">
				<div class="bx_filter_select_block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
					<div class="bx_filter_select_text" data-role="currentOption">
						<?
						foreach ($arItem["VALUES"] as $val => $ar)
						{
							if ($ar["CHECKED"])
							{
								echo $ar["VALUE"];
								$checkedItemExist = true;
							}
						}
						if (!$checkedItemExist)
						{
							echo GetMessage("CT_BCSF_FILTER_ALL");
						}
						?>
					</div>
					<div class="bx_filter_select_arrow"></div>
					<input
						style="display: none"
						type="radio"
						name="<?=$arCur["CONTROL_NAME_ALT"]?>"
						id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
						value=""
						/>
					<?foreach ($arItem["VALUES"] as $val => $ar):?>
						<input
							style="display: none"
							type="radio"
							name="<?=$ar["CONTROL_NAME_ALT"]?>"
							id="<?=$ar["CONTROL_ID"]?>"
							value="<? echo $ar["HTML_VALUE_ALT"] ?>"
							<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
							/>
					<?endforeach?>
					<div class="bx_filter_select_popup" data-role="dropdownContent" style="display: none;">
						<ul>
							<li>
								<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx_filter_param_label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
									<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
								</label>
							</li>
							<?
							foreach ($arItem["VALUES"] as $val => $ar):
								$class = "";
								if ($ar["CHECKED"])
									$class.= " selected";
								if ($ar["DISABLED"])
									$class.= " disabled";
								?>
								<li>
									<label for="<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
								</li>
							<?endforeach?>
						</ul>
					</div>
				</div>
			</div>
			<?
			break;
		case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
			?>
			<div class="bx_filter_select_container">
				<div class="bx_filter_select_block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
					<div class="bx_filter_select_text" data-role="currentOption">
						<?
						$checkedItemExist = false;
						foreach ($arItem["VALUES"] as $val => $ar):
							if ($ar["CHECKED"])
							{
								?>
								<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
								<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
							<?endif?>
								<span class="bx_filter_param_text">
														<?=$ar["VALUE"]?>
													</span>
								<?
								$checkedItemExist = true;
							}
						endforeach;
						if (!$checkedItemExist)
						{
							?><span class="bx_filter_btn_color_icon all"></span> <?
							echo GetMessage("CT_BCSF_FILTER_ALL");
						}
						?>
					</div>
					<div class="bx_filter_select_arrow"></div>
					<input
						style="display: none"
						type="radio"
						name="<?=$arCur["CONTROL_NAME_ALT"]?>"
						id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
						value=""
						/>
					<?foreach ($arItem["VALUES"] as $val => $ar):?>
						<input
							style="display: none"
							type="radio"
							name="<?=$ar["CONTROL_NAME_ALT"]?>"
							id="<?=$ar["CONTROL_ID"]?>"
							value="<?=$ar["HTML_VALUE_ALT"]?>"
							<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
							/>
					<?endforeach?>
					<div class="bx_filter_select_popup" data-role="dropdownContent" style="display: none">
						<ul>
							<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
								<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx_filter_param_label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
									<span class="bx_filter_btn_color_icon all"></span>
									<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
								</label>
							</li>
							<?
							foreach ($arItem["VALUES"] as $val => $ar):
								$class = "";
								if ($ar["CHECKED"])
									$class.= " selected";
								if ($ar["DISABLED"])
									$class.= " disabled";
								?>
								<li>
									<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
										<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
											<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
										<?endif?>
										<span class="bx_filter_param_text">
															<?=$ar["VALUE"]?>
														</span>
									</label>
								</li>
							<?endforeach?>
						</ul>
					</div>
				</div>
			</div>
			<?
			break;
		case "K"://RADIO_BUTTONS
			?>
			<label class="bx_filter_param_label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
									<span class="bx_filter_input_checkbox">
										<input
											type="radio"
											value=""
											name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
											id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
											onclick="smartFilter.click(this)"
											/>
										<span class="bx_filter_param_text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
									</span>
			</label>
			<?foreach($arItem["VALUES"] as $val => $ar):?>
			<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label" for="<? echo $ar["CONTROL_ID"] ?>">
										<span class="bx_filter_input_checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
											<input
												type="radio"
												value="<? echo $ar["HTML_VALUE_ALT"] ?>"
												name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
												/>
											<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
										</span>
			</label>
		<?endforeach;?>
			<?
			break;
		case "U"://CALENDAR
			?>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container bx_filter_calendar_container">
					<?$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
							'SHOW_INPUT' => 'Y',
							'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
							'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
							'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
							'SHOW_TIME' => 'N',
							'HIDE_TIMEBAR' => 'Y',
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);?>
				</div></div>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container bx_filter_calendar_container">
					<?$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
							'SHOW_INPUT' => 'Y',
							'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
							'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
							'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
							'SHOW_TIME' => 'N',
							'HIDE_TIMEBAR' => 'Y',
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);?>
				</div></div>
			<?
			break;
		default://CHECKBOXES
			?>
			<?$once = false;?>
			<?foreach($arItem["VALUES"] as $val => $ar):?>
				<?$count++;?>
				<?if ($count == 6):?>
				<?$spoil_flag = true;?>
				<div class="spoil_title">Еще...</div>
				<?if ($ar["CHECKED"] == true && $once == false) {$block = 'display:block'; $once = true;}?>
				<div class="spoil" style= "<?echo $block?>">
				<?endif?>
				<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
										<span class="bx_filter_input_checkbox">
											<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
												/>
											<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
										</span>
				</label>
			<?endforeach;?>
			<?if ($spoil_flag == true) echo '</div><div class="spoil_close">Скрыть</div>';?>
			<?
	}
	?>
	</div>
	<div class="clb"></div>
	</div>
	</div>
<?else:?> <!-- Опции кроме акций и новинок -->
	<div class="bx_filter_parameters_box <?//=$show;?> <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
	<span class="bx_filter_container_modef"></span>

	<div class="bx_filter_block" style="float: left; width: 15px; margin-top: 5px;">
	<div class="bx_filter_parameters_box_container">
				<?foreach($arItem["VALUES"] as $val => $ar):?>
				<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
										<span class="bx_filter_input_checkbox">
											<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
												/>
											<span class="bx_filter_param_text" style="" title="<?=$ar["VALUE"];?>"><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
										</span>
				</label>
			<?endforeach;?>
	</div>
	</div>
	<div class="bx_filter_parameters_box_title" style="float:left; border: none; font-weight:400;" onclick=""><span></span><?=$arItem["NAME"]?></div>
	<?if ($arItem["FILTER_HINT"] <> ""):?>
		<div class="bx_filter_parameters_box_hint" id="item_title_hint_<?echo $arItem["ID"]?>"></div>
		<script type="text/javascript">
			new top.BX.CHint({
				parent: top.BX("item_title_hint_<?echo $arItem["ID"]?>"),
				show_timeout: 10,
				hide_timeout: 200,
				dx: 2,
				preventHide: true,
				min_width: 250,
				hint: '<?= CUtil::JSEscape($arItem["FILTER_HINT"])?>'
			});
		</script>
	<?endif?>
	</div>
<?endif;?> <!-- акции и новинки -->
<?
}
?>
<div class="clb"></div>
<div class="bx_filter_button_box active">
	<div class="bx_filter_block">
		<div class="bx_filter_parameters_box_container">
			<input
				class="bx_filter_search_button"
				type="submit"
				id="set_filter"
				name="set_filter"
				value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
				/>
			<input
				class="bx_filter_search_reset"
				type="submit"
				id="del_filter"
				name="del_filter"
				value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
				/>
			<div class="bx_filter_popup_result <?=$arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
				<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
				<span class="arrow"></span>
				<a href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
			</div>
		</div>
	</div>
</div>
</form>
<div style="clear: both;"></div>
</div>
</div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>
<script>
	$(".spoil_title").on("click", function (){
		$(".spoil").hide();
		$(this).next().slideToggle(300);
		$(this).hide();
		$(this).next().next().show();
	});
	$(".spoil_close").on("click", function (){
		$(this).prev().slideToggle(300);
		$(this).hide();
		$(this).prev().prev().show();
	});
function geturl (sec){
			$('.bx_filter_parameters_box').hide();
		var obj = {
			param1: $(".bx_filter_popup_result a").attr("href"),
			param2: sec
		} 
		BX.ajax.loadJSON(
			"/jewcat/filter.php",
			obj,
			response
		)
		function response (data) {
					for (val of data.STOP)
					{	
						$('.bx_filter_parameters_box_title:contains("' + val +'")').parent().show();
					}
			/*		for (val of data.SMART)
					{	setTimeout(function(){
						$('.bx_filter_parameters_box_title:contains("' + val +'")').next().children().find(".disabled").hide();
						}, 1000);
					}*/

					
		}
}	
</script>