<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?echo SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--     <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
<!--	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <meta http-equiv="cleartype" content="on">
    <meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<?$APPLICATION->ShowHead()?>

	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHeadStrings();?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/jquery-1.9.1.min.js');?>
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.mobile-1.4.5.min.js');?>
	<?//$APPLICATION->AddHeadScript('/bitrix/templates/jew_market/js/main.js');?>
	<?//$APPLICATION->SetAdditionalCSS('/bitrix/templates/jew_market/fonts.css');?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/ui/assets/css/bootstrap.min.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/ui/assets/css/bootstrap.min.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/ui/css/custom-theme/jquery-ui-1.10.3.custom.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/ui/assets/css/font-awesome.min.css');?>
	<?$APPLICATION->SetAdditionalCSS('/bitrix/css/main/font-awesome.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick-theme.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/jquery.mobile-1.4.5.min.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/demo.css');?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/snap.css');?>
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/custom-theme/jquery.ui.1.10.3.ie.css">
    <![endif]-->
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/jquery-migrate-1.2.1.min.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/bootstrap.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/jquery-ui-1.10.3.custom.min.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/holder.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/google-code-prettify/prettify.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/docs.js"');?>
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick.js');?>
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slideout_def.js');?>
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/snap.js');?>
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/demo.js');?>
	<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.rs.carousel-touch.js');?>		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]> 
	<?
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/html5shiv.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/respond.min.js');
	 ?>
    <![endif]-->
    <!-- Le styles -->
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/fonts.css');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/workarea.js');?>
	
	
	<?$browser = $_SERVER['HTTP_USER_AGENT'] . "\n\n";
	$browser = ToLower($browser);
	if (Bxstrrpos($browser,'android') != 0 || Bxstrrpos($browser,'iphone') != 0 )
	{
		$GLOBALS["isMobile"] = true; 		
	}
//		$GLOBALS["isMobile"] = true; 	//для теста чтобы не влом на мобильную переключаться мобильную
	if (($GLOBALS["isMobile"]))
	{
		$isMobile = true;	
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slideout_def.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/waSMenu.js');
		$curDir = $APPLICATION->GetCurDir();
		$here = explode("/",$curDir);
/*		if ($here[1] == "offers") 
			{
				$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/waSMenuFilter.js');		
			}*/
	}
	?>
</head>
	<?$APPLICATION->ShowPanel();?>
<?
/*echo "<pre>";
print_r($_REQUEST);
echo "</pre>";*/
?>
<body id="waJewmarket">
<header class="hidden-sm hidden-xs">
	<div class="waFluid">
		<div class="container">
			<div class="row" style="background: transparent;">
				<div class="col-md-8 info" style="padding-left: 0px; padding-right: 0px;">
					<div>
					Работаем на выезде! Выбирайте и покупайте не выходя из дома.
					</div>
					<a href="">Как это работает</a>
				</div>
				<div class="col-md-4 cart" style="padding-left: 0px; padding-right: 0px;">
					<div class="text">В корзине <a href="">4</a> товаров</div>
					<div class="buttonOrange">Корзина</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row" style="margin-top: 10px; margin-bottom: 10px">
			<div class="col-xs-4 waClearPadding">
				<a href="/" class="logo">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" class="img-responsive"/>
				</a>
			</div>
			<div class="col-xs-4 waClearPadding">
				<menu class="row topMenu">
					<div class="col-xs-6">
						<li><a href="/catalog/jewelry/?set_filter=y&amp;arrFilter_135_4261170317=Y">Новинки</a></li>
						<li><a class="/delivery">Оплата и доставка</a></li>	
						<li><a href="/catalog/jewelry/?set_filter=y&amp;arrFilter_132_4261170317=Y">Акции</a></li>
					</div>
					<div class="col-xs-6">
						<li><a href="/warranty/">Гарантии и качество</a></li>
						<li><a href="/wholesale/">Оптовые продажи</a></li>
						<li><a href="/contacts/">Контакты</a></li>	
					</div>
				</menu>
			</div>
			<div class="col-xs-4 rightBlock waClearPadding">
				<div class="phone">
					+7 (812) 339 25 37
				</div>
				<a class="callBack">Заказать обратный звонок</a>
<?	$APPLICATION->IncludeComponent("bitrix:system.auth.form", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"FORGOT_PASSWORD_URL" => "",
		"USE_BACKURL" => "/about/index.php",
		"SUCCESS_PAGE" => "/about/index.php",
		"PROFILE_URL" => "/personal/",
		"REGISTER_URL" => "/catalog/",
		"SHOW_ERRORS" => "Y"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>				
<!--					<div class="authPanel">
				<form class="ifreg">
						<p>Вы вошли как:&nbsp;
						<a href="/personal/" title="Мой профиль">
							<i class="fa fa-user fa-2"></i> 
							<span class="header_auth_login" title="admin">Покупатель</span></a>
						</p>
						<p>
							<a href="/personal/" class="header_auth_partner" title="Мой профиль">Личный кабинет</a>
								<span class="exit"><i class="header_red fa fa-power-off"></i>
									<input type="submit" class="header_auth_partner" name="logout_butt" value="Выйти">
								</span>
						</p>
					</form>
				</div> -->
			</div>
		</div>
	</div>
	<div class="waFluid" style="background: #fff">
		<div class="container">	
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-xs-2 waClearPadding">
					<div id="title-search" class="bx-searchtitle">
						<form action="/search/index.php">
							<div class="bx-input-group">
								<input id="title-search-input" type="text" name="q" value="" placeholder="Поиск..." autocomplete="off" class="bx-form-control">
								<span class="bx-input-group-btn">
									<button class="btn btn-default" type="submit" name="s"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<div class="col-xs-10 waClearPadding">
					<menu class="navMenu">
						<li class="scrollout"><a href="">О JEW MARKET</a></li>
						<li><a href="">ЮВЕЛИРНЫЕ УКРАШЕНИЯ</a></li>
						<li><a href="">ПОДОБРАТЬ ПОДАРОК</a></li>
						<li class="scrollout"><a href="">ПАРТНЕРАМ</a></li>
						<li class="scrollout"><a href="">ГДЕ КУПИТЬ</a></li>
					</menu>
				</div>
			</div>	
		</div>
	</div>
<div class="waFluid waCatSections">
	<div class="container">	
		<div class="row" style="background: transparent">
			<div class="col-xs-12 waClearPadding">
				<menu class="firstLvl">
					<li>
						<a href="">Кольца</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Печатки</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Серьги</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Пуссеты</a></li>
									<li><a href="">Каффы</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Подвески</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Шармы</a></li>
									<li><a href="">На браслет и цепь</a></li>
									<li><a href="">Знаки зодиака</a></li>	
									<li><a href="">Кресты</a></li>

								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Браслеты</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Браслет для шармов-кожаный</a></li>
									<li><a href="">Браслет для шармов-цепь</a></li>
									<li><a href="">Жесткие</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Цепи</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Ромб</a></li>
									<li><a href="">Корда</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Брошь, булавка</a>
					</li>
					<li>
						<a href="">Сувениры</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Ложка загребушка</a></li>
									<li><a href="">Монетка</a></li>
									<li><a href="">Ложка на крестины</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Оборудование CARAMEL</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Подставки</a></li>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
				</menu>
			</div>
		</div>
	</div>
</div>
</header>
<?if ($isMobile):?>
<?if (CModule::IncludeModule('iblock')){
					$curDir = $APPLICATION->GetCurDir();
					$curType = $curDir[1];
					$curSubType = $curDir[2];
					$secPath = array();
					$secParent = array();
					//print_r($curType);
					$arFilter1 = array('IBLOCK_ID' => 29, 'ACTIVE' => 'Y', ">=DEPTH_LEVEL" => 2); 
					$arSelect1 = array("ID","NAME","CODE","SECTION_PAGE_URL", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
					$rsSection = CIBlockSection::GetTreeList($arFilter1, $arSelect1); 
					while($arSection = $rsSection->GetNext())
						{
//							echo ' - '.$arSection['SECTION_PAGE_URL'].'<br>';
//							$Section[] = $arSection['ID'];
							$kid = $arSection["ID"];
							$sid = $arSection["IBLOCK_SECTION_ID"];
/*							$secCount = trim($arSection["SECTION_PAGE_URL"], '/');
							$secCount = explode('/',$secCount);
							$secNum = count($secCount);*/
							$arSection["SECTION_PAGE_URL"] = str_replace("catalog","offers", $arSection["SECTION_PAGE_URL"]);
//							$secCount = 
							if ($arSection["DEPTH_LEVEL"] == 2)							
							{
								$secPath[$kid]["NAME"] = $arSection["NAME"];
								$secPath[$kid]["ID"] = $kid;
								$secPath[$kid]["SECTION_PAGE_URL"] = $arSection["SECTION_PAGE_URL"];
								$secPath[$kid]["CHECKED"] =  ($curDir == $arSection["SECTION_PAGE_URL"]) ? "checked" : "" ;
								if ($secPath[$kid]["CHECKED"] != "")
								{
									$path = explode("/",$secPath[$kid]["SECTION_PAGE_URL"]);
									$path = str_replace($arSection['CODE'].'/',"",$secPath[$kid]["SECTION_PAGE_URL"]);
									$name = $secPath[$kid]["NAME"];
								}
							}
							else
							{
								$secPath[$sid]["SUB"][$kid]["NAME"] = $arSection["NAME"];
								$secPath[$sid]["SUB"][$kid]["ID"] = $kid;
								$secPath[$sid]["SUB"][$kid]["SECTION_PAGE_URL"] = $arSection["SECTION_PAGE_URL"];
								if ($curDir == $arSection["SECTION_PAGE_URL"])
								{
									$secPath[$sid]["SUB"][$kid]["CHECKED"] = "checked";
									$secPath[$sid]["CHECKED"] = "checked";
									$path = explode("/",$secPath[$sid]["SUB"][$kid]["SECTION_PAGE_URL"]);
									$path = str_replace($arSection['CODE'].'/',"",$secPath[$sid]["SUB"][$kid]["SECTION_PAGE_URL"]);
									$name = $secPath[$sid]["SUB"][$kid]["NAME"];
								}
/*								$secPath[$sid]["SUB"][$kid]["CHECKED"] =  ($curDir == $arSection["SECTION_PAGE_URL"]) ? "checked" : "" ;
								$secPath[$kid]["CHECKED"]*/
							}
							
							//$secPath[] = $arSection
/*							echo "<pre>";
							print_r($secPath);
							echo "</pre>";*/
						}
	}
	
	?>
<div class="container visible-xs visible-sm" id="topNativeMenu">
	<div class="row visible-xs headMobile">
		<div class="col-xs-12 clearRowPadd">
				<div class="mobileMenu">
					<div class="link"><a href="/" class="logoMobile">Jewmarket</a></div>
					<div class="waNavIcoBlock" id="waNavIcoBlockID">				
							<i class="fa fa-bars" aria-hidden="true"></i>		
					</div>
					<div class="link"><a href="<?=$path?>" class="getBackMobile">< <?=$name?></a></div>
				</div>
		</div>
	</div>
	<div class="row hidden-xs">
		
	</div>
</div>
<!-- <div class="fxMenu">
</div> -->
<?//if ( strpos($APPLICATION->GetCurPage(),"offers") != false ):?>
<!--<nav id="menu" class="waSMenu">
	
</nav> -->
<?//else:?>

<nav id="menu" class="waSMenu">
<div id="waInnerShadow"></div>
	<div class="topFix" id="topFixID">
		<form>
				<input type="text" name="waSearchText" placeholder="Поиск">
				<i class="fa fa-search" aria-hidden="true"></i>
				<input type="submit" value="">
		</form>
		<div class="cancel">Отменить</div>
		<div class="waNavIco">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</div>
	</div>
		<div class="club">
		<table>
			<tr>
				<td rowspan="2">
					<div class="uPhoto">
						<i class="fa fa-user-plus" aria-hidden="true"></i>
						<img src="" />
					</div>				
				</td>
				<td>
					<div class="uName"><a href="">Безымянный <br /> <small>редактировать ...</small></a></div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="uLogin">
						<a href="" class="vkontakte bx-authform-social-icon"></a>
						<a href="" class="facebook bx-authform-social-icon"></a>
						<a href="" class="twitter bx-authform-social-icon"></a>
						<a href="" class="plus bx-authform-social-icon" style="">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<span>1</span>
						</a>
						<!-- Личный кабинет <span>Сообщение : 1</span> -->
					</div>				
				</td>
			</tr>
		</table>
		</div>
<?
/*$curDir = explode('/',$APPLICATION->GetCurDir());
$bread[0]["PATH"] = $curDir[0].'/';
$bread[0]["NAME"] = "Главная страница";
$bread[1]["PATH"] = $bread[0]["PATH"].'catalog/jewelry/';
$bread[1]["NAME"] = "Ювелирный каталог";
$prefix = "/catalog/jewelry/";
$arWASecTitle = array (
	"Тип изделия",
	"Технология",
	"Вставка",
	"&nbsp;"
);
$sLvlCount = 4;
$arWASecFilter = array(
	"/?set_filter=y&arrFilter_163_3653329462=Y",
	"/?set_filter=y&arrFilter_163_1233026983=Y",
	"/?set_filter=y&arrFilter_163_1048137521=Y",
	"/?set_filter=y&arrFilter_163_1589561044=Y",
);
if (CModule::IncludeModule('iblock')){
$curType = $curDir[3];
$curSubType = $curDir[4];
	$firstSec = array();
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 2, "IBLOCK_ID" => 29, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
	   if (!empty($curType) && $curType == $arSect["CODE"])
		{
			$bread[2]["NAME"] = $arSect["NAME"];
			$bread[2]["PATH"] = $bread[1]["PATH"]."".$arSect["CODE"]."/";
		}
        $firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
		$firstSec[$i]["CODE"] = $arSect["CODE"];
				$Sec1 = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 3, "SECTION_ID" => $arSect["ID"]), array("ID","NAME","CODE"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				   if (!empty($curSubType) && $curSubType == $arSect1["CODE"])
					{
						$bread[3]["NAME"] = $arSect1["NAME"];
						$bread[3]["PATH"] = $bread[2]["PATH"]."".$arSect1["CODE"]."/";
					}
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
					$firstSec[$i]["SUB"][$j]["CODE"] = $arSect1["CODE"];
			   }		   
   }
}
*/?>
<?	
/*if (CModule::IncludeModule('iblock')){
					$curDir = $APPLICATION->GetCurDir();
					$curType = $curDir[1];
					$curSubType = $curDir[2];
					$secPath = array();
					$secParent = array();
					//print_r($curType);
					$arFilter1 = array('IBLOCK_ID' => 29, 'ACTIVE' => 'Y', ">=DEPTH_LEVEL" => 2); 
					$arSelect1 = array("ID","NAME","CODE","SECTION_PAGE_URL", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
					$rsSection = CIBlockSection::GetTreeList($arFilter1, $arSelect1); 
					while($arSection = $rsSection->GetNext())
						{
//							echo ' - '.$arSection['SECTION_PAGE_URL'].'<br>';
//							$Section[] = $arSection['ID'];
							$kid = $arSection["ID"];
							$sid = $arSection["IBLOCK_SECTION_ID"];
							$arSection["SECTION_PAGE_URL"] = str_replace("catalog","offers", $arSection["SECTION_PAGE_URL"]);
//							$secCount = 
							if ($arSection["DEPTH_LEVEL"] == 2)							
							{
								$secPath[$kid]["NAME"] = $arSection["NAME"];
								$secPath[$kid]["ID"] = $kid;
								$secPath[$kid]["SECTION_PAGE_URL"] = $arSection["SECTION_PAGE_URL"];
								$secPath[$kid]["CHECKED"] =  ($curDir == $arSection["SECTION_PAGE_URL"]) ? "checked" : "" ;
							}
							else
							{
								$secPath[$sid]["SUB"][$kid]["NAME"] = $arSection["NAME"];
								$secPath[$sid]["SUB"][$kid]["ID"] = $kid;
								$secPath[$sid]["SUB"][$kid]["SECTION_PAGE_URL"] = $arSection["SECTION_PAGE_URL"];
								if ($curDir == $arSection["SECTION_PAGE_URL"])
								{
									$secPath[$sid]["SUB"][$kid]["CHECKED"] = "checked";
									$secPath[$sid]["CHECKED"] = "checked";
								}
							}
						}
	}*/
?>
<!--	`	<div class="waSMMTest">
			<div class="waSMMTitle">Проверка</div>
			<a>Появилось</a>
		</div> -->
		<menu class="catalog">
				<?foreach ($secPath as $k => $v):?>
					<li>
						<?$chevron = (!empty($v["CHECKED"])) ? "down" : "right" ;?>
						<a class="waSMMainCatOpen to-<?=$chevron;?>"
						   href="<?=$v["SECTION_PAGE_URL"]?>"
						   data-secid="cat_<?=$v["ID"]?>">
						   <?=$v["NAME"]?>
						   <?if (!empty($v["SUB"])):?>
								<i class="fa fa-chevron-<?=$chevron;?>" aria-hidden="true"></i>
						   <?endif?>
						</a>
						<?if ($chevron == "down" && !empty($v["SUB"])):?>
							<ul class="waSMSubCatMenu waSMSubCatShow">
								<?foreach ($v["SUB"] as $key => $sub):?>
								<?$chb = (!empty($sub["CHECKED"])) ? "check-" : "" ;?>
									<li>
										<a href="<?=$sub["SECTION_PAGE_URL"]?>" class="<?=$chb?>wa-active">

											<i class="fa fa-<?=$chb?>square-o" aria-hidden="true"></i>
										<?=$sub["NAME"]?>
										</a>
									</li>
								<?endforeach;?>
							</ul>
						<?endif;?>
						<?if ($chevron == "down"):?>
							<ul class="waSMSubFilterMenu waSMSubCatShow">
								<li class="waSMFName" data-filter="465" onclick="waJSMM.clickParent(this)">
									<div class="fpic">
										<i class="fa fa-venus-mars" aria-hidden="true"></i>	
										<span class="fname">ДЛЯ КОГО</span>
															
										<span id="count_465" class="fcount"></span>							
									</div>		
									<div class="finput">не выбрано</div>									
									<ul class="subMenu" id="m111">
									</ul>
								</li>
								<li class="waSMFName" data-filter="459" onclick="waJSMM.clickParent(this)">
									<div class="fpic">
										<i class="fa fa-eyedropper" aria-hidden="true"></i>	
										<span class="fname">ЦВЕТ МЕТАЛЛА</span>
															
										<span id="count_459" class="fcount"></span>							
									</div>		
									<div class="finput">не выбрано</div>									
									<ul class="subMenu">
									</ul>
								</li>
								<li class="waSMFName" data-filter="463" onclick="waJSMM.clickParent(this)">
									<div class="fpic">
										<i class="fa fa-flask" aria-hidden="true"></i>
										<span class="fname">ТЕХНОЛОГИЯ</span>
																
										<span id="count_463" class="fcount"></span>							
									</div>	
									<div class="finput">не выбрано</div>									
									<ul class="subMenu">
									</ul>
								</li>
								<li class="waSMFName" data-filter="461" onclick="waJSMM.clickParent(this)">
									<div class="fpic">
										<i style="left:10px" class="fa fa-info" aria-hidden="true"></i>	
										<span class="fname">ТЕМАТИКА</span>
															
										<span id="count_461" class="fcount"></span>							
									</div>	
									<div class="finput">не выбрано не выбрано</div>
									<ul class="subMenu">
									</ul>
								</li>
								<li class="waSMFName" data-filter="488" onclick="waJSMM.clickParent(this)">
									<div class="fpic">
										<i class="fa fa-diamond" aria-hidden="true"></i>
										<span class="fname">ВСТАВКА</span>
						
										<span id="count_488" class="fcount"></span>							
									</div>				
									<div class="finput">не выбрано</div>									
									<ul class="subMenu">
									</ul>
								</li>
							</ul>
						<?endif?>
					</li>
				<?endforeach;?>
		</menu>
		<!--
		<menu class="catalog" style="margin-top: 15px;">
				<li><a class="waSMMainCatOpen" href="/offers/jewelry/koltsa/">Кольца<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
					<?if (strpos($APPLICATION->GetCurDir(),"koltsa") != 0):?>
					<ul class="waSMSubCatMenu waSMSubCatShow">
						<li><a href=""><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Печатки</a></li>
					</ul>
					<ul class="waSMSubFilterMenu waSMSubCatShow">
						<li class="waSMFName" data-filter="463"><a href=""><i class="fa fa-plus-square" aria-hidden="true"></i>Технология <div></div><span></span></a>
							<ul>
							</ul>
						</li>
						<li class="waSMFName" data-filter="465"><a href=""><i class="fa fa-plus-square" aria-hidden="true"></i>Для кого<div></div><span></span></a>
							<ul>
							</ul>							
						</li>
					</ul>
					<?endif?>
				</li>
				<li><a href="">Серьги<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					<?if (strpos($APPLICATION->GetCurDir(),"sergi") != 0):?>
					<ul class="waSMSubCatMenu">
						<li><a href="">Пуссеты</a></li>
						<li><a href="">Каффы</a></li>
					</ul>					
					<?endif?>
				</li>
				<li><a href="">Подвески<i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
				<li><a href="">Браслеты<i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
				<li><a href="">Цепи<i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
				<li><a href="">Сувениры<i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
		</menu> -->
</nav>
<?$isMobileJS = array();
$isMobileJS["ON"] = true;
?>
<?endif;?>
<div id="ppanel" class="container">
	<div id="waShadowLM" class="waSMShadow"></div>