<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$imgArr = array (
	0 => '/bitrix/templates/resize_testy/components/bitrix/news.list/template1/images/1280_1.jpg',
	1 => '/bitrix/templates/resize_testy/components/bitrix/news.list/template1/images/1000_1.jpg',
	2 => '/bitrix/templates/resize_testy/components/bitrix/news.list/template1/images/500_1.jpg',
	3 => '/bitrix/templates/resize_testy/components/bitrix/news.list/template1/images/1280_2.jpg',
	4 => '/bitrix/templates/resize_testy/components/bitrix/news.list/template1/images/1000_2.jpg',
	5 => '/bitrix/templates/resize_testy/components/bitrix/news.list/template1/images/500_2.jpg'
);
$imgSize = array (
	0 => '426589',
	1 => '302811',
	2 => '117893',
	4 => '',
	5 => '',
	6 => '',
	7 => '',
	8 => '',
	9 => ''
);
$i = -1;
?>
<div class="resize_cont">

<?foreach($arResult["ITEMS"] as $arItem):?>
<?//print_r($arItem["DETAIL_PICTURE"]);?>
<?$i++;?>
	<div class="resize_row">
	<div class="title"><?=$arItem["NAME"]?></div>
		<div class="resize_img x500">
			<a class="a_pic2" href="" style="background-image: url(<?echo ($templateFolder.''.$arItem["DISPLAY_PROPERTIES"]["JM_FILEPATH"]["~VALUE"]);?>)"></a><br />
			<span class="">Несжатое изображение (контейнер 500х500). Размер файла <b><?=$arItem["DISPLAY_PROPERTIES"]["JM_FILESIZE"]["VALUE"];?></b> байт</span>
		</div>
		<div class="resize_img x500">
			<a class="a_pic2" href="" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"></a>
			<span class="">Сжатое изображение (контейнер 500х500, размер 500х500). Размер файла <b><?=$arItem["DETAIL_PICTURE"]["FILE_SIZE"];?></b> байт</span>
		</div>
		<div class="resize_img x173">
			<a class="a_pic2" href="" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
			<span class="">Сжатое изображение (контейнер 173х173, размер 400х400). Размер файла <b><?=$arItem["PREVIEW_PICTURE"]["FILE_SIZE"];?></b> байт</span>
		</div>
	</div>
<?endforeach?>
</div>
