<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

/*
Количество фианитов
Металл
Тип изделия
Плетение
Вид вставки
Тип плетения
Вид плетения
Размер звена

ТП

ВСТАВКА
РАЗМЕР
*/
$this->setFrameMode(true);

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

//if (isset($_GET["SECTION_ID"])) $_GET["SECTION_ID"] = 302;
/*$pos = strpos($url,"SECTION_ID");
$url1 = substr($url,$pos,14);
$section = explode("=",$url1);
$section[1] = $sec;
$url_true = implode("=",$section);
$ff = str_replace($url1,$url_true,$url);
echo $ff;*/



	$firstSec = array();
		$Section = CIBlockSection::GetList(array(),array("DEPTH_LEVEL" => 1, "IBLOCK_ID" => 7), array("ID","NAME"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
        $firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
				$Sec1 = CIBlockSection::GetList(array(),array("DEPTH_LEVEL" => 2, "SECTION_ID" => $arSect["ID"]), array("ID","NAME"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				echo $firstSec[$i]["ID"];
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
			   }		   
   }
?>

<div class="bx_filter <?=$templateData["TEMPLATE_CLASS"]?> <?if ($arParams["FILTER_VIEW_MODE"] == "horizontal"):?>bx_horizontal<?endif?>">
<div class="bx_filter_section">
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
<div class="filter_block">
<div class="ajax_report"><img src="<?=$templateFolder?>/images/ajax.gif"/></div>
<div class="cross2"></div>
	<div class="wrap"></div>
<div class="sections">

	<!-- <div class="bx_filter_parameters_box_title_sec" id="ST__<?echo $ii?>">Все изделия</div> -->
	<!-- <div class="jew_type">
	<?foreach($firstSec as $value):?>
			<?foreach ($value["SUB"] as $item):?>
				<div class="secondSec" title="<? echo $item["ID"]?>"><? echo $item["NAME"]?></div>
			<?endforeach;?>
		<?endforeach?>
	</div> -->
	<!--  <div class="sec_bx_filter_block">  -->
		<?foreach($firstSec as $value):?>
			<div class="firstSec" title="<? echo $value["ID"]?>"><?echo $value["NAME"]?><!--<span></span>--></div>
			<div class="secondSec_block">
				<?foreach ($value["SUB"] as $item):?>
					<div class="secondSec" title="<? echo $item["ID"]?>"><? echo $item["NAME"]?></div>
				<?endforeach?>
			</div>
		<?endforeach;?>
	<!-- </div> -->
</div>
<?foreach($arResult["HIDDEN"] as $arItem):?>
	<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
<?endforeach;
//prices
foreach($arResult["ITEMS"] as $key=>$arItem)
{/*
	$key = $arItem["ENCODED_ID"];
	if(isset($arItem["PRICE"])):
		if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
			continue;

		$precision = 2;
		if (Bitrix\Main\Loader::includeModule("currency"))
		{
			$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
			$precision = $res['DECIMALS'];
		}
		?>
		<div class="bx_filter_parameters_box active">
			<span class="bx_filter_container_modef"></span>
			<div class="bx_filter_parameters_box_title" onclick="smartFilter.hideFilterProps(this)"><?=$arItem["NAME"]?></div>
			<div class="bx_filter_block">
				<div class="bx_filter_parameters_box_container">
					<div class="bx_filter_parameters_box_container_block">
						<div class="bx_filter_input_container">
							<input
								class="min-price"
								type="text"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								/>
						</div>
					</div>
					<div class="bx_filter_parameters_box_container_block">
						<div class="bx_filter_input_container">
							<input
								class="max-price"
								type="text"
								name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								/>
						</div>
					</div>
					<div style="clear: both;"></div>

					<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
						<?
						$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
						$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
						$price1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
						$price2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
						$price3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
						$price4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
						$price5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
						?>
						<div class="bx_ui_slider_part p1"><span><?=$price1?></span></div>
						<div class="bx_ui_slider_part p2"><span><?=$price2?></span></div>
						<div class="bx_ui_slider_part p3"><span><?=$price3?></span></div>
						<div class="bx_ui_slider_part p4"><span><?=$price4?></span></div>
						<div class="bx_ui_slider_part p5"><span><?=$price5?></span></div>

						<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
						<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
						<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
						<div class="bx_ui_slider_range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
							<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
							<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
						</div>
					</div>
					<div style="opacity: 0;height: 1px;"></div>
				</div>
			</div>
		</div>
	<?
	$arJsParams = array(
		"leftSlider" => 'left_slider_'.$key,
		"rightSlider" => 'right_slider_'.$key,
		"tracker" => "drag_tracker_".$key,
		"trackerWrap" => "drag_track_".$key,
		"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
		"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
		"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
		"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
		"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
		"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
		"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
		"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
		"precision" => $precision,
		"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
		"colorAvailableActive" => 'colorAvailableActive_'.$key,
		"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
	);
	?>
		<script type="text/javascript">
			BX.ready(function(){
				window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
			});
		</script>
	<?endif;
*/}

//not prices
?>
<div class="enable_block">
<div class="scroll">
<?
foreach($arResult["ITEMS"] as $key=>$arItem)
{
/*	echo "<pre>";
	print_r($arItem);
	echo "</pre>";*/
	if(
		empty($arItem["VALUES"])
		|| isset($arItem["PRICE"])
	)
		continue;

	if (
		$arItem["DISPLAY_TYPE"] == "A"
		&& (
			$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
		)
	)
		continue;
	?>
	<?	$flag = false;
		$style = '';
		if ($arItem["NAME"] == 'Тип изделия') {$flag = true; $style='style="display:none"';}
	?>
	<?
	
	?>
	<div id="<?echo "en".$arItem["ID"]?>" <?echo $style;?> class="bx_filter_parameters_box <?if ($flag == true):?>disbled3<?endif;?> <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
	<span class="bx_filter_container_modef"></span>
	<?$ii++;?>
	<!-- onclick="smartFilter.hideFilterProps(this)" -->
	<div <?echo $style;?> class="bx_filter_parameters_box_title"  id="ST__<?echo $ii?>"><?=$arItem["NAME"]?></div>

	<?if ($arItem["FILTER_HINT"] <> ""):?>
		<div class="bx_filter_parameters_box_hint" id="item_title_hint_<?echo $arItem["ID"]?>"></div>
		<script type="text/javascript">
			new top.BX.CHint({
				parent: top.BX("item_title_hint_<?echo $arItem["ID"]?>"),
				show_timeout: 10,
				hide_timeout: 200,
				dx: 2,
				preventHide: true,
				min_width: 250,
				hint: '<?= CUtil::JSEscape($arItem["FILTER_HINT"])?>'
			});
		</script>
	<?endif?>
	<div class="bx_filter_block" <?//echo $style;?>>
	<div class="bx_filter_parameters_box_container" id="SM__<?echo $ii?>">
	<?
	$arCur = current($arItem["VALUES"]);
	switch ($arItem["DISPLAY_TYPE"])
	{
		case "A"://NUMBERS_WITH_SLIDER
			?>
			<div class="bx_filter_parameters_box_container_block">
				<div class="bx_filter_input_container">
					<input
						class="min-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div>
			</div>
			<div class="bx_filter_parameters_box_container_block">
				<div class="bx_filter_input_container">
					<input
						class="max-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div>
			</div>
			<div style="clear: both;"></div>

			<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
				<?
				$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
				$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
				$value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
				$value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
				$value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
				$value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
				$value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
				?>
				<div class="bx_ui_slider_part p1"><span><?=$value1?></span></div>
				<div class="bx_ui_slider_part p2"><span><?=$value2?></span></div>
				<div class="bx_ui_slider_part p3"><span><?=$value3?></span></div>
				<div class="bx_ui_slider_part p4"><span><?=$value4?></span></div>
				<div class="bx_ui_slider_part p5"><span><?=$value5?></span></div>

				<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
				<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
				<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
				<div class="bx_ui_slider_range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
					<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
					<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
				</div>
			</div>
		<?
		$arJsParams = array(
			"leftSlider" => 'left_slider_'.$key,
			"rightSlider" => 'right_slider_'.$key,
			"tracker" => "drag_tracker_".$key,
			"trackerWrap" => "drag_track_".$key,
			"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
			"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
			"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
			"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
			"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
			"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
			"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
			"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
			"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
			"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
			"colorAvailableActive" => 'colorAvailableActive_'.$key,
			"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
		);
		?>
			<script type="text/javascript">
				BX.ready(function(){
					window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
				});
			</script>
			<?
			break;
		case "B"://NUMBERS
			?>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container">
					<input
						class="min-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div></div>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container">
					<input
						class="max-price"
						type="text"
						name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
						id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
						value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
						/>
				</div></div>
			<?
			break;
		case "G"://CHECKBOXES_WITH_PICTURES
			?>
			<?foreach ($arItem["VALUES"] as $val => $ar):?>
			<input
				style="display: none"
				type="checkbox"
				name="<?=$ar["CONTROL_NAME"]?>"
				id="<?=$ar["CONTROL_ID"]?>"
				value="<?=$ar["HTML_VALUE"]?>"
				<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
				/>
			<?
			$class = "";
			if ($ar["CHECKED"])
				$class.= " active";
			if ($ar["DISABLED"])
				$class.= " disabled";
			?>
			<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label dib<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
										<span class="bx_filter_param_btn bx_color_sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
			</label>
		<?endforeach?>
			<?
			break;
		case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
			?>
			<?foreach ($arItem["VALUES"] as $val => $ar):?>
			<input
				style="display: none"
				type="checkbox"
				name="<?=$ar["CONTROL_NAME"]?>"
				id="<?=$ar["CONTROL_ID"]?>"
				value="<?=$ar["HTML_VALUE"]?>"
				<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
				/>
			<?
			$class = "";
			if ($ar["CHECKED"])
				$class.= " active";
			if ($ar["DISABLED"])
				$class.= " disabled";
			?>
			<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
										<span class="bx_filter_param_btn bx_color_sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
										<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
											if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
												?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
											endif;?></span>
			</label>
		<?endforeach?>
			<?
			break;
		case "P"://DROPDOWN
			$checkedItemExist = false;
			?>
			<div class="bx_filter_select_container">
				<div class="bx_filter_select_block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
					<div class="bx_filter_select_text" data-role="currentOption">
						<?
						foreach ($arItem["VALUES"] as $val => $ar)
						{
							if ($ar["CHECKED"])
							{
								echo $ar["VALUE"];
								$checkedItemExist = true;
							}
						}
						if (!$checkedItemExist)
						{
							echo GetMessage("CT_BCSF_FILTER_ALL");
						}
						?>
					</div>
					<div class="bx_filter_select_arrow"></div>
					<input
						style="display: none"
						type="radio"
						name="<?=$arCur["CONTROL_NAME_ALT"]?>"
						id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
						value=""
						/>
					<?foreach ($arItem["VALUES"] as $val => $ar):?>
						<input
							style="display: none"
							type="radio"
							name="<?=$ar["CONTROL_NAME_ALT"]?>"
							id="<?=$ar["CONTROL_ID"]?>"
							value="<? echo $ar["HTML_VALUE_ALT"] ?>"
							<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
							/>
					<?endforeach?>
					<div class="bx_filter_select_popup" data-role="dropdownContent" style="display: none;">
						<ul>
							<li>
								<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx_filter_param_label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
									<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
								</label>
							</li>
							<?
							foreach ($arItem["VALUES"] as $val => $ar):
								$class = "";
								if ($ar["CHECKED"])
									$class.= " selected";
								if ($ar["DISABLED"])
									$class.= " disabled";
								?>
								<li>
									<label for="<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
								</li>
							<?endforeach?>
						</ul>
					</div>
				</div>
			</div>
			<?
			break;
		case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
			?>
			<div class="bx_filter_select_container">
				<div class="bx_filter_select_block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
					<div class="bx_filter_select_text" data-role="currentOption">
						<?
						$checkedItemExist = false;
						foreach ($arItem["VALUES"] as $val => $ar):
							if ($ar["CHECKED"])
							{
								?>
								<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
								<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
							<?endif?>
								<span class="bx_filter_param_text">
														<?=$ar["VALUE"]?>
													</span>
								<?
								$checkedItemExist = true;
							}
						endforeach;
						if (!$checkedItemExist)
						{
							?><span class="bx_filter_btn_color_icon all"></span> <?
							echo GetMessage("CT_BCSF_FILTER_ALL");
						}
						?>
					</div>
					<div class="bx_filter_select_arrow"></div>
					<input
						style="display: none"
						type="radio"
						name="<?=$arCur["CONTROL_NAME_ALT"]?>"
						id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
						value=""
						/>
					<?foreach ($arItem["VALUES"] as $val => $ar):?>
						<input
							style="display: none"
							type="radio"
							name="<?=$ar["CONTROL_NAME_ALT"]?>"
							id="<?=$ar["CONTROL_ID"]?>"
							value="<?=$ar["HTML_VALUE_ALT"]?>"
							<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
							/>
					<?endforeach?>
					<div class="bx_filter_select_popup" data-role="dropdownContent" style="display: none">
						<ul>
							<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
								<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx_filter_param_label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
									<span class="bx_filter_btn_color_icon all"></span>
									<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
								</label>
							</li>
							<?
							foreach ($arItem["VALUES"] as $val => $ar):
								$class = "";
								if ($ar["CHECKED"])
									$class.= " selected";
								if ($ar["DISABLED"])
									$class.= " disabled";
								?>
								<li>
									<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
										<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
											<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
										<?endif?>
										<span class="bx_filter_param_text">
															<?=$ar["VALUE"]?>
														</span>
									</label>
								</li>
							<?endforeach?>
						</ul>
					</div>
				</div>
			</div>
			<?
			break;
		case "K"://RADIO_BUTTONS
			?>
			<label class="bx_filter_param_label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
									<span class="bx_filter_input_checkbox">
										<input
											type="radio"
											value=""
											name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
											id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
											onclick="smartFilter.click(this)"
											/>
										<span class="bx_filter_param_text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
									</span>
			</label>
			<?foreach($arItem["VALUES"] as $val => $ar):?>
			
			<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label" for="<? echo $ar["CONTROL_ID"] ?>">
										<span class="bx_filter_input_checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
											<input
												type="radio"
												value="<? echo $ar["HTML_VALUE_ALT"] ?>"
												name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
												/>
											<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
										</span>
			</label>
		<?endforeach;?>
			<?
			break;
		case "U"://CALENDAR
			?>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container bx_filter_calendar_container">
					<?$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
							'SHOW_INPUT' => 'Y',
							'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
							'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
							'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
							'SHOW_TIME' => 'N',
							'HIDE_TIMEBAR' => 'Y',
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);?>
				</div></div>
			<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container bx_filter_calendar_container">
					<?$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
							'SHOW_INPUT' => 'Y',
							'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
							'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
							'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
							'SHOW_TIME' => 'N',
							'HIDE_TIMEBAR' => 'Y',
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);?>
				</div></div>
			<?
			break;
		default://CHECKBOXES
			?>
			<?foreach($arItem["VALUES"] as $val => $ar):?>
				<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
				<?foreach ($arAllSecs as $ttItem):?>
					<?if ($ttItem == $ar["VALUE"])
					{
						$class_sec = "clickSec";
					}	
					?>
				<?endforeach?>
										<span class="bx_filter_input_checkbox">
											<input
												type="checkbox"
												<?if ($flag == true) { echo 'class="clickSec"';} else { echo 'class="clickStand"';}?>
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this, sec)"
												/>
											<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
										</span>
				</label>
			<?endforeach;?>
			<?
	}
	?>
	</div>
	<div class="test_bx"></div>
	
	<div class="clb"></div>
	</div>
	</div> <!-- parameter_box -->
<?
}
?>
<div class="clb"></div>


<!-- <div class="bx_filter_parameters_box active"> -->

</div>
 </div> <!-- enable_block -->
<?foreach($arResult["ITEMS"] as $key=>$arItem)
{
	$key = $arItem["ENCODED_ID"];
	if(isset($arItem["PRICE"])):
		if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
			continue;

		$precision = 2;
		if (Bitrix\Main\Loader::includeModule("currency"))
		{
			$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
			$precision = $res['DECIMALS'];
		}
		?>
		<div class="prices active">
			<span class="bx_filter_container_modef"></span>
			<div class="bx_filter_parameters_box_title2">Цена</div>
			<div class="bx_filter_block">
				<div class="bx_filter_parameters_box_container">


					<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
						<?
						$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
						$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
						$price1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
						$price2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
						$price3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
						$price4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
						$price5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
						?>
						<div class="bx_ui_slider_part p1"><span><?=$price1?></span></div>
						<div class="bx_ui_slider_part p2"><span><?=$price2?></span></div>
						<div class="bx_ui_slider_part p3"><span><?=$price3?></span></div>
						<div class="bx_ui_slider_part p4"><span><?=$price4?></span></div>
						<div class="bx_ui_slider_part p5"><span><?=$price5?></span></div>

						<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
						<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
						<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
						<div class="bx_ui_slider_range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
							<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>">
							</a>
							<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>">

							</a>
						</div>
					</div>
					<div style="opacity: 0;height: 1px;"></div>
								<div class="bx_filter_input_container">
								От:
								<input
									class="min-price"
									type="text"
									name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
									size="5"
									onkeyup="smartFilter.keyup(this)"
									/>
								</div>
							<div class="bx_filter_input_container">
							До:
								<input
									class="max-price"
									type="text"
									name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
									size="5"
									onkeyup="smartFilter.keyup(this)"
									/>
							</div>




				</div>
			</div>
		</div>
	<?
	$arJsParams = array(
		"leftSlider" => 'left_slider_'.$key,
		"rightSlider" => 'right_slider_'.$key,
		"tracker" => "drag_tracker_".$key,
		"trackerWrap" => "drag_track_".$key,
		"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
		"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
		"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
		"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
		"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
		"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
		"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
		"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
		"precision" => $precision,
		"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
		"colorAvailableActive" => 'colorAvailableActive_'.$key,
		"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
	);
	?>
		<script type="text/javascript">
			BX.ready(function(){
				window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
			});
		</script>
	<?endif;
}

?>
 <div class="butt">
	<!-- <div class="bx_filter_block"> -->
		<div class="bx_filter_parameters_box_container">
			<!-- <input
				class="bx_filter_search_button"
				type="submit"
				id="set_filter"
				name="set_filter"
				value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
				/>
			<input
				class="bx_filter_search_reset"
				type="submit"
				id="del_filter"
				name="del_filter"
				value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
				/> -->
			<div class="bx_filter_popup_result <?=$arParams["POPUP_POSITION"]?>" id="modef"  style="display: inline-block;">
			<span id="modef_num"></span>
				<?//echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
				<span class="arrow"></span>
				<a href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
			</div>
		</div>
	<!-- </div> -->
</div>
</div> <!-- filter_block -->
</form>

<div style="clear: both;"></div>
</div>
</div>
<?
$section_rule = htmlspecialchars($_GET["SECTION_ID"]);
				$arFilter = array("IBLOCK_ID" => 6);
				$arSelect = array("ID","IBLOCK_ID","NAME","PROPERTY_JM_FILTER_PROPS","PROPERTY_JM_FILTER_STOP_LIST","PROPERTY_JM_FILTER_MIN");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
							$flag = false;
							$arFields = $ob->GetFields();
							foreach ($arFields["PROPERTY_JM_FILTER_PROPS_VALUE"] as $key => $value)
								{
									if ($value == $section_rule)
										{
											foreach ($arFields["PROPERTY_JM_FILTER_STOP_LIST_VALUE"] as $kk => $stop)
											{
												$JSONResult["STOP"][] = $stop;
											}
											foreach ($arFields["PROPERTY_JM_FILTER_MIN_VALUE"] as $kk => $min)
											{
												$JSONResult["MIN"][] = $min;
											}
										}
								}
							
						}
?>
<div class="smartfilter2">
<div class="filter_top">
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>

	<?
		$show = "disabled3";
		foreach ($JSONResult["STOP"] as $k=>$stop)
		{
			if ($stop == $arItem["NAME"]) $show = "";
			//	print_r($stop);
		}
		foreach ($JSONResult["MIN"] as $k=>$stop)
		{
			if ($stop == $arItem["NAME"]) $show = "";
			//	print_r($stop);
		}
	?>
<div class="bx_filter_parameters_box2 <?=$show;?> <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
<div class="bx_filter_parameters_box_title2"><?=$arItem["NAME"]?></div>
			<div class="bx_filter_block2">
			<div class="bx_filter_parameters_box_container2">
			<div class="bx_filter_select_container">
				<div class="bx_filter_select_block">
					<div class="bx_filter_select_text">
						<?
						foreach ($arItem["VALUES"] as $val => $ar)
						{
							if ($ar["CHECKED"])
							{
								echo $ar["VALUE"];
								$checkedItemExist = true;
							}
						}
						if (!$checkedItemExist)
						{
							echo $arItem["NAME"];
						}
						?>
					</div>
					<div class="bx_filter_select_arrow"></div>
					<div class="bx_filter_select_popup" style="display: none;">
						<ul>
							<li>
								<label class="bx_filter_param_label">
									<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>

								</label>
							</li>
							<?
							foreach ($arItem["VALUES"] as $val => $ar):
								$class = "";
								if ($ar["CHECKED"])
									$class.= " selected";
								if ($ar["DISABLED"])
									$class.= " disabled";
								?>
								<li>
									<label class="bx_filter_param_label<?=$class?>"</label>
								</li>
							<?endforeach?>
						</ul>
					</div>

				</div>
			</div>
			</div>
			</div>
</div>
<?endforeach?>
<div class="bx_filter_popup_result2">
	<a><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
</div>
<div class="clear_all"></div>
</div>
</div> <!-- smartfilter2 -->
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
	
</script>
<script>
$(document).keydown( function (ddd) {
        if( ddd.keyCode === 27 ) {
		$('.bx_filter').fadeOut();

		}
		});
$(".filter_click").on("click", function() {
	$(".smartfilter").fadeIn();
	$(".firstSec:first-child").click();
	
})
var f = 1;
	$(".bx_filter_parameters_box_title_sec").on("click", function () {
		
		$(".bx_filter_input_checkbox input:checkbox:checked").click();
		$(".enable_block").hide();
		$(this).removeClass("drop_down");
		$(".firstSec").removeClass("act_title");
		$(".bx_filter_parameters_box").removeClass("enabled");
		$(".secondSec_block").hide();
		$(this).next().show();
		
		
	});
	$(".bx_filter_parameters_box_title").on("click", function () {
		$(".bx_filter_parameters_box_title").removeClass("act_title");
		$(this).addClass("act_title");
		//$(".sec_bx_filter_block").hide();
		/*$(".bx_filter_block").hide();*/
		
		$(this).next().show();
	});
	$(".firstSec").on("click", function () {
		sec = $(this).attr("title");
		//geturl(sec);
		//$(".disabled3").removeClass("enabled");
		$(".jew_type").hide();
		//$(".disbled3").hide();
		/*$(".min-price").attr("value","");
		$(".max-price").attr("value","");*/
		$(".bx_filter_parameters_box_container .disabler").show();
		$(".bx_ui_slider_handle.left").attr("style","left: 0");
		//$(".bx_filter_parameters_box_title_sec").css("visibility","hidden");
		$(".enable_block").hide();
		$(".secondSec_block").hide();
		$(".firstSec").removeClass("act_title");
		//$(".bx_filter_parameters_box").removeClass("enabled");
		/*$(".bx_filter_parameters_box").hide();*/
		$(".secondSec").removeClass("act_title_sec");
		$(".bx_filter_parameters_box_title_sec").addClass("drop_down");
		$(this).addClass("act_title");
		$(".bx_filter_input_checkbox input:checkbox:checked").click();
		/*$(this).next().fadeIn(500);*/
		$(this).next().show();
		$(this).next().children(".secondSec:first-child").click();
		//$(".bx_filter_param_label:first-child").click();
		/*$(this).next().children(".secondSec").each( function () {
			$(this).hide;
			$(this).fadeIn(500);
		});*/
		
	});
/*	$(".jew_type").children(".secondSec").on("click", function () {

	});*/
	$.fn.jew_main = function () 
	{

		$(this).on("click", function () {
		//$(".bx_filter_input_checkbox input:checkbox:checked").click();
			//alert($(this).attr("class"));
			//alert("fgsdfg");
			
		fake = $(this).parent().attr("class");

		secondSec = $(this);
		f = $(this).text();
		sec = $(this).attr("title");
		//sec = $(this).parent().prev().attr("title");
		//console.log(sec);
		/*if (fake == "jew_type")
			{

				$(".jew_type").hide();
				$(".bx_filter_parameters_box_title_sec").addClass("drop_down");
				$(".sections").children(".secondSec_block").each( function () {
					if (sec == $(this).find(".secondSec").attr("title")) 
					{
						//console.log(sec);					
						$(this).prev().click();
						//console.log(sec);
						//$(this).children(".secondSec").click();
						
					//console.log($(this).parent().attr("class"));
					//console.log($(this).children(".secondSec").text());
					}
				});
			}*/
		//$(".disbled3").hide();
		//$(".bx_filter_parameters_box").hide();
		$(".secondSec").removeClass("act_title_sec");
		/*$(".enable_block").hide();*/
		$(this).addClass("act_title_sec");
		/*$(".bx_filter_parameters_box").removeClass("enabled");*/
		$(".clickSec:checkbox:checked").click();
		$('.bx_filter_param_text:contains("' + f +'")').prev().click();
//		$(".ajax_report").show();

/*		setTimeout(function(){*/
					geturl(sec);

						/*$(".bx_filter_parameters_box").each(function (){
						father = $(this);
						$(this).children(".bx_filter_block").children(".bx_filter_parameters_box_container").children(".bx_filter_param_label").each(function () {
							flag = false;
							if ( $(this).hasClass("disabled") == false )
							{
								father.addClass("enabled");
								flag = true;
							}
							if (flag == true) 
							{
								return false;
							}
						});
						});	*/
						//$(".sections").addClass("enabled");
						//$(".enable_block").children(".enabled:first-child").show();
					/*	$(".sections").siblings(".enabled").each(function () {
							$(this).show();
							return false;
						});*/
						//$(".disabled3").removeClass("enabled");
						$(".ajax_report").hide();
						
						$(".enable_block").show();
						//alert($(secondSec).attr("class"));
						//secondSec.addClass("level")
						
//				}, 2000);

	});
	}
	$(".secondSec").jew_main();
	$(".jew_type").children(".secondSec").jew_main();
/*	$(".secondSec").on("click", function (jew) {
		//alert($(this).attr("class"));
		//console.log(jew);
		alert(jew);
		secondSec = $(this);
		$(".secondSec").removeClass("act_title_sec");
		$(".enable_block").hide();
		$(this).addClass("act_title_sec");
		f = $(this).text();
		sec = $(this).attr("title");
		$(".clickSec:checkbox:checked").click();
		$('.bx_filter_param_text:contains("' + f +'")').prev().click();
		$(".ajax_report").show();
		setTimeout(function(){

			geturl(sec);
						$(".bx_filter_parameters_box").each(function (){
						father = $(this);
						$(this).children(".bx_filter_block").children(".bx_filter_parameters_box_container").children(".bx_filter_param_label").each(function () {
							flag = false;
							if ( $(this).hasClass("disabled") == false )
							{
								father.addClass("enabled");
								flag = true;
							}
							if (flag == true) 
							{
								//father.children(".bx_filter_parameters_box_title").addClass("level_up");
								return false;
							}
						});
						});	
						//$(".sections").addClass("enabled");
						$(".enable_block").find(".enabled:first").show();
					//	$(".sections").siblings(".enabled").each(function () {
					//		$(this).show();
					//		return false;
					//	});
						$(".ajax_report").hide();
						$(".enable_block").show();
						//alert($(secondSec).attr("class"));
						secondSec.addClass("level")
						
				}, 2000);

	});*/
	/*$(".bx_filter_param_label input").on("click", function () {
		setTimeout(function(){
			geturl(sec);
				}, 2000);
		
		
	});*/
	$(".bx_filter_param_label").on("click", function () {
		sec = $(".act_title").attr("title");
		
		//alert(sec);
		//$(".bx_filter_parameters_box.enabled").attr("style","display:block;");
//		$(".ajax_report").show();
		var block = $(this);
		var block = $(this).parents(".bx_filter_parameters_box").attr("ID");	
//			setTimeout( function() {
					//	geturl(sec);
/*
				var flaf_each = false;
				$(".bx_filter_parameters_box.enabled").each(function () {
					if (flaf_each == false)
						{
						if ($(this).attr("ID") == block) {
							flaf_each = true;
						}
						}
					else
					{
						var ert = $(this).attr("class");
						var enabl = ert.indexOf("enabled");
						var disabl = ert.indexOf("disabled3");
						if (enabl != -1 && disabl == -1)
							{
								$(this).attr("style","display:block;");
								console.log($(this));
								//return false;
							}
					}
				
				});
		$("#en124").hide();	*/
//		$(".ajax_report").hide();

//				}, 2000);	
	});
	/*$(".bx_ui_slider_handle").on("click", function () {
			setTimeout(function(){
			geturl(sec);
				}, 2000);		
	});*/
/*function geturl (sec){	
		//$("#SECTION_ID").attr("value",sec);
		$.get(
		  "/jewcat/filter.php",
		  {
			param1: $(".bx_filter_popup_result a").attr("href"),
			param2: sec
		  },
		  onAjaxSuccess
		);
		function onAjaxSuccess (data) {
			$(".bx_filter_popup_result a").attr("href",data);	
		}
		
};*/
/*function geturl (sec){	

	$.getJSON( 
	"/jewcat/filter.php",
	{
			param1: $(".bx_filter_popup_result a").attr("href"),
			param2: sec
	},	
	function (data)
		{
			request = JSON.parse(data);
			alert(request);
		}
	);

	}*/
function geturl (sec){
			$('.bx_filter_parameters_box').hide();
		var obj = {
			param1: $(".bx_filter_popup_result a").attr("href"),
			param2: sec
		} 
		BX.ajax.loadJSON(
			"/jewcat/filter.php",
			obj,
			response
		)
		function response (data) {
//			$(".bx_filter_popup_result a").attr("href",data.SECTION_RESULT);
		//console.log(data);
					for (val of data.STOP)
					{	
						$('.bx_filter_parameters_box_title:contains("' + val +'")').parent().show();
					}
			/*		for (val of data.SMART)
					{	setTimeout(function(){
						$('.bx_filter_parameters_box_title:contains("' + val +'")').next().children().find(".disabled").hide();
						}, 1000);
					}*/

					
		}
}	
	
	$('.cross').on("click", function (){
		$(".smartfilter").fadeOut();
	});	
$(".cross2").on("click", function () {
	$(".smartfilter").fadeOut();
})
$(".bx_filter_select_text").on("click", function(){
	$(this).next().next().toggle();
});
$(".bx_filter_select_arrow").on("click", function(){
	$(this).next().toggle();
});
$(".bx_filter_select_popup ul li label").on("click", function() {
	var text = $(this).text();
	var clear_all = $(".clear_all").text();
	$(this).parents(".bx_filter_select_popup").prev().prev().text(text)
	$(this).parents(".bx_filter_select_popup").hide();
	if ( clear_all != "clear" )
		{
			$(".clickStand:checkbox:checked").click();
		}
		else
		{
			$('.bx_filter_param_text:contains("' + text +'")').prev().click();
		}
	
	
	$(".clear_all").text("clear");
})
$(".bx_filter_popup_result2 a").on("click", function () {
	
	alert($(".bx_filter_popup_result").children().next().next().click());
	
})
</script>
