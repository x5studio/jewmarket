<!DOCTYPE html>
<html>
<head>
<!--<link rel="stylesheet" href="css/pc.css" type="text/css">
<link rel="stylesheet" href="css/tablet.css" type="text/css">
<link rel="stylesheet" href="css/fonts.css" type="text/css"> -->
<link rel="shortcut icon" href="<?echo SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<?$APPLICATION->ShowHead()?>

	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHeadStrings();?>
	<?//$APPLICATION->AddHeadScript('/bitrix/templates/jew_market/js/jquery-1.8.3.min.js');?>
	<?//CJSCore::Init('jquery');?>
	<?CUtil::InitJSCore(array('ajax', 'jquery'/*Если не подключена ранее*/, 'popup'));?>
	<?//CAjax::Init();?>
	<?$APPLICATION->AddHeadScript('/bitrix/templates/jew_market/js/main.js');?>

	<?//$APPLICATION->AddHeadScript('/bitrix/templates/jew_market/js/customize_ajax.js');?>
	<?//$APPLICATION->AddHeadScript('/bitrix/templates/jew_market/js/jquery.maskedinput.js');?>
	<?$APPLICATION->SetAdditionalCSS('/bitrix/templates/jew_market/fonts.css');?>
	<?$APPLICATION->SetAdditionalCSS('/bitrix/css/main/font-awesome.css');?>
	<?CModule::IncludeModule("sale");?>



<script type="text/javascript" src="/bitrix/templates/jew_market/js/jquery.maskedinput.js"></script>
	<script>
		jQuery(function($){
		   $("#phone").mask("+7 (999) 999-9999");
		});
		jQuery(function($){
		   $("#ORDER_PROP_3").mask("+7 (999) 9999999");
		});
</script>
</head>
	<?$APPLICATION->ShowPanel();?>
<body id="jewmarket">
<?if (strpos($_SERVER["REQUEST_URI"],"personal/order/make") === false):?>
<header>
	<div class="top_line">
		<div class="innerwrapper">
		<div class="equipage">Работаем на выезде! Выбирайте и покупайте не выходя из дома. <a href="/wholesale/rabotaem-na-vyezde/">Как это работает</a></div>
<div id="bx_basket1" class="bx-basket bx-opener">
<?$cart_count = CSaleBasket::GetList(false, ["FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL"], [], false, ['ID']);?>
			<div class="basket">
				<span>
					В корзине <a href="<?=$arParams['PATH_TO_BASKET']?>"><?=$cart_count?></a> товаров
				</span>
				<a href="/personal/cart/" class="button">Корзина</a>
			</div>
</div>
			<?/*$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "cart", Array(
			"COMPONENT_TEMPLATE" => ".default",
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
				"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
				"SHOW_TOTAL_PRICE" => "N",	// Показывать общую сумму по товарам
				"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
				"SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
				"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
				"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
				"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
				"SHOW_PRODUCTS" => "N",	// Показывать список товаров
				"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
			),
			false
		);*/?>	
		</div>	
	</div>
	<div class="header_main">
		<div class="innerwrapper">
			<div class="logo"><a href="/" class="logo"><img src="<?echo SITE_TEMPLATE_PATH?>/images/logo.png" /></a></div>
			<menu class="header_menu">
				<li><a href="<?echo SITE_DIR?>catalog/jewelry/?set_filter=y&arrFilter_135_4261170317=Y">Новинки</a></li>
				<li><a class="<?echo SITE_DIR?>delivery">Оплата и доставка</a></li>	
				<li><a href="<?echo SITE_DIR?>catalog/jewelry/?set_filter=y&arrFilter_132_4261170317=Y">Акции</a></li>
				<li><a href="<?echo SITE_DIR?>warranty/">Гарантии и качество</a></li>
				<li><a href="<?echo SITE_DIR?>wholesale/">Оптовые продажи</a></li>
				<li><a href="<?echo SITE_DIR?>contacts/">Контакты</a></li>				
<!--				<li><a href="<?echo SITE_DIR?>about/">О магазине</a></li>
				<li><a href="<?echo SITE_DIR?>partners/">Партнерам</a></li> -->
			</menu>
			<div class="contacts">
					<span class="tell">+7 (812) <b>339 25 37</b></span><img src="<?echo SITE_TEMPLATE_PATH?>/images/phone.png"/>
					<a href="#" id="consult" class="callorder">Заказать обратный звонок</a>
					<a href="/personal/" class="profile">Личный кабинет</a>
					
<?	if (strpos($_SERVER["REQUEST_URI"],"club") === false)
	{
	$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"FORGOT_PASSWORD_URL" => "",
		"USE_BACKURL" => "/about/index.php",
		"SUCCESS_PAGE" => "/about/index.php",
		"PROFILE_URL" => "/personal/",
		"REGISTER_URL" => "/catalog/",
		"SHOW_ERRORS" => "Y"
	),
	false
	);}?>
					<?/*$APPLICATION->IncludeComponent(
	"bxmod:auth.dialog", 
	"template1", 
	array(
		"COMPONENT_TEMPLATE" => "template1",
		"SUCCESS_RELOAD_TIME" => "2"
	),
	false
);*/?>
			</div>
<!--			<div class="right_block">
				<div class="tel">+7 (800) <b>775 73 79</b></div>
				<a class="callorder2">Заказать обратный звонок</a>
				<a href="/personal/" class="profile2">Личный кабинет</a>
			</div> -->
		</div>
	</div>

<?
	$week = array (
		1 => array ( "SHORT" => 'Пн', "FULL" => "Понедельник"),
		2 => array ( "SHORT" => 'Вт', "FULL" => "Вторник"),
		3 => array ( "SHORT" => 'Ср', "FULL" => "Среда"),
		4 => array ( "SHORT" => 'Чт', "FULL" => "Четверг"),
		5 => array ( "SHORT" => 'Пт', "FULL" => "Пятница"),
		6 => array ( "SHORT" => 'Сб', "FULL" => "Суббота"),
		7 => array ( "SHORT" => 'Вс', "FULL" => "Воскресение")
	);
	$arr_month = array (
		1 => 'Января',
		2 => 'Февраля',
		3 => 'Марта',
		4 => 'Апреля',
		5 => 'Мая',
		6 => 'Июня',
		7 => 'Июля',
		8 => 'Августа',
		9 => 'Сентября',
		10 => 'Октября',
		11 => 'Ноября',
		12 => 'Декабря'
	);
	//$arr_time = array (0 => "09",1 => "10",2 => "11",3 => "12",4 => "13",5 => "14",6 => "15",7 => "16",8 => "17");
	$arr_time = array ("09","10","11","12","13","14","15","16","17");
	$date = date("j-n-t-N-H");
	$date = explode("-",$date);
	$today = $date[0];
	$month = $date[1];
	$all = $date[2];
	$days = $date[3];
	$time = $date[4];
	//$time = "14";
	$flag = false;
	foreach ($arr_time as $key => $item)
		{
			if ($item == $time) $flag = true;
			if ($flag == true)
				{
					$true_arr_time[$key] = $item;
				}
		}
		if (empty($true_arr_time)) $true_arr_time = $arr_time;
?>
	<div class="recall_shadow">
			<form class="recall_form">
			<div class="cross"></div>
				<div class="row">
					<div class="title">Тема вопроса</div>
					<textarea type="text" required class="" name="theme"></textarea>
				</div>
				<div class="row">
					<div class="row_block">
						<div class="title">Ваше имя<span style="color:red">*</span></div>
						<input type="text" required class="" name="name"/>						
					</div>
					<div class="row_block">
						<div class="title">Телефон<span style="color:red">*</span></div>
						<input id="phone" name="tel" pattern="+7 ([0-9]{3}) [0-9]{3}-[0-9]{2}-[0-9]{2}" type="tel">
					</div>
				</div>
				<div class="row">
					<div class="title" style="font-weight:700; color: #000;">
						<span id="day_name">Сегодня</span>, <span id="day_num"><?=$today?></span> <span id="day_month"><?=$arr_month[$month];?></span>
					</div>
					<div class="day_cont">
						<?$o = $days-1;?>
						<?$flag_day = false;?>
						<?for ($i = 0; $i <= 9; $i++):?>
							<?if ($flag_day == false ) {$j = $i;} else {$j++;}?>
								<?$active = ($j == 0) ? "active" : "";?>
								<?$day_name = ($j <=1) ? 'data-date="name'.$j.'"' : "";?>
								<?if ($o != 7) {$o++;} else {$o = 1;}?>
								<?if ($week[$o]["SHORT"] == 'Вс' || $week[$o]["SHORT"] == 'Сб') {$weekend = "weekend";} else {$weekend = "";}?>
									<div <?=$day_name?> class="day <?echo($active." ".$weekend);?>">
										<span><?echo ($today+$j)?></span><br/>
										<span><?=$week[$o]["SHORT"]?><div><?=$week[$o]["FULL"]?></div></span>
									</div>
								<?$tt = $today+$i;?>
							<?if ($tt == $all)	{$j = 0; $flag_day = true; $today = 0;}?>
						<?endfor;?>
					</div>
					<div class="title day_title">Выберите подходящее для вас время</div>
					<div id="time_to_call" class="title" style="font-weight:700; color: #000; display:none">
						С <span id="time_start"></span>:00 До <span id="time_end"></span>:00
					</div>
					<div class="time_count">
						<?foreach($arr_time as $key => $item):?>
							<?$active = ($item == $true_arr_time[$key]) ? "today" : "";?>
							<div data-date="<?=$active?>" class="<?=$active?>"><span><?=$item?></span><span class="hover_on"></span></div>
						<?endforeach?>
					</div>
					<div class="row" style="text-align:center;">
						<div class="apply">Заказать звонок</div>
					</div>
				</div>
			<div class="close_mess">
				<div class="mess">
				Сообщение из формы отправлено. Мы вам обязательно перезвоним
				</div>
				<br />
				<a class="close_recall">Закрыть</a>
							
			</div>
			</form>
	</div>
</header>

		<nav class="sec_default">
			<div class="innerwrapper">
				<li class="scrollout"><a href="/about/">О Jew Market</a></li>
				<li><a href="/catalog/jewelry/">Ювелирные украшения</a></li>
				<li><a href="/gift/">Подобрать подарок</a></li>
				<li class="scrollout"><a href="/partners/">Партнерам</a></li>
				<li class="scrollout"><a href="/wherebuy/">Где купить</a></li>
				<div id="bx_basket2" class="bx-basket bx-opener wahiddencart">
							<div class="basket">
								<span>
									В корзине <a href="<?=$arParams['PATH_TO_BASKET']?>"><?=$cart_count?></a> товаров
								</span>
								<a href="/personal/cart/" class="button">Корзина</a>
							</div>
				</div>
<?
/* global $USER;
if ($USER->IsAdmin())
{*/
$APPLICATION->IncludeComponent(
	"jewmarket:search.title", 
	"visual", 
	array(
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0_iblock_catalog" => array(
			0 => "29",
		),
		"CHECK_DATES" => "N",
		"CONTAINER_ID" => "title-search",
		"CONVERT_CURRENCY" => "N",
		"INPUT_ID" => "title-search-input",
		"NUM_CATEGORIES" => "1",
		"ORDER" => "date",
		"PAGE" => "#SITE_DIR#search/index.php",
		"PREVIEW_HEIGHT" => "75",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PREVIEW_WIDTH" => "75",
		"PRICE_CODE" => array(
			0 => "ACTUAL",
		),
		"PRICE_VAT_INCLUDE" => "N",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"SHOW_PREVIEW" => "Y",
		"TOP_COUNT" => "5",
		"USE_LANGUAGE_GUESS" => "Y",
		"COMPONENT_TEMPLATE" => "visual"
	),
	false
);
/*}*/
?>		
			</div>
		</nav>
	<?
$curDir = explode('/',$APPLICATION->GetCurDir());
$bread[0]["PATH"] = $curDir[0].'/';
$bread[0]["NAME"] = "Главная страница";
$bread[1]["PATH"] = $bread[0]["PATH"].'catalog/jewelry/';
$bread[1]["NAME"] = "Ювелирный каталог";
if (CModule::IncludeModule('iblock')){
$curType = $curDir[3];
$curSubType = $curDir[4];
	$firstSec = array();
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 2, "IBLOCK_ID" => 29, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
	   if (!empty($curType) && $curType == $arSect["CODE"])
		{
			$bread[2]["NAME"] = $arSect["NAME"];
			$bread[2]["PATH"] = $bread[1]["PATH"]."".$arSect["CODE"]."/";
		}
        $firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
		$firstSec[$i]["CODE"] = $arSect["CODE"];
				$Sec1 = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 3, "SECTION_ID" => $arSect["ID"]), array("ID","NAME","CODE"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				   if (!empty($curSubType) && $curSubType == $arSect1["CODE"])
					{
						$bread[3]["NAME"] = $arSect1["NAME"];
						$bread[3]["PATH"] = $bread[2]["PATH"]."".$arSect1["CODE"]."/";
					}
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
					$firstSec[$i]["SUB"][$j]["CODE"] = $arSect1["CODE"];
			   }		   
   }
}
?>
<div class="sections_all sec_default">
<div class="sections">
		<?foreach($firstSec as $value):?>
		<?$insert_fil++;?>
			<div class="firstSec"><a href="/catalog/jewelry/<?=$value["CODE"]?>/" class="firstTitle" rel="canonical"><?echo $value["NAME"]?></a>
			<?if (!empty($value["SUB"])):?>
			<div class="secondSec_block">
				<div class="secondSec">
					<div class="title">Тип изделия</div>
					<?foreach ($value["SUB"] as $item):?>
						<a href="/catalog/jewelry/<?=$value["CODE"]?>/<?=$item["CODE"]?>/" rel="canonical"><? echo $item["NAME"]?></a>
					<?endforeach?>
					<a class="catall" href="/catalog/jewelry/<?=$value["CODE"]?>/">Смотреть все ></a>
				</div>
			
			<div class="secondSec">
				<div class="title">Технология</div>
				<a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a>
				<a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_1233026983=Y">Алмазная грань</a>
				<a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_1048137521=Y">Родирование</a>
				<a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_1589561044=Y">Эмаль</a>
			</div>
			<?if ($insert_fil < 4 || $insert_fil > 6):?>
			<div class="secondSec">
				<div class="title">Вставка</div>
			</div>
			<?endif?>
			<div class="secondSec d_img">
				<img src=""/>
			</div>
			</div> <!-- secondSec_block  -->
			<?endif?>
			</div>
		<?endforeach;?>
</div>
</div>
<?if (strpos($_SERVER["REQUEST_URI"],"catalog") === false && $APPLICATION->GetCurPage(false) !== '/'):?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb", 
		"catalog_bc", 
		array(
			"COMPONENT_TEMPLATE" => "catalog_bc",
			"START_FROM" => "0",
			"PATH" => "",
			"SITE_ID" => "s1"
		),
		false
	);?>
<?endif?>
<?if (strpos($_SERVER["REQUEST_URI"],"catalog") === false):?>
<div class="content">
	<div class="workarea">
<?/*$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.small", 
	"ajax_basket", 
	array(
		"COMPONENT_TEMPLATE" => "ajax_basket",
		"PATH_TO_BASKET" => "/personal/basket.php",
		"PATH_TO_ORDER" => "/personal/order.php",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N"
	),
	false
);*/?>	
<?endif?>
<?else:?>
<div class="content">
	<div id="workarea" class="workarea">
<?endif;?>