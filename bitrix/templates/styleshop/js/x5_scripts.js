jQuery(document).ready(function ($) {
    $(document).on('click', '.ajax-form-link', function (event) {
        event.preventDefault();
        // showCalcPopup($(this).attr("href"));
        showCalcPopup(this);
    });

    function showCalcPopup(target) {

        var data = {
            'form_popup': $(target).data('form-popup')
        };

        $.fancybox({
            type: 'ajax',
            autoSize: true,
            width: 500,
            maxWidth: 500,
            minWidth: 230,
            cache: false,

            helpers: {
                title: {
                    type: 'inside',
                    position: 'top'
                }
            },
            title: $(target).data('form-title'),
            href : $(target).attr('href'),
            openEffect	: 'fade',
            closeEffect	: 'fade'
        });

    }


    hideEmptyPropValues();
});

function hideEmptyPropValues(){
    $(".block-sku-detail-sku-container-list").each(function(){
        if($(this).find("li").length <= 1){
            $(this).parents(".block-sku-detail-sku").css("display", "none");
        }
    })
}
