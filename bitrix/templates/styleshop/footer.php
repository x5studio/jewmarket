<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__); ?>

<?if($curPage != SITE_DIR.'index.php'):?>

</div>
    
<!--container--></div>
<!--row--></div>
<?endif?>

</main>

<script type="text/javascript">

    $( document ).ready(function() {

        $(".unproduct-container").on("click", function () {
            if ($(window).width < 992) {
                console.log("CLIKK");
                $(this).find('.block_list').stop().slideToggle(700);
            }
        });
        <?/*XFive/Catalog/CKomplekt.php
        вывод js-скрипта для изменения кнопок для комплектов "В корзину" на "В корзине". Делаем через js, так как нельзя это делать через кэшируемый код компонента*/?>
        <?=XFive\Catalog\CKomplekt::getBasketKomplektJs()?>

    });


</script>


<?/***************** footer *******************/?>
        <?$APPLICATION->IncludeComponent(
        "bitrix:main.include", 
        ".default",
        Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/footer/type1.php",
            "AREA_FILE_RECURSIVE" => "N",
            "EDIT_MODE" => "html"
        ),
        false,
        array(
            "HIDE_ICONS" => "Y"
        )
    );?>
    <?/***************** footer end *******************/?>
</div>
</body><!-- /body -->
</html>