<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bPriceType  = false;
$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
?>
<div id="basket_items_delayed" class="bx_ordercart_order_table_container" style="display:none">
	<table class="table" id="delayed_items">
		<thead>
			<tr>
				<?
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
					$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
					if ($arHeader["name"] == '')
						$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
					if (in_array($arHeader["id"], array("TYPE"))) // some header columns are shown differently
					{
						$bPriceType = true;
						continue;
					}
					elseif ($arHeader["id"] == "PROPS")
					{
						$bPropsColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "DELAY")
					{
						continue;
					}
					elseif ($arHeader["id"] == "DELETE")
					{
						$bDeleteColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "WEIGHT")
					{
						$bWeightColumn = true;
					}

					if ($arHeader["id"] == "NAME"):
					?>
						<td class="item" colspan="2">
					<?
					elseif ($arHeader["id"] == "PRICE"):
					?>
						<td class="price">
					<?
					else:
					?>
						<td class="custom">
					<?
					endif;
					?>
						<?=$arHeader["name"]; ?>
						</td>
				<?
				endforeach;

				if ($bDeleteColumn || $bDelayColumn):
				?>
					<td class="custom"></td>
				<?
				endif;
				?>
			</tr>
		</thead>

		<tbody>
			<?
			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

				if ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y"):
			?>
				<tr id="<?=$arItem["ID"]?>">
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

						if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in columns in this template
							continue;

						if ($arHeader["id"] == "NAME"):
						
							$sku = '';
							$arColor = [];
							ob_start();

							if(!empty($arItem["PROPS"])):
								?><ul class="bx_ordercart_itemart list-inline clearfix mb">
										<?
										if ($bPropsColumn):
											foreach ($arItem["PROPS"] as $val):

												if (is_array($arItem["SKU_DATA"]))
												{
													$bSkip = false;
													foreach ($arItem["SKU_DATA"] as $propId => $arProp)
													{
														if ($arProp["CODE"] == $val["CODE"])
														{
															$bSkip = true;
															break;
														}
													}
													if ($bSkip)
														continue;
												}
												?>
												<li><span class="bx_ordercart_itemart-name"><?php echo $val["NAME"] ?></span></li>
												<li><span class="bx_ordercart_itemart-value"><?php echo $val["VALUE"] ?></span></li>
												<?
											endforeach;
										endif;
										?>
									</ul>
								<?
								endif;
								if (is_array($arItem["SKU_DATA"]) && !empty($arItem["SKU_DATA"])):
									foreach ($arItem["SKU_DATA"] as $propId => $arProp):

										// if property contains images or values
										$isImgProperty = false;
										if (!empty($arProp["VALUES"]) && is_array($arProp["VALUES"]))
										{
											foreach ($arProp["VALUES"] as $id => $arVal)
											{
												if (!empty($arVal["PICT"]) && is_array($arVal["PICT"])
													&& !empty($arVal["PICT"]['SRC']))
												{
													$isImgProperty = true;
													break;
												}
											}
										}

										if ($isImgProperty): // iblock element relation property
										?>
											<div class="bx_item_detail_scu_small_noadaptive">

												<div class="bx_scu_scroller_container">

													<div class="bx_scu">
														<ul
															class="sku_prop_list list-inline">
															<li><?=$arProp["NAME"]?></li>
															<?
															foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

																$selected = false;
																foreach ($arItem["PROPS"] as $arItemProp):
																	if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
																	{
																		if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"])
																			$selected = true;
																	}
																endforeach;
																if($selected):
															?>
																<li
																	data-value-id="<?=$arSkuValue["XML_ID"]?>"
																	data-element="<?=$arItem["ID"]?>"
																	data-property="<?=$arProp["CODE"]?>">
																	<span class="cnt_item"><?php echo $arSkuValue["NAME"]?></span>
																	<?$arColor[] = $arSkuValue["PICT"]["SRC"]?>
																		<span class="cnt_item" style="background-image:url(<?php echo $arSkuValue["PICT"]["SRC"];?>)">
																	</span>
																</li>
															<?endif?>
															<?
															endforeach;
															?>
														</ul>
													</div>
												</div>

											</div>
										
										<?
										else:
										?>
											<div class="bx_item_detail_size_small_noadaptive">

												<div class="bx_size_scroller_container">
													<div class="bx_size">
														<ul
															class="sku_prop_list list-inline">
															<li><?=$arProp["NAME"]?></li>
															<?
															foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

																$selected = false;
																foreach ($arItem["PROPS"] as $arItemProp):
																	if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
																	{
																		if ($arItemProp["VALUE"] == $arSkuValue["NAME"])
																			$selected = true;
																	}
																endforeach;
																if($selected):
															?>
																<li
																	data-value-id="<?=($arProp['TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory' ? $arSkuValue['XML_ID'] : $arSkuValue['NAME']); ?>"
																	data-element="<?=$arItem["ID"]?>"
																	data-property="<?=$arProp["CODE"]?>"
																	>
																	<span class="cnt"><?=$arSkuValue["NAME"]?></span>
																</li>
																<?endif?>
															<?
															endforeach;
															?>
														</ul>
													</div>
												</div>

											</div>
										<?
										endif;
									endforeach;
								endif;

							$sku = ob_get_contents();
							ob_end_clean();

						?>
							<td class="itemphoto" <?if(true && isset($arResult['complements'][$k])):?>style="display:"<?endif;?>>
								<div class="bx_ordercart_photo_container">
									<?
									if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
										$url = $arItem["PREVIEW_PICTURE_SRC"];
									elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
										$url = $arItem["DETAIL_PICTURE_SRC"];
									else:
										$url = $templateFolder."/images/no_photo.png";
									endif;
									?>

									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
										<img class="img-thumbnail" src="<?=$url?>" alt="" />
										<?
											if(isset($arColor) && !empty($arColor))
											{
												foreach($arColor as $color)
												{
													?><span class="cnt_item" style="background-image:url(<?php echo $color ?>)"></span><?
												}
											}
										?>
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
								</div>
								<?
								if (!empty($arItem["BRAND"])):
								?>
								<div class="bx_ordercart_brand">
									<img alt="" src="<?=$arItem["BRAND"]?>" />
								</div>
								<?
								endif;
								?>
							</td>
							<td class="item" <?if(true && isset($arResult['complements'][$k])):?>colspan="2"<?endif;?>>
								<h4 class="bx_ordercart_itemtitle">
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
										<?=$arItem["NAME"]?>
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
								</h4>
								<?php echo $sku ?>




								<? //sostav komplekta 20171118 ?>

                                <!-- комплекты -->
								<?if(true && isset($arResult['complements'][$k])):?>
									<?
									global $theme;
									$column = $theme->Option()->get('section_column');
									global $arrFilterKomplekt;
									if ($arResult['complements'][$k] ) {
										$complementKod = $arResult['complements'][$k]['komplekt_kod'];
										$complementProductsId = $arResult['complements'][$k]['komplektProductsId'];

										$arrFilterKomplekt = array('PROPERTY_' . COMPLEMENT_PROP_NAME => $complementKod);
										$intSectionID = $APPLICATION->IncludeComponent(
											"bitrix:catalog.section",
											"basketcomplement",
											array(
												"IBLOCK_TYPE" => "1c_catalog",
												"IBLOCK_ID" => IBLOCK_CATALOG_ID,
												"ELEMENT_SORT_FIELD" => 'PROPERTY_'.COMPLEMENT_PROP_NAME,
												"ELEMENT_SORT_ORDER" => 'asc',
												"ELEMENT_SORT_FIELD2" => "sort",
												"ELEMENT_SORT_ORDER2" => "asc",
												"PROPERTY_CODE" => array(
													0 => \XFive\Conf::PROPERTY_DISCOUNT_CODE,
													1 => \XFive\Conf::PROPERTY_NEW_PRODUCT_CODE,
													2 => \XFive\Conf::PROPERTY_HIT_CODE,
													3 => \XFive\Conf::PROPERTY_SPECIALOFFER_CODE,
													4 => "WARRANTY",
												),
												"META_KEYWORDS" => "-",
												"META_DESCRIPTION" => "-",
												"BROWSER_TITLE" => "-",
												"INCLUDE_SUBSECTIONS" => "Y",
												"BASKET_URL" => "/personal/cart/",
												"ACTION_VARIABLE" => "action",
												"PRODUCT_ID_VARIABLE" => "id",
												"SECTION_ID_VARIABLE" => "N",
												"PRODUCT_QUANTITY_VARIABLE" => "quantity",
												"PRODUCT_PROPS_VARIABLE" => "prop",
												"FILTER_NAME" => "arrFilterKomplekt",
												"CACHE_TYPE" => "A",
												"CACHE_TIME" => "36000000",
												"CACHE_FILTER" => "N",
												"CACHE_GROUPS" => "Y",
												"SET_TITLE" => "Y",
												"SET_STATUS_404" => "Y",
												"DISPLAY_COMPARE" => "N",
												"PAGE_ELEMENT_COUNT" => "15",
												"LINE_ELEMENT_COUNT" => "3",
												"PRICE_CODE" => \XFive\Main\Settings::getInstance()->get("CATALOG_PRICES"),
												"USE_PRICE_COUNT" => "N",
												"SHOW_PRICE_COUNT" => "1",

												"PRICE_VAT_INCLUDE" => "Y",
												"USE_PRODUCT_QUANTITY" => "Y",
												"PRODUCT_PROPERTIES" => array(),
												"DISPLAY_TOP_PAGER" => "Y",
												"DISPLAY_BOTTOM_PAGER" => "Y",
												"PAGER_TITLE" => "Товары",
												"PAGER_SHOW_ALWAYS" => "N",
												"PAGER_TEMPLATE" => "pagenav",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
												"PAGER_SHOW_ALL" => "N",

												"OFFERS_CART_PROPERTIES" => array(
													0 => "NAME",
													1 => "PREVIEW_PICTURE",
													2 => "DETAIL_PICTURE",
													3 => PROPERTY_ARTNUMBER_CODE,
												),
												"OFFERS_FIELD_CODE" => array(
													0 => "NAME",
													1 => "PREVIEW_PICTURE",
													2 => "DETAIL_PICTURE",
													3 => PROPERTY_ARTNUMBER_CODE,
												),
												"OFFERS_PROPERTY_CODE" => \XFive\Main\Settings::getInstance()->get("OFFERS_PROPERTY_CODE"),
												"OFFERS_SORT_FIELD" => "sort",
												"OFFERS_SORT_ORDER" => "asc",
												"OFFERS_SORT_FIELD2" => "id",
												"OFFERS_SORT_ORDER2" => "desc",
												"OFFERS_LIMIT" => "0",

												"SECTION_ID" => "",
												"SECTION_CODE" => "",
												//"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
												//"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
												'CONVERT_CURRENCY' => "Y",
												'CURRENCY_ID' => "RUB",
												'HIDE_NOT_AVAILABLE' => "N",
												'HOVER_IMAGE' => "Y",

												'LABEL_PROP' => "-",
												'ADD_PICT_PROP' => "MORE_PHOTO",
												'PRODUCT_DISPLAY_MODE' => "Y",
												'MESS_BTN_ADDED_TO_BASKET' => "В корзине",

												"USE_SKU_TOOLTIP" => "N",

												'SHOW_CLOSE_POPUP' => "Y",

												'OFFER_ADD_PICT_PROP' => "MORE_PHOTO",
												"OFFER_TREE_PROPS" => \XFive\Main\Settings::getInstance()->get("OFFER_TREE_PROPS"),
												//'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
												'SHOW_DISCOUNT_PERCENT' => "Y",
												'SHOW_OLD_PRICE' => "Y",
												'MESS_BTN_BUY' => "Выбрать",
												'MESS_BTN_ADD_TO_BASKET' => "В корзину",
												//'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
												'MESS_BTN_COMPARE' => "Сравнение",
												'MESS_BTN_DETAIL' => "Подробнее",
												'MESS_NOT_AVAILABLE' => "Нет в наличии",
												"MAX_WIDTH" => "104",
												"MAX_HEIGHT" => "104",
												"USE_QUICKVIEW" => "N",
												"ON_PREVIEW_TEXT" => "Y",
												"USE_FAVORITES" => "Y",
												"FORUM_ID" => '1',
												"USE_COMMENTS" => "Y",
												"DISPLAY_PROPERTIES_CHARACTERISTICS" => array(
													0 => "WARRANTY",
													1 => "MANUFACTURER",
													2 => "COUNTRY",
													3 => "NUMBER_OF_SIM_CARDS",
													4 => "ENCLOSURE_TYPE",
													5 => "PLATFORM",
													6 => "DIAGONAL_SCREEN",
													7 => "",
												),
												"IS_AJAX" => '',
												"COLUMN" => $column,

												"SHOW_ALL_WO_SECTION" => "Y",

												"COMPLEMENTS_PRODUCTS_ID" => $complementProductsId,

												"BASKET_ROW_ID" => $arItem['ID']
											),
											$component
										);

									}

									?>

								<?endif;?>
								<? //END sostav komplekta ?>





							</td>
							<?
						elseif ($arHeader["id"] == "QUANTITY"):
						?>
							<td class="custom" data-label="<?php echo GetMessage("SALE_".$arHeader["id"]); ?>">
								<span><?=$arHeader["name"]; ?>:</span>
								<div style="text-align: center;">
									<?echo $arItem["QUANTITY"];
										if (isset($arItem["MEASURE_TEXT"]))
											echo "&nbsp;".$arItem["MEASURE_TEXT"];
									?>
								</div>
							</td>
						<?
						elseif ($arHeader["id"] == "PRICE"):
						?>
							<td class="content_price<?if(0 < floatval($arItem["DISCOUNT_PRICE"])):?> old<?endif?>" data-label="<?php echo GetMessage("SALE_".$arHeader["id"]); ?>">
								<?if (doubleval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
									<div class="price"><?=$arItem["PRICE_FORMATED"]?></div>
									<div class="old-price"><?=$arItem["FULL_PRICE_FORMATED"]?></div>
								<?else:?>
									<div class="price"><?=$arItem["PRICE_FORMATED"];?></div>
								<?endif?>

								<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
									<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
									<div class="type_price_value"><?=$arItem["NOTES"]?></div>
								<?endif;?>
							</td>
						<?
						elseif ($arHeader["id"] == "DISCOUNT"):
						?>
							<td class="custom" data-label="<?php echo GetMessage("SALE_".$arHeader["id"]); ?>">
								<span><?=$arHeader["name"]; ?>:</span>
								<?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?>
							</td>
						<?
						elseif ($arHeader["id"] == "WEIGHT"):
						?>
							<td class="custom">
								<span><?=$arHeader["name"]; ?>:</span>
								<?=$arItem["WEIGHT_FORMATED"]?>
							</td>
						<?
						else:
						?>
							<td class="custom sum" data-label="<?=GetMessage("SALE_".$arHeader["id"]); ?>">
								<span><?=GetMessage("SALE_".$arHeader["id"]); ?>:</span>
								<?=$arItem[$arHeader["id"]]?>
							</td>
						<?
						endif;
					endforeach;

					if ($bDelayColumn || $bDeleteColumn):
					?>
						<td class="control">
							<div><a class="add" title="<?=GetMessage("SALE_ADD_TO_BASKET")?>" href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["add"])?>"></a></div>
							<?
							if ($bDeleteColumn):
							?>
								<div><a class="delete" title="<?=GetMessage("SALE_DELETE")?>" href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"></a></div>
							<?
							endif;
							?>
						</td>
					<?
					endif;
					?>
				</tr>
				<?
				endif;
			endforeach;
			?>
		</tbody>

	</table>
</div>
<?