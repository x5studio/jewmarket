<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;

$defaultParams = array(
	'TEMPLATE_THEME' => 'blue'
);
$arParams = array_merge($defaultParams, $arParams);
unset($defaultParams);

$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME'])
{
	$arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
	if ('site' == $arParams['TEMPLATE_THEME'])
	{
		$templateId = (string)Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', SITE_ID);
		$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? 'eshop_adapt' : $templateId;
		$arParams['TEMPLATE_THEME'] = (string)Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', SITE_ID);
	}
	if ('' != $arParams['TEMPLATE_THEME'])
	{
		if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
			$arParams['TEMPLATE_THEME'] = '';
	}
}
if ('' == $arParams['TEMPLATE_THEME'])
	$arParams['TEMPLATE_THEME'] = 'blue';


//20171115 BEGIN poluchenie tovarov vhodyashix v komplekt

if(true) {

	$arKomplektsId = array();
	$complements = array();

	foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

		if ($arItem['TYPE'] != CCatalogProductSet::TYPE_SET) continue;
		$arKomplektsId[] = $arItem['PRODUCT_ID'];
		$complectID = $arItem['PRODUCT_ID'];

		if (strpos($arItem['PRODUCT_XML_ID'], 'price') <= 0) continue; //znachit eto ne nami sozdanniy komplekt
		$t = explode("-", $arItem['PRODUCT_XML_ID']);
		$komplekt_kod = $t[0];
		$amount_el = count($t);
		for ($i2 = 1; $i < $amount_el; $i2++) {
			if (strpos($t[$i2], 'price') === 0) break;
			$komplektProductsId_byXmlId[] = $t[$i2];
		}

		$arSets = CCatalogProductSet::getAllSetsByProduct($complectID, CCatalogProductSet::TYPE_SET); // массив комплектов данного товара
		$arSet = array_shift($arSets); // комплект данного товара

		$komplektProductsId = array();
		foreach ($arSet['ITEMS'] as $key2 => $value2) {
			$komplektProductsId[$value2['ITEM_ID']] = $value2['ITEM_ID'];
		}

		$db_res = CCatalogProduct::GetList(
			array("ID" => "ASC"),
			array("ID" => $komplektProductsId),
			false,
			false,
			array('ID', 'TYPE', 'ELEMENT_NAME')
		);

		$komplektProductsId_final = array();
		$arIdsForPropertyQuery = array();
		while (($ar_res = $db_res->Fetch())) {
			if ($ar_res['TYPE'] == 4) {
				$arIdsForPropertyQuery[] = $ar_res['ID'];
			} else {
				$komplektProductsId_final[$ar_res['ID']] = $ar_res['ID'];
			}
		}

		$productsForOffers = \CCatalogSKU::getProductList($arIdsForPropertyQuery);
		foreach ($productsForOffers as $idOffer => $arParentProduct) {
			$komplektProductsId_final[$arParentProduct['ID']] = $idOffer;
		}

		$complements[$k] = array(
			'komplekt_kod' => $komplekt_kod,
			'komplektProductsId' => $komplektProductsId_final
		);

	endforeach;

	$arResult['complements'] = $complements;

//20171115 END poluchenie tovarov vhodyashix v komplekt


//20171120 BEGIN poluchenie artikula dlya tovarov v korzine

	$itemsId = array();
	$offersItemsId = array();
	foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
		if ($arItem['TYPE'] == CCatalogProductSet::TYPE_SET) continue;
		$itemsId[$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
		$offersItemsId [ $arItem['PRODUCT_ID'] ] =  $arItem['ID'];
		//print_r($arItem);
	endforeach;
	//echo "</pre>";

	$db_res = CCatalogProduct::GetList(
		array("ID" => "ASC"),
		array("ID" => $itemsId),
		false,
		false,
		array('ID','TYPE','ELEMENT_NAME')
	);

	$arArticuls = array();
	$arOffersId = array();
	$arProductsId = array();
	while (($ar_res = $db_res->Fetch()))
	{
		if($ar_res['TYPE']==4){
			$arOffersId[] = $ar_res['ID'];
		} else {
			$arProductsId[ $ar_res['ID'] ] = $ar_res['ID'];
		}
	}


	if($arOffersId){ //poluchenie parent product
		$productsForOffers = \CCatalogSKU::getProductList($arOffersId);
		foreach ($productsForOffers as $idOffer => $arParentProduct) {
			$arProductsId[ $idOffer ] = $arParentProduct['ID'];
		}
	}

	$arFilter = array("ID"=>$arProductsId);
	$arSelect = array("ID","PROPERTY_".PROPERTY_ARTNUMBER_CODE);

	$res = \CIBlockElement::GetList(array(),$arFilter,false, false,$arSelect );
	while( $arItem = $res->GetNext()){
		$arArticuls[ $arItem['ID'] ] = $arItem[ "PROPERTY_".PROPERTY_ARTNUMBER_CODE."_VALUE" ];
	}

	$arResult['parentProductsId'] = $arProductsId;
	$arResult['parentProductArticuls'] = $arArticuls;
	foreach($arResult['parentProductsId'] as $idOffer=>$idParentProduct){
		$arResult['offerArticuls'][ $offersItemsId[$idOffer] ] = $arResult['parentProductArticuls'][ $idParentProduct ];
	}

//20171120 END poluchenie artikula dlya tovarov v korzine

}