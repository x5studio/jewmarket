
/*
function updateKomplektButton(obj) {

    var obj_jq = $(obj);
    var div_el = obj_jq.closest('.row');

    var inb = obj.getAttribute('data-inbasket');
    if(inb=='true') return true;

    var knopki = div_el.find('a.offers:not(.addkomplekt)');
    var komplekt_tip = obj_jq.attr("data-komplekttip");

    var arr = knopki.map(function(indx, element){
        return $(element).attr("data-elementid");
    });
    var arValues = arr.get();

    $.ajax({
        type:'POST',
        url:'/ajax/addComplement.php',
        dataType:'json',
        data:{'values':arValues,
            'komplekt_tip':komplekt_tip
        },

        timeout:10000,
        success:function(data,textStatus){
            //debugger;
            //alert('success='+data.responseText);
        },
        complete:function(data,textStatus){

            if(data.responseJSON.status!="ok") return false;
           var id_komplekt = data.responseJSON.id;

            var product = BX.parseJSON(obj.getAttribute('data-options'));
            product.detailPageUrl = data.responseJSON.detail_url;
            var json_str = JSON.stringify(product);
            obj.setAttribute('data-options', json_str);
            obj.setAttribute('href', data.responseJSON.detail_url);
            obj.setAttribute('data-inbasket', 'true');

                BX.style(obj, 'display', '');
                obj.setAttribute('data-elementid', id_komplekt);
                //komplektInBasket(obj);
            BX.removeClass(obj, 'add2basket_0');
            BX.addClass(obj, 'add2basket_'+id_komplekt);
            Basket.add($(obj));


        }
    });
    return false;
}


function updateKomplektInBasket(obj) {

    var obj_jq = $(obj);
    var div_el = obj_jq.closest('.catalog-products-container');

    var knopki = div_el.find('a.offers:not(.addkomplekt)');
    var komplekt_tip = obj_jq.attr("data-komplekttip");

    var arr = knopki.map(function(indx, element){
        return $(element).attr("data-elementid");
    });
    var arValues = arr.get();


    $.ajax({
        type:'POST',
        url:'/ajax/addComplement.php',
        dataType:'json',
        data:{'values':arValues,
            'komplekt_tip':komplekt_tip
        },

        timeout:10000,
        success:function(data,textStatus){
            //debugger;
            //alert('success='+data.responseText);
        },
        complete:function(data,textStatus){

            if(data.responseJSON.status!="ok") return false;
            var id_komplekt = data.responseJSON.id;

            var product = BX.parseJSON(obj.getAttribute('data-options'));
            product.detailPageUrl = data.responseJSON.detail_url;
            var json_str = JSON.stringify(product);
            obj.setAttribute('data-options', json_str);
            obj.setAttribute('href', data.responseJSON.detail_url);
            obj.setAttribute('data-inbasket', 'true');

            BX.style(obj, 'display', '');
            obj.setAttribute('data-elementid', id_komplekt);
            //komplektInBasket(obj);
            BX.removeClass(obj, 'add2basket_0');
            BX.addClass(obj, 'add2basket_'+id_komplekt);
            Basket.add($(obj));


        }
    });
    return false;
}


function recalc_price(obj){
    var obj_jq = $(obj);
    //debugger;
    var div_el = obj_jq.closest('.row');

    var prices = div_el.find('span.price');
    var discount_percent = div_el.find('.complement_discount_percent').first().val();
    var arr = prices.map(function(indx, element){
        return $(element).html();
    });
    //debugger;
    var arValues = arr.get();
    var newprice=0;
    for(var i in arValues){
        newprice = newprice + parseFloat(arValues[i].replace(/\D+/g,""));
        //alert(newprice);
    }
    newprice = newprice - (newprice*discount_percent/100);
    newprice = Math.round(newprice);
    div_el.find('span.complement_price_text').first().html(newprice);
}
*/

$(document).on('click','.komplekt-container .block-sku-detail-sku-container', function(e) { //20171110 dlya knopki komplekta V korzinu

    BX.showWait();
    var obj = e.target;
    var obj_jq = $(obj);
    var div_el = obj_jq.closest('.catalog-products-container');

    var knopki = div_el.find('a.offers:not(.addkomplekt)');
    var komplekt_tip = obj_jq.attr("data-komplekttip");

    var arr = knopki.map(function(indx, element){
        return $(element).attr("data-elementid");
    });
    var arValues = arr.get();

    var basket_id = div_el.find('input.basket_row_id').val();

    $.ajax({
        type:'POST',
        //url:'/ajax/updateBasketComplement.php',
        url:'/ajax/ajax_komplekt.php',
        dataType:'json',
        data:{'values':arValues,
            'basket_id':basket_id,
            'doaction':'changebasketkomplekt'
        },

        timeout:10000,
        success:function(data,textStatus){
            //debugger;
            //alert('success='+data.responseText);
        },
        complete:function(data,textStatus){
            updateBasket();
            $('#'+basket_id+' .bx_ordercart_itemtitle').html(data.responseJSON.NAME);
            BX.closeWait();
        }
    });

});



