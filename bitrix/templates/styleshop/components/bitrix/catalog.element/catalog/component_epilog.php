<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $templateData */

/** @var @global CMain $APPLICATION */

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addString('<meta property="og:type" content="website" />');
Asset::getInstance()->addString('<meta property="og:site_name" content="' . \Bitrix\Main\Config\Option::get('main', 'site_name') . '" />');

$baseUrl = SITE_SERVER_NAME;//$_SERVER['HTTP_HOST'];

if (isset($arResult['PREVIEW_PICTURE']) && !empty($arResult['PREVIEW_PICTURE']))
	Asset::getInstance()->addString('<meta property="og:image" content="' . (CMain::IsHTTPS() ? "https://" : "http://") . $baseUrl . $arResult['PREVIEW_PICTURE']['SRC'] . '" />');

if (isset($arResult['CANONICAL_PAGE_URL']) && !empty($arResult['CANONICAL_PAGE_URL']))
	Asset::getInstance()->addString('<meta property="og:url" content="' . $arResult['CANONICAL_PAGE_URL'] . '" />');

if (!empty($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']))
	Asset::getInstance()->addString('<meta property="og:title" content="' . $arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE'] . '" />');
else
	Asset::getInstance()->addString('<meta property="og:title" content="' . $arResult['NAME'] . '" />');

if (!empty($arResult['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']))
	Asset::getInstance()->addString('<meta property="og:description" content="' . $arResult['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION'] . '" />');

unset($baseUrl);

?>
<? /*x5 20180222 BEGIN выполняем js код для установки ТП, переданного в параметрах компонента и вешаем события, чтобы подменялся УРЛ при переключении ТП*/ ?>
<script type="text/javascript">

    RegExp.escape = function (text) {
        return text.replace(/[-/[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
//        var t = text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
//        return t.replace(/[/]/g, "");
    }

    jQuery.expr[':'].regex = function (elem, index, match) {
        var matchParams = match[3].split(','),
            validLabels = /^(data|css):/,
            attr = {
                method: matchParams[0].match(validLabels) ?
                    matchParams[0].split(':')[0] : 'attr',
                property: matchParams.shift().replace(validLabels, '')
            },
            regexFlags = 'ig',
            regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);
        return regex.test(jQuery(elem)[attr.method](attr.property));
    }

    $(document).ready(function () {
        //alert('kod_dlya_vstavki - <?=$arResult['X5_VSTAVKA_ELEM_ID']?>');

		<?if($arResult['X5_VSTAVKA_ELEM_ID']):?>
        var $cur_vstavka = $('[data-treevalue="<?=$arResult['X5_VSTAVKA_ELEM_ID']?>"]');
        var id_parent = $cur_vstavka.parent().prop('id');
		<?else:?>
        debugger;
        //alert('nonno');
        var container = $('div.detail-product');
        var $cur_vstavka = container.find("ul:regex(id, .*prop_304_list.*)").eq(0); //prop_304_list

        var id_parent = '';
        if ($cur_vstavka.length >= 1)
            id_parent = $cur_vstavka.prop('id');

		<?endif;?>

        //var all_list = JSON.parse('<?=$arResult["VSTAVKA_OPTIONS_JSON"]?>');
        if (id_parent != '') {
            var options = [];

			<?
			foreach($arResult["VSTAVKA_OPTIONS"] as $property_id=>$prop_value_xml_id){?>
            options[ <?=$property_id?> ] = "<?=$prop_value_xml_id?>";
			<?
			}
			?>

            $(document).on("click", "#" + id_parent + ">li", options, function (e) {
                debugger;
                var id_value = $(e.currentTarget).data('onevalue');
                //alert('data_onevalue'+data_value);
                var kod_vstavki = e.data[id_value];

                var current_url = location.href;
                var is_vstavka_in_url = current_url.search(/vstavka/i);

                var new_url = current_url;
                if (kod_vstavki == '15ba2e1b-2c3d-4ea9-ba7a-da2c9326773c') { //если "без вставки"

                    if (is_vstavka_in_url != -1) {
                        new_url = current_url.replace(new RegExp("\/vstavka-.*?\/", 'i'), "/");
                    }

                } else {

                    if (is_vstavka_in_url != -1) {
                        new_url = current_url.replace(new RegExp("\/vstavka-.*?\/", 'i'), "/vstavka-" + kod_vstavki + "/");
                    } else {
                        new_url = current_url.replace(new RegExp("\/$", 'i'), "/vstavka-" + kod_vstavki + "/");
                    }
                }
                if (current_url != new_url) {
                    history.pushState(null, null, new_url);
                }


                history.pushState(null, null, new_url);
            });

			<?if($arResult['X5_VSTAVKA_ELEM_ID']): //если через урл передан парамтер вставки?>

            $cur_vstavka.click();
			<?else: //если урл бер параметра вставки, кликаем активный элемент, чтобы добавить в урл вставку?>
            $cur_vstavka.find('li.active').click();
			<?endif;?>
        }
    });


</script>
<? /*x5 20180222 END выполняем js код для установки ТП, переданного в параметрах компонента и вешаем события, чтобы подменялся УРЛ при переключении ТП*/ ?>
