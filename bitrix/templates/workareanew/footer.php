
<footer id="footer">
<!--	<div class="container hidden-xs hidden-sm"> -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 follow">
						<div class="title">ПРИСОЕДИНЯЙТЕСЬ К НАМ:</div>
						<div class="block socialLinks">
							<a class="vk bx-socialsidebar-icon" target="-blank" href="https://vk.com/clubcaramel_love"></a>
							<a class="in bx-socialsidebar-icon" target="-blank" href="https://www.instagram.com/charm_caramel/"></a>
						</div>	
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<form class="waSubscribe">
							<input class="buttonOrange" type="submit" value="Подписаться на новинки">
							<input type="text" placeholder="Введите ваш E-mail">
							<span class="hidden-md hidden-lg"><i class="fa fa-play" aria-hidden="true"></i></span>
				</form>
			</div>
		</div>
		<div class="row" style="margin-top: 15px">
			
			<div class="col-lg-md col-lg-4 hidden-xs hidden-sm socialWidgets">
					<script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>

					<!-- VK Widget -->
					<div id="vk_groups"></div>
					<script type="text/javascript">
					VK.Widgets.Group("vk_groups", {mode: 0, width: "196", height: "245", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 101006857);
					</script>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 footerMenu">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="title closed">Украшения из серебра<i class="fa fa-chevron-right hidden-md hidden-lg" aria-hidden="true"></i></div>
						<menu>
							<li><a href="/catalog/jewelry/koltsa/" title="Кольца">Кольца</a></li>
							<li><a href="/catalog/jewelry/sergi/" title="Серьги">Серьги</a></li>
							<li><a href="/catalog/jewelry/tsepi/" title="Цепи">Цепи</a></li>
							<li><a href="/catalog/jewelry/podveski/" title="Подвески">Подвески</a></li>
							<li><a href="/catalog/jewelry/braslety/" title="Браслеты">Браслеты</a></li>
							<li><a href="/catalog/jewelry/" title=">Весь каталог →" class="catalog">Весь каталог →</a></li>
						</menu>					
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="title closed">Сервис<i class="fa fa-chevron-right hidden-md hidden-lg" aria-hidden="true"></i></div>
						<menu>
							<li><a href="" title="Примерка на выбор">Примерка на выбор</a></li>
							<li><a href="" title="Доставка">Доставка</a></li>
							<li><a href="" title="Гарантия качества">Гарантия качества</a></li>
							<li><a href="" title="Оплата">Оплата</a></li>
						</menu>			
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="title closed">Полезное<i class="fa fa-chevron-right hidden-md hidden-lg" aria-hidden="true"></i></div>
						<menu>
							<li><a href="/contacts/" title="Где купить, контакты">Где купить, контакты</a></li>
							<li><a href="/partners/" title="Партнерам">Партнерам</a></li>
							<li><a href="/catalog/jewelry/?set_filter=y&arrFilter_132_4261170317=Y" title="Акции и новинки">Акции и новинки</a></li>
							<li><a href="/feedback/" title="Отзывы">Отзывы</a></li>
							<li><a href="/faq/" title="Популярные вопросы">Популярные вопросы</a></li>
						</menu>			
					</div>
				</div>
				<div>

				</div>
			</div>
		</div>
		<div class="row hidden-md hidden-lg">
			<div class="col-xs-12 flogo">
				<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" class="img-responsive"/></a>
				<div style="margin: 10px 0 10px 0!important; font-size: 21px; font-weight: 700">+7 (812) 339 25 37</div>
				<div>Copyright (c) 2007-2016</div>

			</div>
		</div>
	</div>
<div class="container hidden-xs hidden-sm" id="foterForMobile">
	<div class="row">
		<div class="col-xs-12">
			
		</div>
	</div>
</div>
</footer>
</div>
 </div>
 <!-- end ppanel container -->
<script type="text/javascript">
//BX.showWait();
//	var addCode =  <?=$newAID?>;
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter36780320 = new Ya.Metrika({
                    id:36780320,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ut:"noindex"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/36780320?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<div style="display:none;">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76395937-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>