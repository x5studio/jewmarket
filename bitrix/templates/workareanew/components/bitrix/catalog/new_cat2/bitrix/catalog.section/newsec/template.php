<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?

if (!empty($arResult['ITEMS']))
{
	

	$templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);

	$arSkuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		
		foreach ($arResult['SKU_PROPS'] as &$arProp)
		{
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_size full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_size';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.';" title="'.$arOneValue['NAME'].'"><i></i><span class="cnt">'.$arOneValue['NAME'].'</span></li>';
				}
				$templateRow .= '</ul></div>'.
'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'</div></div>';
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_scu full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_scu';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.'; padding-top: '.$strOneWidth.';"><i title="'.$arOneValue['NAME'].'"></i>'.
'<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$arOneValue['PICT']['SRC'].'\');" title="'.$arOneValue['NAME'].'"></span></span></li>';
				}
				$templateRow .= '</ul></div>'.
'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'</div></div>';
			}
			$arSkuTemplate[$arProp['CODE']] = $templateRow;
		}
		unset($templateRow, $arProp);
	}

	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="bx-section-desc <? echo $templateData['TEMPLATE_CLASS']; ?>">
	<!-- <p class="bx-section-desc-post"><?=$arResult["DESCRIPTION"]?></p>-->
</div>
<div id="waJewCatalog" class="catalog_block">
<!-- <div class="bx_catalog_list_home col<? echo $arParams['LINE_ELEMENT_COUNT']; ?> <? echo $templateData['TEMPLATE_CLASS']; ?>"> -->
<!-- <div class="catalog"> -->
	<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{ 

	/*	$product_arr = $arItem["ID"];
		$komplect_flag = false;
		$arKomplekt = array();
		$cc = -1;
		$all_offers = '';
		if ($arItem["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"] != '')
			{
				$komplekt_flag = true;
				$komplekt = $arItem["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"];
				$arFilter = array("PROPERTY_KOMPLEKT_VALUE" => $komplekt);
				$arSelect = array("ID","IBLOCK_ID","NAME","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							if ($arFields["ID"] != $product_arr)
								{
							$cc++;
							$arKomplekt[$cc]["COUNT"] = $cc;
							$arKomplekt[$cc]["ID"] = $arFields["ID"];
							$arKomplekt[$cc]["NAME"] = $arFields["NAME"];
							$pic = CFile::GetFileArray($arFields["DETAIL_PICTURE"]);
							$arKomplekt[$cc]["DETAIL_PICTURE"] = $pic["SRC"];
							$arKomplekt[$cc]["ARTICLE"] = $arFields["PROPERTY_CML2_ARTICLE_VALUE"];
							$arKomplekt[$cc]["DETAIL_PAGE_URL"] = $arFields["DETAIL_PAGE_URL"];
								}
						}	
			$komp = -1;
			$all_stone_komp = array();
			$all_size_komp = array();
			$offers_komp = array();
			foreach ($arKomplekt as $value)
				{
				$komp = $value["COUNT"];
				$res = CCatalogSKU::getOffersList($value["ID"],0,array(),array("ID","CODE"),array());
							$stone = array();
							$size = array();
						$i = -1;
							$count1 = 0;						
						foreach($res[$value["ID"]] as $key1 => $elem)
						{
							$count1++;
							$arFilter = array("ID" => $key1);
							$arSelect = array("ID","PROPERTY_VSTAVKA","PROPERTY_RAZMER"," CATALOG_GROUP_1");

							$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);

							while($ob = $res->GetNextElement())
							{
								$i++;
								
								$arFields = $ob->GetFields();
								//echo $arFields["ID"];
								$res_price = GetCatalogProductPrice($arFields["ID"],1);
								$offers_komp[$komp][$i]["SIZE"] = $arFields["PROPERTY_RAZMER_VALUE"];
								$offers_komp[$komp][$i]["STONE"] = $arFields["PROPERTY_VSTAVKA_VALUE"];
								$offers_komp[$komp][$i]["ID"] = $arFields["ID"];
								$offers_komp[$komp][$i]["PRICE"] = $res_price["PRICE"];
								$all_offers = $all_offers.$arFields["ID"].'-';
								if (array_search($arFields["PROPERTY_RAZMER_VALUE"],$all_size) == false)
									{
										$all_size_komp[$komp][] = $arFields["PROPERTY_RAZMER_VALUE"];
									}
								if (array_search($arFields["PROPERTY_VSTAVKA_VALUE"],$all_stone) == false)
									{
										$all_stone_komp[$komp][] = $arFields["PROPERTY_VSTAVKA_VALUE"];
									}

							}
						}
							$all_size_komp[$komp] = array_unique($all_size_komp[$komp]);
							$all_stone_komp[$komp] = array_unique($all_stone_komp[$komp]);		
				}
			}

		$res = CCatalogSKU::getOffersList($product_arr,0,array(),array("ID","CODE"),array());
					$stone = array();
					$size = array();
					$all_stone = array();
					$all_size = array();
					$offers = array();
				$i = -1;
					$count1 = 0;

				foreach($res[$arItem["ID"]] as $key1 => $elem)
				{
					
					$count1++;
					$arFilter = array("ID" => $key1);
					$arSelect = array("ID","PROPERTY_VSTAVKA","PROPERTY_RAZMER"," CATALOG_GROUP_1");

					$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);

					while($ob = $res->GetNextElement())
					{
						$i++;
						$arFields = $ob->GetFields();
						$res_price = GetCatalogProductPrice($arFields["ID"],1);
						$offers[$i]["SIZE"] = $arFields["PROPERTY_RAZMER_VALUE"];
						$offers[$i]["STONE"] = $arFields["PROPERTY_VSTAVKA_VALUE"];
						$offers[$i]["ID"] = $arFields["ID"];
						$offers[$i]["PRICE"] = $res_price["PRICE"];
						$all_offers = $all_offers.$arFields["ID"].'-';
						if (array_search($arFields["PROPERTY_RAZMER_VALUE"],$all_size) == false)
							{
								$all_size[] = $arFields["PROPERTY_RAZMER_VALUE"];
							}
						if (array_search($arFields["PROPERTY_VSTAVKA_VALUE"],$all_stone) == false)
							{
								$all_stone[] = $arFields["PROPERTY_VSTAVKA_VALUE"];
							}

					}
				}
				
					$all_size = array_unique($all_size);
					$all_stone = array_unique($all_stone);*/
					
	
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

	$minPrice = false;
	if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
		$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
	?>

	
		<div class="col-xs-3 catalog_item">
<?
$articul = $arItem["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["DISPLAY_VALUE"];
$articul = explode('-',$articul);
$num = $articul[1];			
?>
			<?
			if ($arItem["DETAIL_PICTURE"]["SRC"] != '')
			{
				$pic = $arItem["DETAIL_PICTURE"]["SRC"];
			}
			else
			{
				$pic = $arItem['PREVIEW_PICTURE']['SRC'];
			}			
			?>
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="ppic">
				<img src="<?=$pic?>" class="img-responsive" />
			</a>
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="price">
				<?	if (!empty($minPrice))
					{
						if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
						{
							echo GetMessage(
								'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
								array(
									'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
									'#MEASURE#' => GetMessage(
										'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
										array(
											'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
											'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
										)
									)
								)
							);
						}
						else
						{
							echo $minPrice['PRINT_DISCOUNT_VALUE'];
						}
						if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
						{
							?> <? echo $minPrice['PRINT_VALUE']; ?><?
						}
					}
					unset($minPrice);?>	
							 руб				
			</a>
			<div class="title"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo $productTitle; ?></a></div>
			<div class="articul">Артикул: <?=$num?></div>
			<div class="hidden">
				<div class="hidden_block prev_pic">
					<div class="cross"></div>
					<img src="<?
			if ($arItem["DETAIL_PICTURE"]["SRC"] != '')
			{
				echo $arItem["DETAIL_PICTURE"]["SRC"];
			}
			else
			{
				echo $arItem['PREVIEW_PICTURE']['SRC'];
			}			
			?>">					
				</div>
			</div>
			<span title="Увеличить изображение. Если не работает - перезагрузите страницу" class="image_plus"><i class="fa fa-search-plus fa-fw"></i></span>
			<?$tt=1;?>
			<?if ($tt == 7):?>
			<span title="Оптовая форма. Если не работает - перезагрузите страницу" class="oncl"><i class="fa fa-info-circle fa-fw"></i></span>
			<div class="hidden">
			<?
			$count2 = $count1*60+100;
			if ($count2 <= 300)  $count2 = 320;
			?>
				<!-- <div class="hidden_block" style="height: <?echo $count2;?>px"> -->
				<div class="hidden_block">
					<div class="und"></div>
					<div class="cross"></div>
					<div class="close">
						<div class="close_mess">
								<div class="mess">
									Положить в корзину или закрыть?
								</div>
								<br />
								<a class="close_buy">В корзину</a>
								<a class="close_close">Закрыть вкладку</a>
							
						</div>
					</div>
			<div class="stand">
						<div class="img_box">
							<div class="title"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo $productTitle; ?></a></div>
							<div class="articul">Артикул: <?=$num?></div>
							<div class="img"><img src="<? echo $arItem['DETAIL_PICTURE']["SRC"] ?>"/></div>
						</div>

			<form class="add2basket_form">
			<!-- ЦЕПИ -->
			
			<!-- <table>
				<tr>
				<?foreach($all_stone as $stone):?>
					<td><?=$stone?></td>
				<?endforeach?>
				<td>Размер</td>
				<td>Цена</td>
				</tr>
		<?
		
				/*	foreach ($all_size as $size)
						{
							echo '<tr>';
							foreach($all_stone as $stone)
								{
									$active = false;
									echo '<td>';
									foreach($offers as $offer)
										{
											if ($offer["STONE"] == $stone && $offer["SIZE"] == $size )
											{
												echo '<div class="input_top_down"><input class="sel" name="tp'.$offer["ID"].'" value="0">
												<div class="sel_block">
													<a class="plus"></a>
													<a class="minus"></a>
												</div>
												</div>';
												$active = true;
												//break;
											}
										}
									if ($active == false) echo '<div class="input_top_down unenabled" name="disabled"><input class="sel" value="0"></div>';
									echo '</td>';
								}
								echo '<td class="form_size">'.$size.'</td><td>'.$offer["PRICE"].' &#8381;</td></tr>';
						}*/
		?>
			<input type="hidden" name="offers" value="<?=$all_offers?>"/>

			</table> -->
			<table>
			<?$if_first = 0;?>
				<tr>
				<?foreach($all_stone as $stone):?>
					<td><?=$stone?></td>
				<?endforeach?>
				<td>Размер</td>
				<!--<td>Цена</td> -->
				</tr>
		<?
		
					foreach ($all_size as $size)
						{
							echo '<tr>';
							foreach($all_stone as $stone)
								{
									$active = false;
									echo '<td>';
									foreach($offers as $offer)
										{
											if ($offer["STONE"] == $stone && $offer["SIZE"] == $size )
											{
												echo '<div class="input_top_down"><input class="sel" name="tp'.$offer["ID"].'" value="0">
												<div class="sel_block">
													<a class="plus"></a>
													<a class="minus"></a>
												</div>
												</div>';
												$active = true;
												//break;
											}
										}
									if ($active == false) echo '<div class="input_top_down unenabled" name="disabled"><input class="sel" value="0"></div>';
									echo '</td>';
								}
								echo '<td class="form_size">'.$size.'</td></tr>';
						}
						 
		?>
				<tr>
					<?foreach($all_stone as $stone):?>
						<td>
						<?$if_first++;
						if ($if_first ==1) echo('<div class="if_first">ЦЕНА: </div>'); 
						?>
						<?=$offer["PRICE"]?> руб
						</td>
						<td>&nbsp;</td>
					<?endforeach?>
				</tr>
			<input type="hidden" name="offers" value="<?=$all_offers?>"/>

			</table>
			<div class="under_table">
				<div class="action null">Обнулить</div>
				<input class="action" type="submit" value="Купить"/>
			</div>
			</form>	
			</div>

			<?//Торговые предложения комплекта?>
				<?if ($komplekt_flag == true):?>
					<div class="komplekt_box">
					<?foreach($arKomplekt as $item):?>
					<?$komp = $item["COUNT"]?>
					<div class="komplekt_elem">
						<div class="img_box">
							<div class="title"><a href="<? echo $item['DETAIL_PAGE_URL']; ?>"><? echo $item["NAME"] ?></a></div>
							<div class="articul">Артикул: <?=$item["PROPERTY_CML2_ARTICLE_VALUE"]?></div>
							<div class="img"><img src="<? echo $item['DETAIL_PICTURE']; ?>"/></div>
						</div>
			
			<form class="add2basket_form">
			<table>
			<!--<tr>
				<td style="border: 0; margin: 0; padding:0;"><div class="deactive"></div></td>
			</tr> -->
				<tr>
				<?foreach($all_stone as $stone):?>
					<td><?=$stone?></td>
				<?endforeach?>
				
				<?$if_first = 0;?>
				<td>Размер</td>
				<!-- <td>Цена</td> -->
				</tr>
		<?	/*--------------------------------------------*/
			
					foreach ($all_size_komp[$komp] as $size)
						{
							echo '<tr>';
							foreach($all_stone_komp[$komp] as $stone)
								{
									$ui = 0;
									$active = false;
									echo '<td>';
									foreach($offers_komp[$komp] as $offer)
										{
											
										$kompl_inc++;									
										//echo($offer["STONE"]);
										$ui++;
										
											if ($offer["STONE"] == $stone && $offer["SIZE"] == $size )
											{
												$komp_prices[$ui] = $offer["PRICE"];
												echo '<div class="input_top_down"><input class="sel" name="tp'.$offer["ID"].'" value="0">
												<div class="sel_block">
													<a class="plus"></a>
													<a class="minus"></a>
												</div>
												</div>';
												$active = true;
												//break;
											}
										}
									if ($active == false) echo '<div class="input_top_down unenabled" name="disabled"><input class="sel_disabled" value="0"></div>';
									echo '</td>';
								}
								echo '<td class="form_size">'.$size.'</td></tr>';
						}
		?>
				<tr>
					<?foreach($all_stone as $stone):?>
						<td>
						<?$if_first++;
						//if ($if_first ==1) echo('<div class="if_first">ЦЕНА: </div>'); 
						?>
						<?=$komp_prices[$ui]?> &#8381;
						</td>
					<?endforeach?>
						<td>&nbsp;</td>
				</tr>
			</table>
			<div class="under_table">
				<div class="action null">Обнулить</div>
				<input class="action" type="submit" value="Купить"/>
			</div>
			<input type="hidden" name="offers" value="<?=$all_offers?>"/>
			</form>
					</div>
					<?endforeach?>
					</div>
				<?endif?>
				</div>
			</div>
		<?endif?>
		</div>
	
<?
}
/*}*/
?><div style="clear: both;"></div>
</div>
<!-- </div> -->
<div class="basket_success">
	<div class="basket_success_sub">
		Товар добавлен в корзину<br /><br />
		<div class="action">Продолжить</div>
	</div>
</div>
<div class="removement">
</div>
<div id="hideBlock" style="display:none;">
   <h1>Hello</h1>
   <p>text</p>
</div>
<script type="text/javascript">
BX.message({
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
}
?>
<script>
	/*    function AjaxFormRequest() {
				alert ('fgsdfg');
                $.ajax({
                    url:     "/jewcat/add2basket.php", //Адрес подгружаемой страницы
                    type:     "POST", //Тип запроса
                    dataType: "html", //Тип данных
                    data: jQuery("#add2basket").serialize(), 
                    success: function(response) { //Если все нормально
                    document.getElementById(result_id).innerHTML = response;
                },
                error: function(response) { //Если ошибка
                document.getElementById(result_id).innerHTML = "Ошибка при отправке формы";
                }
             });
        }*/

</script>
