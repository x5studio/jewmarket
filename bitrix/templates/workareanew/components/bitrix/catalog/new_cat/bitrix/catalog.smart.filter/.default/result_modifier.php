<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

/*$curDir = explode('/',$APPLICATION->GetCurDir());
$bread[0]["PATH"] = $curDir[0].'/';
$bread[0]["NAME"] = "√лавна¤ страница";
$bread[1]["PATH"] = $bread[0]["PATH"].'offers/jewelry/';
$bread[1]["NAME"] = "ёвелирный каталог";
$curType = $curDir[3];
$curSubType = $curDir[4];
	$firstSec = array();
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 2, "IBLOCK_ID" => 29, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
	   if (!empty($curType) && $curType == $arSect["CODE"])
		{
			$bread[2]["NAME"] = $arSect["NAME"];
			$bread[2]["PATH"] = $bread[1]["PATH"]."".$arSect["CODE"]."/";
		}
		$firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
		$firstSec[$i]["CODE"] = $arSect["CODE"];		
				$Sec1 = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 3, "SECTION_ID" => $arSect["ID"]), array("ID","NAME","CODE"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				   if (!empty($curSubType) && $curSubType == $arSect1["CODE"])
					{
						$bread[3]["NAME"] = $arSect1["NAME"];
						$bread[3]["PATH"] = $bread[2]["PATH"]."".$arSect1["CODE"]."/";
					}
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
					$firstSec[$i]["SUB"][$j]["CODE"] = $arSect1["CODE"];
			   }		   
	}*/
					$curDir = $APPLICATION->GetCurDir();
					$curType = $curDir[1];
					$curSubType = $curDir[2];
					$secPath = array();
					$secParent = array();
					$arFilter1 = array('IBLOCK_ID' => 29, 'ACTIVE' => 'Y', ">=DEPTH_LEVEL" => 1); 
					$arSelect1 = array("ID","NAME","CODE","SECTION_PAGE_URL", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
					$rsSection = CIBlockSection::GetTreeList($arFilter1, $arSelect1); 
					$i = 0;
					while($arSection = $rsSection->GetNext())
						{

							$n = str_replace("catalog","offers", $arSection["SECTION_PAGE_URL"]);
							$arSection["SECTION_PAGE_URL"] = str_replace("catalog","offers", $arSection["SECTION_PAGE_URL"]);
							$i++;
							$kid = $arSection["ID"];
							$sid = $arSection["IBLOCK_SECTION_ID"];
							if ($arSection["DEPTH_LEVEL"] == 2)							
							{
								$secPath[$kid]["NAME"] = $arSection["NAME"];
								$secPath[$kid]["ID"] = $kid;
								$secPath[$kid]["SECTION_PAGE_URL"] = $arSection["SECTION_PAGE_URL"];
								$secPath[$kid]["CHECKED"] =  ($curDir == $arSection["SECTION_PAGE_URL"]) ? "checked" : "" ;
								if ($secPath[$kid]["CHECKED"] == "checked")
								{
									
									$APPLICATION->AddChainItem($secPath[$kid]["NAME"], $secPath[$kid]["SECTION_PAGE_URL"]);
									$secMenu = $secPath[$kid]["ID"];
									$menuBack = $secPath[$kid]["SECTION_PAGE_URL"];
									$curBC["NAME"] = $arSection["NAME"];
/*									$backurl = explode("/",$arSection["SECTION_PAGE_URL"]);
									unset($backurl[count($backurl)-2]);				
									array_values($backurl);		*/						
									$curBC["BACK_URL"] = implode("/",$backurl);
								}
							}
							elseif($arSection["DEPTH_LEVEL"] == 1)
							{
								$secMenu = $kid;
								$rootHref = $arSection["SECTION_PAGE_URL"];
								$curBC["NAME"] = $arSection["NAME"];
/*								$backurl = explode("/",$arSection["SECTION_PAGE_URL"]);
								unset($backurl[count($backurl)-1]);									
								$curBC["BACK_URL"] = implode("/",$backurl);		*/		
								$curBC["BACK_URL"]	= "/";						
							}
							else
							{
								$secPath[$sid]["SUB"][$kid]["NAME"] = $arSection["NAME"];
								$secPath[$sid]["SUB"][$kid]["ID"] = $kid;
								$secPath[$sid]["SUB"][$kid]["SECTION_PAGE_URL"] = $arSection["SECTION_PAGE_URL"];
								if ($curDir == $arSection["SECTION_PAGE_URL"])
								{
									$secPath[$sid]["SUB"][$kid]["CHECKED"] = "checked";
									$secPath[$sid]["CHECKED"] = "checked";
									$menuBack = $arSection["SECTION_PAGE_URL"];
									$APPLICATION->AddChainItem($secPath[$sid]["NAME"], $secPath[$sid]["SECTION_PAGE_URL"]);									
									$APPLICATION->AddChainItem($secPath[$sid]["SUB"][$kid]["NAME"], $secPath[$sid]["SUB"][$kid]["SECTION_PAGE_URL"]);
									$secMenu = $secPath[$sid]["ID"];
									$curBC["NAME"] = $arSection["NAME"];
/*									$backurl = explode("/",$arSection["SECTION_PAGE_URL"]);
									unset($backurl[count($backurl)-2]);				
									array_values($backurl);		*/						
									$curBC["BACK_URL"] = implode("/",$backurl);
								}
							}
//							if (empty($secID)) ?  : ;
							$arParams["SEC_CODE"][$i] = str_replace("/offers/jewelry/","",$arSection["SECTION_PAGE_URL"]);
							
						}
$arResult["SECTIONS"] = $secPath;
$arResult["SECTIONS_ROOT"] = $rootHref;
//echo "<pre>";
//print_r($arParams["SEC_CODE"]);
//echo "</pre>";
/*foreach ($secPath as $k => $v)
{
	$path = $v["SECTION_PAGE_URL"];
	if (!empty($v["SUB"]))
	{
		foreach ($v as $ky => $vy)
			{
				$path .= $vy[]
			}
	}
}*/
foreach ($arResult["ITEMS"][464] as $key => $item)
{
	foreach ($item as $k => &$v)
	{
		$val = strtolower($v["VALUE"]);
		switch ($val)
			{
				case "черный": $arColor = "background: #000"; break;
				case "фиолетовый": $arColor = "background: #ee82ee"; break;
				case "голубой": $arColor = "background: #87ceeb"; break;
				case "зеленый": $arColor = "background: #008000"; break;
				case "коричневый": $arColor = "background: #cd853f"; break;
				case "красный": $arColor = "background: #dc143c"; break;
				case "синий": $arColor = "background: #0000ff"; break;
				case "безцветный": $arColor = "url('".$templateFolder."/images/outcolor.png'); width: 21px; height: 21px; border: 1px solid rgba(0,0,0,0.5); background-size: 21px;"; break;
				case "желтый" : $arColor = "background: #e5e500"; break;
				case "розовый" : $arColor = "background: #FF69B4"; break;
				case "сиреневый" : $arColor = "background: #c8a2c8"; break;
				case "белый": $arColor = "background: #fff; width: 21px; height: 21px; border: 1px solid rgba(0,0,0,0.5);"; break;
						default: $arColor = "background: #9b9b9b";				
			}
		$v["INSERT_COLOR"] = $arColor;
		$v["DISPLAY_TYPE"] = "C";
	}	
}
unset($v);
$arResult["ITEMS"][464]["DISPLAY_TYPE"] = "C";
$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";
$arParams["DISPLAY_ELEMENT_COUNT"] = "N";
if ($arParams["SECTION_MENU_MOBILE"] == "Y")
{
	// без использования SEF и с ним
	if ($arParams["SEF_MODE"] == "N")
	{
		$curDirParams = explode("set_filter",$APPLICATION->GetCurUri(true));
		$arResult["WASM_RELOAD"] = (empty($curDirParams[1])) ? "N" : "Y";	
	}
	else
	{
		$arResult["WASM_RELOAD"] = "NaN";
	}
	$keys = array(
	465 => "fa-venus-mars", 
	459 => "fa-eyedropper", 
	463 => "fa-flask", 
	461 => "fa-info", 
	488 => "fa-diamond");
	$html = "";
	//array_key_exists 
//	$html .= '<ul id="waSMFBlock" data-node="'.$secID.'" class="waSMSubFilterMenu waSMSubCatShow not-ready">';
	if (empty($arResult["MOBILE_FILTER"]))
	{
		foreach ($arResult["ITEMS"] as $k => $v)
		{
			if (array_key_exists($k, $keys))
			{	
				if (!empty($v["VALUES"]))
				{
				$ff = false;
				foreach ($v["VALUES"] as $kk => $ar)
				{	
					if ($ar["CHECKED"]) 
					{
					$ff = true;
					$isCh = '<i class="fa '.$keys[$k].' orange" aria-hidden="true"></i>';					
					}

				}
				if ($ff != true)  $isCh = '<i class="fa '.$keys[$k].'" aria-hidden="true"></i>';	
				$html .= '<li class="waSMFName" data-filter="'.$k.'">';
				$html .= '									
				<div class="fpic">'. $isCh .'
					<span class="fname">'.$v["NAME"].'</span>
					<span id="count_'.$k.'" class="fcount"></span>							
				</div>';
/*				$ff = false;
				foreach ($v["VALUES"] as $kk => $ar)
				{	
					if ($ar["CHECKED"]) 
					{
					$ff = true;
					$html .= '<div style="display:none;" class="finput inSelect">'.$ar["VALUE"].'</div>';						
					}

				}
				if ($ff != true)  $html .= '<div style="display:none;" class="finput">не выбрано</div>';	*/
/*				$t = array_search(1,$v["VALUES"])
				if ($t)
				{
				$html .= '<div class="finput">'.$v["VALUES"][$t]["NAME"]'</div>';
				}
				else
				{
				$html .= '<div class="finput">не выбрано</div>';					
				}	*/
				$html .= '<ul class="subMenu">';
				
				foreach ($v["VALUES"] as $kk => $ar)
				{
//	<li class="felem" data-lid="'.$ar["CONTROL_ID"].'" onclick="waJSMM.click(this);">'.$ar["VALUE"].'</li>'		
					if ($ar["CHECKED"])
					{
						
					$html .= '
					<li class="felem checked" data-lid="'.$ar["CONTROL_ID"].'" onclick="waJSMM.clickFilter(this);">'.$ar["VALUE"].'</li>';
					}
					else
					{
					$html .= '
					<li class="felem" data-lid="'.$ar["CONTROL_ID"].'" onclick="waJSMM.clickFilter(this);">'.$ar["VALUE"].'</li>';						
					}
				}
				$html .= '
				</ul>';
				
				$html .= '</li>';
			}
			}
		}
	//	$html .= '</ul>';
	//	$outerhtml = htmlspecialcharsEx($html);
	//	$html = str_replace("\n", "", $outerhtml);
		$arResult["MOBILE_FILTER"]["MENU"] = htmlspecialcharsEx($html);
		$arResult["MOBILE_FILTER"]["BC"] = htmlspecialcharsEx($menuBack);
	}
	
//	foreach ($arResult as $key => &$item)
}
/*$arResult["waSEF"] = $GLOBALS["waSEF"];*/
//print_r($arResult);
//$isFilter = strpos($curDir,"set_filter");

/*if (strpos($curDir,"set_filter")){
	print_r($curDir);
$arResult["WASM_RELOAD"] = "RELOAD";
}*/