<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);
?>
	<?
$curDir = explode('/',$APPLICATION->GetCurDir());
$APPLICATION->SetPageProperty("bc_cat1", 'sdfsdf');
$bread[0]["PATH"] = $curDir[0].'/';
$bread[0]["NAME"] = "Главная страница";
$bread[1]["PATH"] = $bread[0]["PATH"].'catalog/jewelry/';
$bread[1]["NAME"] = "Ювелирный каталог";
$curType = $curDir[3];
$curSubType = $curDir[4];
$curElem = $curDir[5];
$curElemName = $arResult["NAME"];
	$firstSec = array();
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 2, "IBLOCK_ID" => 29, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
	   if (!empty($curType) && $curType == $arSect["CODE"])
		{
			$bread[2]["NAME"] = $arSect["NAME"];
			$bread[2]["PATH"] = $bread[1]["PATH"]."".$arSect["CODE"]."/";
		}
        $firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
		$firstSec[$i]["CODE"] = $arSect["CODE"];
				$Sec1 = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 3, "SECTION_ID" => $arSect["ID"]), array("ID","NAME","CODE"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				   if (!empty($curSubType) && $curSubType == $arSect1["CODE"])
					{
						$bread[3]["NAME"] = $arSect1["NAME"];
						$bread[3]["PATH"] = $bread[2]["PATH"]."".$arSect1["CODE"]."/";
					}
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
					$firstSec[$i]["SUB"][$j]["CODE"] = $arSect1["CODE"];
			   }		   
   }
$bread[4]["NAME"] = $arResult["NAME"];
$bread[4]["PATH"] = $curDir[5];
?>
<div class="breadcrumb">
	<div class="bread_inn">
	<?$arr_count = count($bread);?>
		<a href="<?=$bread[0]["PATH"]?>"><span><?=$bread[0]["NAME"]?></span></a><i class="fa fa-angle-right"></i>
		<?if ($arr_count == 2):?>
		<span><?=$bread[1]["NAME"]?></span>
		<?else:?>
			<a href="<?=$bread[1]["PATH"]?>"><span><?=$bread[1]["NAME"]?></span></a><i class="fa fa-angle-right"></i>
		<?endif?>
		<?if (!empty($curType)):?>
			<?if ($arr_count == 3):?>
			<span><?=$bread[2]["NAME"]?></span>
			
			<?else:?>
			<a href="<?=$bread[2]["PATH"]?>"><span><?=$bread[2]["NAME"]?></span></a><i class="fa fa-angle-right"></i>	
			<?endif?>
			<?if (!empty($curSubType)):?>
				<?if ($arr_count == 4):?>
					<span><?=$bread[3]["NAME"]?></span>
				<?else:?>
					
					<a href="<?=$bread[3]["PATH"]?>"><span><?=$bread[3]["NAME"]?></span></a><i class="fa fa-angle-right"></i>
				<?endif;?>
			<?endif?>
		<?endif?>
		<?if (!empty($curSubType)):?>
			<span><?=$curElemName?></span>
		<?endif?>
		<a class="elemexit" href="<?=$_SERVER['HTTP_REFERER']?>"><span>< &nbsp; Назад</span></a>
	</div>
</div>
<div class="content">
	<div class="workarea">
<?/*$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.small", 
	"ajax_basket", 
	array(
		"COMPONENT_TEMPLATE" => "ajax_basket",
		"PATH_TO_BASKET" => "/personal/basket.php",
		"PATH_TO_ORDER" => "/personal/order.php",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N"
	),
	false
)*/?>	
<div class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>" id="<? echo $arItemIDs['ID']; ?>" itemscope itemtype="http://schema.org/Product">
<?
if ('Y' == $arParams['DISPLAY_NAME'])
{
?>
<?
$product_arr = $arResult["ID"];

$cc = -1;
		if ($arResult["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"] != '')
			{
				$komplekt_flag = true;
				$komplekt = $arResult["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"];
				$arFilter = array("PROPERTY_KOMPLEKT_VALUE" => $komplekt, "IBLOCK_ID" => $arResult["IBLOCK_ID"]);
				$arSelect = array("ID","IBLOCK_ID","NAME","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
							
							$arFields = $ob->GetFields();
							if ($arFields["ID"] != $product_arr)
								{
							$cc++;
							$arKomplektD[$cc]["COUNT"] = $cc;
							$arKomplektD[$cc]["ID"] = $arFields["ID"];
							$arKomplektD[$cc]["NAME"] = $arFields["NAME"];
							$pic = CFile::GetFileArray($arFields["DETAIL_PICTURE"]);
							$arKomplektD[$cc]["DETAIL_PICTURE"] = $pic["SRC"];
							$arK = explode('-',$arFields["PROPERTY_CML2_ARTICLE_VALUE"]);
							$arKomplektD[$cc]["ARTICLE"] = $arK[1];
							$arKomplektD[$cc]["DETAIL_PAGE_URL"] = $arFields["DETAIL_PAGE_URL"];
								}
						}	
			}
?>
<?
}
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
	<div class="bx_item_container" >
<div style="display: block; position: absolute; top: 400px; left: 35px; z-index: 9999;" class="ya-share2" 
data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,blogger,lj,viber"
data-image="<? echo $arFirstPhoto['SRC']; ?>"
data-description="<? echo $arResult['DETAIL_TEXT']; ?>"
data-title="<?
	echo (
		isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
		? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
		: $arResult["NAME"]); ?>"></div>
		<div class="bx_lt">
<div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
	<div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
	<div class="bx_bigimages_imgcontainer" >
	<span class="bx_bigimages_aligner">
	<img itemprop="image" id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>">

	</span>
<?
if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
{
	if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
	{
		if (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF'])
		{
?>
	<div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>"><? echo -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
<?
		}
	}
	else
	{
?>
	<div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" style="display: none;"></div>
<?
	}
}
if ($arResult['LABEL'])
{
?>
	<div class="bx_stick average left top" id="<? echo $arItemIDs['STICKER_ID'] ?>" title="<? echo $arResult['LABEL_VALUE']; ?>"><? echo $arResult['LABEL_VALUE']; ?></div>
<?
}
?>
	</div>
	</div>
<?
if ($arResult['SHOW_SLIDER'])
{
	if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
	{
		if (5 < $arResult['MORE_PHOTO_COUNT'])
		{
			$strClass = 'bx_slider_conteiner full';
			$strOneWidth = (100/$arResult['MORE_PHOTO_COUNT']).'%';
			$strWidth = (20*$arResult['MORE_PHOTO_COUNT']).'%';
			$strSlideStyle = '';
		}
		else
		{
			$strClass = 'bx_slider_conteiner';
			$strOneWidth = '20%';
			$strWidth = '100%';
			$strSlideStyle = 'display: none;';
		}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
	<div class="bx_slider_scroller_container">
	<div class="bx_slide">
	<ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
<?
		foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto)
		{
?>
	<li data-value="<? echo $arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
		}
		unset($arOnePhoto);
?>
	</ul>
	</div>
	<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	</div>
	</div>
<?
	}
	else
	{
		foreach ($arResult['OFFERS'] as $key => $arOneOffer)
		{
			if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
				continue;
			$strVisible = ($key == $arResult['OFFERS_SELECTED'] ? '' : 'none');
			if (5 < $arOneOffer['MORE_PHOTO_COUNT'])
			{
				$strClass = 'bx_slider_conteiner full';
				$strOneWidth = (100/$arOneOffer['MORE_PHOTO_COUNT']).'%';
				$strWidth = (20*$arOneOffer['MORE_PHOTO_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_slider_conteiner';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'].$arOneOffer['ID']; ?>" style="display: <? echo $strVisible; ?>;">
	<div class="bx_slider_scroller_container">
	<div class="bx_slide">
	<ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>">
<?
			foreach ($arOneOffer['MORE_PHOTO'] as &$arOnePhoto)
			{
?>
	<li data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
			}
			unset($arOnePhoto);
?>
	</ul>
	</div>
	<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
	</div>
	</div>
<?
		}
	}
}
?>
<?/*$APPLICATION->IncludeComponent(
	"bitrix:sp-artgroup.share", 
	"template1", 
	array(
		"COMPONENT_TEMPLATE" => "template1",
		"HOW_INC" => "local",
		"SERVICES" => array(
			0 => "vkontakte",
			1 => "facebook",
			2 => "twitter",
			3 => "odnoklassniki",
			4 => "moimir",
			5 => "gplus",
			6 => "lj",
			7 => "friendfeed",
			8 => "moikrug",
			9 => "surfingbird",
		),
		"SHOW" => "button"
	),
	false
);*/?> 
<?/*$APPLICATION->IncludeComponent(
	"bitrix:main.share",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"HANDLERS" => array("mailru","facebook","vk","lj","delicious","twitter"),
		"HIDE" => "N",
		"PAGE_TITLE" => "",
		"PAGE_URL" => "",
		"SHORTEN_URL_KEY" => "",
		"SHORTEN_URL_LOGIN" => ""
	)
);*/?>

</div>
		</div>
		<div class="bx_rt">
<div class="bx_item_title"><h1><span itemprop="name"><?
	echo (
		isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
		? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
		: $arResult["NAME"]
	); ?>
</span></h1>
<?
$articul = $arResult["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["DISPLAY_VALUE"];
$articul = explode('-',$articul);
$num = $articul[1];
?>
<div class="article">Артикул: <span><?=$num?></span></div>
</div>
<?if (!empty($arResult["DISPLAY_PROPERTIES"]["KOLLEKTSIYA"]["DISPLAY_VALUE"])):?>
<div class="elem_collection">
	<div class="coll_title"><a>Коллекция <?=$arResult["DISPLAY_PROPERTIES"]["KOLLEKTSIYA"]["DISPLAY_VALUE"];?></a></div>
	<div class="coll_img">
		
	</div>
</div>
<?endif?>
<?
/*$product_arr = $arResult["ID"];
		$komplect_flag = false;
		$arKomplekt = array();
		$cc = -1;
		$all_offers = '';
		if ($arResult["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"] != '')
			{
				$komplekt_flag = true;
				$komplekt = $arResult["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"];
				$arFilter = array("PROPERTY_KOMPLEKT_VALUE" => $komplekt);
				$arSelect = array("ID","IBLOCK_ID","NAME","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							if ($arFields["ID"] != $product_arr)
								{
							$cc++;
							$arKomplekt[$cc]["COUNT"] = $cc;
							$arKomplekt[$cc]["ID"] = $arFields["ID"];
							$arKomplekt[$cc]["NAME"] = $arFields["NAME"];
							$pic = CFile::GetFileArray($arFields["DETAIL_PICTURE"]);
							$arKomplekt[$cc]["DETAIL_PICTURE"] = $pic["SRC"];
							$arKomplekt[$cc]["ARTICLE"] = $arFields["PROPERTY_CML2_ARTICLE_VALUE"];
							$arKomplekt[$cc]["DETAIL_PAGE_URL"] = $arFields["DETAIL_PAGE_URL"];
								}
						}	
			$komp = -1;
			$all_stone_komp = array();
			$all_size_komp = array();
			$offers_komp = array();
			foreach ($arKomplekt as $value)
				{
				$komp = $value["COUNT"];
				$res = CCatalogSKU::getOffersList($value["ID"],0,array(),array("ID","CODE"),array());
							$stone = array();
							$size = array();
						$i = -1;
							$count1 = 0;						
						foreach($res[$value["ID"]] as $key1 => $elem)
						{
							$count1++;
							$arFilter = array("ID" => $key1);
							$arSelect = array("ID","PROPERTY_VSTAVKA","PROPERTY_RAZMER"," CATALOG_GROUP_1");

							$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);

							while($ob = $res->GetNextElement())
							{
								$i++;
								
								$arFields = $ob->GetFields();
								$res_price = GetCatalogProductPrice($arFields["ID"],1);
								$offers_komp[$komp][$i]["SIZE"] = $arFields["PROPERTY_RAZMER_VALUE"];
								$offers_komp[$komp][$i]["STONE"] = $arFields["PROPERTY_VSTAVKA_VALUE"];
								$offers_komp[$komp][$i]["ID"] = $arFields["ID"];
								$offers_komp[$komp][$i]["PRICE"] = $res_price["PRICE"];
								$all_offers = $all_offers.$arFields["ID"].'-';
								if (array_search($arFields["PROPERTY_RAZMER_VALUE"],$all_size) == false)
									{
										$all_size_komp[$komp][] = $arFields["PROPERTY_RAZMER_VALUE"];
									}
								if (array_search($arFields["PROPERTY_VSTAVKA_VALUE"],$all_stone) == false)
									{
										$all_stone_komp[$komp][] = $arFields["PROPERTY_VSTAVKA_VALUE"];
									}

							}
						}
							$all_size_komp[$komp] = array_unique($all_size_komp[$komp]);
							$all_stone_komp[$komp] = array_unique($all_stone_komp[$komp]);		
				}
			}


		$res = CCatalogSKU::getOffersList($product_arr,0,array(),array("ID","CODE"),array());
					$stone = array();
					$size = array();
					$all_stone = array();
					$all_size = array();
					$offers = array();
				$i = -1;
					$count1 = 0;

			
				foreach($res[$product_arr] as $key1 => $elem)
				{
					$count1++;
					$arFilter = array("ID" => $key1);
					$arSelect = array("ID","PROPERTY_VSTAVKA","PROPERTY_RAZMER"," CATALOG_GROUP_1");

					$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);

					while($ob = $res->GetNextElement())
					{
						$i++;
						$arFields = $ob->GetFields();
						$res_price = GetCatalogProductPrice($arFields["ID"],1);
						$offers[$i]["SIZE"] = $arFields["PROPERTY_RAZMER_VALUE"];
						$offers[$i]["STONE"] = $arFields["PROPERTY_VSTAVKA_VALUE"];
						$offers[$i]["ID"] = $arFields["ID"];
						$offers[$i]["PRICE"] = $res_price["PRICE"];
						$all_offers = $all_offers.$arFields["ID"].'-';
						if (array_search($arFields["PROPERTY_RAZMER_VALUE"],$all_size) == false)
							{
								$all_size[] = $arFields["PROPERTY_RAZMER_VALUE"];
							}
						if (array_search($arFields["PROPERTY_VSTAVKA_VALUE"],$all_stone) == false)
							{
								$all_stone[] = $arFields["PROPERTY_VSTAVKA_VALUE"];
							}

					}
				}
					$all_size = array_unique($all_size);
					$all_stone = array_unique($all_stone);
*/?>
<?
$useBrands = ('Y' == $arParams['BRAND_USE']);
$useVoteRating = ('Y' == $arParams['USE_VOTE_RATING']);
if ($useBrands || $useVoteRating)
{
?>
	<div class="bx_optionblock">
<?
	if ($useVoteRating)
	{
		?><?$APPLICATION->IncludeComponent(
			"bitrix:iblock.vote",
			"stars",
			array(
				"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ELEMENT_ID" => $arResult['ID'],
				"ELEMENT_CODE" => "",
				"MAX_VOTE" => "5",
				"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
				"SET_STATUS_404" => "N",
				"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
				"CACHE_TYPE" => $arParams['CACHE_TYPE'],
				"CACHE_TIME" => $arParams['CACHE_TIME']
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}
	if ($useBrands)
	{
		?><?$APPLICATION->IncludeComponent("bitrix:catalog.brandblock", ".default", array(
			"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"ELEMENT_ID" => $arResult['ID'],
			"ELEMENT_CODE" => "",
			"PROP_CODE" => $arParams['BRAND_PROP_CODE'],
			"CACHE_TYPE" => $arParams['CACHE_TYPE'],
			"CACHE_TIME" => $arParams['CACHE_TIME'],
			"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
			"WIDTH" => "",
			"HEIGHT" => ""
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}
?>
	</div>
<?
}
unset($useVoteRating, $useBrands);
?>
<div class="bx_lb_sale">
<div class="item_price">
<?
$minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
?>
	<div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style=""><? echo ($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></div>
	<div class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>"><? echo $minPrice['PRINT_DISCOUNT_VALUE']; ?></div><span>руб</span>
	<div class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style=""><? echo ($boolDiscountShow ? GetMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $minPrice['PRINT_DISCOUNT_DIFF'])) : ''); ?></div>
	
	
</div>
<div class="item_info_section_buy">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	$canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
}
else
{
	$canBuy = $arResult['CAN_BUY'];
}
$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

$showSubscribeBtn = false;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y')
{
	if ($arParams['SHOW_BASIS_PRICE'] == 'Y')
	{
		$basisPriceInfo = array(
			'#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
			'#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
		);
?>
		
<?
	}
?>
	
	<div class="item_buttons vam">
		<div class="elem_quant">Количество:</div>
		<span class="item_buttons_counter_block">
			<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>"><i class="fa fa-minus fa-fw"></i></a>
			<input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
					? 1
					: $arResult['CATALOG_MEASURE_RATIO']
				); ?>">
			<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>"><i class="fa fa-plus fa-fw"></i></a>
			<span class="bx_cnt_desc" style="display: none;" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
		</span>
		<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
	if ($showBuyBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
	}
	if ($showAddBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart detail_onbuy" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><i class="fa fa-shopping-cart fa-fw"></i>Купить</a>
<?
	}
?>
		</span>
		<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
<?
	if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn)
	{
?>
		<span class="item_buttons_counter_block">
<?
		if ($arParams['DISPLAY_COMPARE'])
		{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
<?
		}
		if ($showSubscribeBtn)
		{

		}
?>
		</span>
<?
	}
?>
	</div>
<?
	if ('Y' == $arParams['SHOW_MAX_QUANTITY'])
	{
		if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
		{
?>
	<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
<?
		}
		else
		{
			if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO'])
			{
?>
	<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
<?
			}
		}
	}
}
else
{
?>
	<div class="item_buttons vam">
		<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
		
<?
	if ($showBuyBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
	}
	if ($showAddBtn)
	{
?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
	}
?>
		</span>
		<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
<?
	if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn)
	{
		?>
		<span class="item_buttons_counter_block">
	<?
	if ($arParams['DISPLAY_COMPARE'])
	{
		?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
	<?
	}
	if ($showSubscribeBtn)
	{

	}
?>
		</span>
<?
	}
?>
	</div>
<?
}
unset($showAddBtn, $showBuyBtn);
?>
</div>
<div class="item_info_section_buy">
<?
$tt = 8;
?>
<?if ($tt == 3):?>
<div class="whoresale"><i class="fa fa-diamond fa-fw"></i>Опт</div>
<?endif;?>
</div>

<?if ($tt == 3):?>
<div class="hidden">
				<div class="hidden_block">
					<div class="und"></div>
					<div class="cross"></div>
					<div class="close">
						<div class="close_mess">
								<div class="mess">
									Положить в корзину или закрыть?
								</div>
								<br />
								<a class="close_buy">В корзину</a>
								<a class="close_close">Закрыть вкладку</a>
							
						</div>
					</div>
			<div class="stand">
						<div class="img_box">
							<div class="title"><a href="<? echo $arResult['DETAIL_PAGE_URL']; ?>"><? echo $arResult["NAME"]; ?></a></div>
							<div class="articul">Артикул: <?=$arResult["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["DISPLAY_VALUE"]?></div>
							<div class="img"><img src="<? echo $arFirstPhoto['SRC']; ?>"/></div>
						</div>

			<form class="add2basket_form">
			<!-- ЦЕПИ -->
			
			<!-- <table>
				<tr>
				<?foreach($all_stone as $stone):?>
					<td><?=$stone?></td>
				<?endforeach?>
				<td>Размер</td>
				<td>Цена</td>
				</tr>
		<?
		
				/*	foreach ($all_size as $size)
						{
							echo '<tr>';
							foreach($all_stone as $stone)
								{
									$active = false;
									echo '<td>';
									foreach($offers as $offer)
										{
											if ($offer["STONE"] == $stone && $offer["SIZE"] == $size )
											{
												echo '<div class="input_top_down"><input class="sel" name="tp'.$offer["ID"].'" value="0">
												<div class="sel_block">
													<a class="plus"></a>
													<a class="minus"></a>
												</div>
												</div>';
												$active = true;
												//break;
											}
										}
									if ($active == false) echo '<div class="input_top_down unenabled" name="disabled"><input class="sel" value="0"></div>';
									echo '</td>';
								}
								echo '<td class="form_size">'.$size.'</td><td>'.$offer["PRICE"].' &#8381;</td></tr>';
						}*/
		?>
			<input type="hidden" name="offers" value="<?=$all_offers?>"/>

			</table> -->
			<table>
			<?$if_first = 0;?>
				<tr>
				<?foreach($all_stone as $stone):?>
					<td><?=$stone?></td>
				<?endforeach?>
				<td>Размер</td>
				<!--<td>Цена</td> -->
				</tr>
		<?
		
					foreach ($all_size as $size)
						{
							echo '<tr>';
							foreach($all_stone as $stone)
								{
									$active = false;
									echo '<td>';
									foreach($offers as $offer)
										{
											if ($offer["STONE"] == $stone && $offer["SIZE"] == $size )
											{
												echo '<div class="input_top_down"><input class="sel" name="tp'.$offer["ID"].'" value="0">
												<div class="sel_block">
													<a class="plus"></a>
													<a class="minus"></a>
												</div>
												</div>';
												$active = true;
												//break;
											}
										}
									if ($active == false) echo '<div class="input_top_down unenabled" name="disabled"><input class="sel" value="0"></div>';
									echo '</td>';
								}
								echo '<td class="form_size">'.$size.'</td></tr>';
						}
						 
		?>
				<tr>
					<?foreach($all_stone as $stone):?>
						<td>
						<?$if_first++;
						if ($if_first ==1) echo('<div class="if_first">ЦЕНА: </div>'); 
						?>
						<?=$offer["PRICE"]?> &#8381;
						</td>
						<td>&nbsp;</td>
					<?endforeach?>
				</tr>
			<input type="hidden" name="offers" value="<?=$all_offers?>"/>

			</table>
			<div class="under_table">
				<div class="action null">Обнулить</div>
				<input class="action" type="submit" value="Купить"/>
			</div>
			</form>	
			</div>		
			</div>
			</div>
<?endif;?>
</div>
<?
unset($minPrice);
if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
{
?>
<div class="char">
<div class="item_info_section_text">
<?
if ('' != $arResult['DETAIL_TEXT'])
{
?>
	<div class="bx_item_description" itemprop="description">
<?
	if ('html' == $arResult['DETAIL_TEXT_TYPE'])
	{
		echo $arResult['DETAIL_TEXT'];
	}
	else
	{
		?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
	}
?>
	</div>
<?
}
?>
</div>
<?
	if (!empty($arResult['DISPLAY_PROPERTIES']))
	{
?>
	<div class="attr_title">Характеристики</div>
	<table class="attr" cellpadding="0" cellspacing="0">
<?	
/*echo '<pre>';
print_r($arResult['DISPLAY_PROPERTIES']);
echo '</pre>';*/
		foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
		{?>
		<?//$arOneProp['NAME'] = mb_strtolower($arOneProp['NAME'],'UTF-8');
		//$arOneProp['NAME'] = mb_convert_case($arOneProp['NAME'], MB_CASE_TITLE, "UTF-8");
		//$arOneProp['NAME'] = ucfirst($arOneProp['NAME']);
		?>
		<?if ($arOneProp['NAME'] != "Комплект" && $arOneProp['NAME'] != "Проба"):?>
		<?if ($arOneProp['NAME'] == "Артикул")
			{
				$articul = $arOneProp['DISPLAY_VALUE'];
				$articul = explode('-',$articul);
				$arOneProp['DISPLAY_VALUE'] = $articul[1];				
			}
		?>
		<?if ($arOneProp['NAME'] != 'Артикул'):?>
		<tr><td><? echo $arOneProp['NAME']; ?></td>
		<?$help_style = ''; if ($arOneProp["HELP"] != '') $help_style='class="attr_active"';?>
		<td <?=$help_style?>><?
			echo (
				is_array($arOneProp['DISPLAY_VALUE'])
				? implode(' / ', $arOneProp['DISPLAY_VALUE'])
				: $arOneProp['DISPLAY_VALUE']
			); ?>
			<?if ($arOneProp["HELP"] != ''):?>
				<div class="attr_help">
					<?=$arOneProp["HELP"];?>
				</div>
			<?endif?>
			</td></tr>
		<?endif;?>
		<?endif;?>
		<?
		}
		unset($arOneProp);
?>
	</table>
<?
	}
	if ($arResult['SHOW_OFFERS_PROPS'])
	{
?>
	<dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></dl>
<?
	}
?> 

<?
}
if ('' != $arResult['PREVIEW_TEXT'])
{
	if (
		'S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
		|| ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
	)
	{
?>
<div class="item_info_section">
<?
		echo ('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>');
?>
</div>
<?
	}
}
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP']))
{
	$arSkuProps = array();
?>
 <div class="item_info_section_offer" id="<? echo $arItemIDs['PROP_DIV']; ?>"> 
 <table cellpadding="0" cellspacing="0">
<?
	foreach ($arResult['SKU_PROPS'] as &$arProp)
	{
		if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
			continue;
		$arSkuProps[] = array(
			'ID' => $arProp['ID'],
			'SHOW_MODE' => $arProp['SHOW_MODE'],
			'VALUES_COUNT' => $arProp['VALUES_COUNT']
		);
		if ('TEXT' == $arProp['SHOW_MODE'])
		{
			if (5 < $arProp['VALUES_COUNT'])
			{
				$strClass = 'bx_item_detail_size full';
				$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
				$strWidth = (20*$arProp['VALUES_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_item_detail_size';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
			if ($arProp['VALUES_COUNT'] <=2)
				{
					$i = -1;
					foreach ($arProp['VALUES'] as $arOneValue)
					{
						if ($arOneValue["NAME"] == "БЕЗ ВСТАВОК" || $arOneValue["NAME"] == "БЕЗ РАЗМЕРА") $offer_show = false;
					
					}

				}
				else
				{
					$offer_show = true;
				}
			$offer_style = ($offer_show == false ? 'style="display: none!important"' : "");

?>
	<?//if (count($arProp['VALUES']) > 2):?>
	<?//if ($offer_show != false):?>
	<tr <?=$offer_style;?> >
	<td><?echo htmlspecialcharsex($arProp['NAME']);?></td>
	<td>
		<span class="offer_show"><i class="fa fa-sort"></i> Выбрать</span>
	</td>
	</tr>
	<tr class="offer_hidden" <?=$offer_style;?>>
	<td colspan="2">
	<?//print_r($arProp['VALUES']);?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
		<span class="bx_item_section_name_gray">&nbsp;</span>
		<div class="bx_size_scroller_container"><div class="bx_size">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>">
<i title="<? echo $arOneValue['NAME']; ?>"></i><span class="cnt" title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span></li>
<?
			}
?>
			</ul>
			</div>
			<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
			<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		</div>
	</div>
	</td>
	</tr>
	<?//endif?>
<?
		}
		elseif ('PICT' == $arProp['SHOW_MODE'])
		{
			if (5 < $arProp['VALUES_COUNT'])
			{
				$strClass = 'bx_item_detail_scu full';
				$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
				$strWidth = (20*$arProp['VALUES_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_item_detail_scu';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
		<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
		<div class="bx_scu_scroller_container"><div class="bx_scu">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID'] ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>; display: none;" >
<i title="<? echo $arOneValue['NAME']; ?>"></i>
<span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');" title="<? echo $arOneValue['NAME']; ?>"></span></span></li>
<?
			}
?>
			</ul>
			</div>
			<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
			<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		</div>
	</div>
<?
		}
	}
	unset($arProp);
?>
	</table>
</div>

</div> <!-- char -->
<?
}
?>

			<div class="clb"></div>
		</div>

		<div class="bx_md">
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	if ($arResult['OFFER_GROUP'])
	{
		foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID)
		{
?>
	<span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
		"ELEMENT_ID" => $offerID,
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
?>
	</span>
<?
		}
	}
}
else
{
	if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
	{
?><?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $arResult["ID"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
	}
}
?>
</div>
		</div>
					<?if (!empty($arKomplektD)):?>
		<div class="bx_rb">
					<div class="komplekt_box">
<div class="koplekt_title">Комплекты:</div>
					<?foreach($arKomplektD as $item):?>
					<?$komp = $item["COUNT"]?>
					<div class="catalog_item">
							<a class="a_pic" href="<? echo $item['DETAIL_PAGE_URL']; ?>" style="background-image: url(<?=$item["DETAIL_PICTURE"]?>)"></a>							
							<div class="title"><a href="<? echo $item['DETAIL_PAGE_URL']; ?>"><? echo $item["NAME"] ?></a></div>
							<div class="articul">Артикул: <?=$item["ARTICLE"]?></div>

					</div>
					<?endforeach;?>
					</div>	
		</div>
		<?endif?>
<?
class WAPrice {
	function  getByID ($id, $priceId) {
		$arr = CCatalogSKU::getOffersList($id,0,array(),array("ID","CODE"),array());
		foreach ($arr[$id] as $key => $item)
			{
								$db_res = CPrice::GetList(
									array(),
									array(
											"PRODUCT_ID" => $key,
											"CATALOG_GROUP_ID" => $priceId
										)
								);
								if ($ar_res = $db_res->Fetch())
								{
									$result = CurrencyFormat($ar_res["PRICE"], $ar_res["CURRENCY"]);
								}
			}
		return $result;
	}
}
$section_id = $arResult["SECTION"]["ID"];
/*print_r($product_arr);*/
				$cc = -1;
				$arFilter = array("SECTION_ID" => $section_id, "!ID" => $product_arr, "ACTIVE" => "Y");
				$arSelect = array("ID","IBLOCK_ID","NAME","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
						$arFields = $ob->GetFields();
							$cc++;
							$price = WAPrice::getByID($arFields["ID"],5);
							$arAnalog[$cc]["COUNT"] = $cc;
							$arAnalog[$cc]["ID"] = $arFields["ID"];
							$arAnalog[$cc]["NAME"] = $arFields["NAME"];
							$pic = CFile::GetFileArray($arFields["DETAIL_PICTURE"]);
							$arAnalog[$cc]["DETAIL_PICTURE"] = $pic["SRC"];

							$art = explode('-',$arFields["PROPERTY_CML2_ARTICLE_VALUE"]);
							$arAnalog[$cc]["ARTICLE"] = $art[1];
							$arAnalog[$cc]["DETAIL_PAGE_URL"] = $arFields["DETAIL_PAGE_URL"];
							$arAnalog[$cc]["PRICE"] = $price.' руб';
							if ($cc == 10) break;
						}	
?>
<?if ($cc != -1):?>
<div class="cartitle">
	Похожие товары
</div>
<div class="carblock">
   <div class="carousel shadow"> 
      <div class="carousel-button-left"><a href="#"></a></div> 
      <div class="carousel-button-right"><a href="#"></a></div> 
		<div class="carousel-wrapper"> 
		   <div class="carousel-items"> 
		   <?foreach($arAnalog as $item):?>
			  <div class="carousel-block">			
					<div class="catalog_item">
						<a class="a_pic" href="<? echo $item['DETAIL_PAGE_URL']; ?>" style="background-image: url(<?=$item["DETAIL_PICTURE"]?>)"></a>
							<div class="title"><a href="<? echo $item['DETAIL_PAGE_URL']; ?>"><? echo $item["NAME"] ?></a></div>
							<div class="articul">Артикул: <?=$item["ARTICLE"]?></div>	
							<div class="price"><?=$item["PRICE"]?></div>
					</div>
			  </div>
			<?endforeach;?>
		   </div>
		</div>
   </div>
</div>
<?endif?>
		<div class="bx_lb">
<div class="tac ovh">
</div>
<div class="tab-section-container">
<?
if ('Y' == $arParams['USE_COMMENTS'])
{
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.comments",
	"",
	array(
		"ELEMENT_ID" => $arResult['ID'],
		"ELEMENT_CODE" => "",
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"SHOW_DEACTIVATED" => $arParams['SHOW_DEACTIVATED'],
		"URL_TO_COMMENT" => "",
		"WIDTH" => "",
		"COMMENTS_COUNT" => "5",
		"BLOG_USE" => $arParams['BLOG_USE'],
		"FB_USE" => $arParams['FB_USE'],
		"FB_APP_ID" => $arParams['FB_APP_ID'],
		"VK_USE" => $arParams['VK_USE'],
		"VK_API_ID" => $arParams['VK_API_ID'],
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
		"BLOG_TITLE" => "",
		"BLOG_URL" => $arParams['BLOG_URL'],
		"PATH_TO_SMILE" => "",
		"EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
		"AJAX_POST" => "Y",
		"SHOW_SPAM" => "Y",
		"SHOW_RATING" => "N",
		"FB_TITLE" => "",
		"FB_USER_ADMIN_ID" => "",
		"FB_COLORSCHEME" => "light",
		"FB_ORDER_BY" => "reverse_time",
		"VK_TITLE" => "",
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?>
<?
}
?>
</div>
		</div>
			<div style="clear: both;"></div>
	</div>
	<div class="clb"></div>
</div><?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arOneJS)
	{
		if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
		{
			$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
			$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
		}
		$strProps = '';
		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($arOneJS['DISPLAY_PROPERTIES']))
			{
				foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
				{
					$strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
						is_array($arOneProp['VALUE'])
						? implode(' / ', $arOneProp['VALUE'])
						: $arOneProp['VALUE']
					).'</dd>';
				}
			}
		}
		$arOneJS['DISPLAY_PROPERTIES'] = $strProps;
	}
	if (isset($arOneJS))
		unset($arOneJS);
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'NAME' => $arResult['~NAME']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
}
else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
	{
?>
<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
		if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
		{
			foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
			{
?>
	<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
				if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
					unset($arResult['PRODUCT_PROPERTIES'][$propID]);
			}
		}
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if (!$emptyProductProperties)
		{
?>
	<table>
<?
			foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo)
			{
?>
	<tr><td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
	<td>
<?
				if(
					'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
					&& 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
				)
				{
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
					}
				}
				else
				{
					?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
					}
					?></select><?
				}
?>
	</td></tr>
<?
			}
?>
	</table>
<?
		}
?>
</div>
<?
	}
	if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
	{
		$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
}
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<script>
	$(".offer_show").on("click", function () {
		$(this).parents("tr").next().slideToggle(300);
	});
	/*$(".detail_onbuy").on("click", function () {
		$(".basket_success").show();
	})*/
</script>