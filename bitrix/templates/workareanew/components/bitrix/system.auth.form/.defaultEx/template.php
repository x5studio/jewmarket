<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	{?>
	<div class="authError">
	<?//ShowMessage($arResult['ERROR_MESSAGE']);?>
	</div>
	<script>
		$(".bxreg_shadow").show();
	</script>
	<?
	}?>
	<?if($arResult["FORM_TYPE"] == "login"):?>
	<a class="profile" id="throw_reg" title="Войдите или зарегистрируйтесь">Вход / Регистрация</a>
	<?endif?>
<div class="bx-system-auth-form">

<?if($arResult["FORM_TYPE"] == "login"):?>
<?
   CAjax::Init(); 
   $bxajaxid = CAjax::GetComponentID('bitrix:system.auth.form', ''); 
   if (strlen($_POST['AJAX_CALL']) && $_POST['AJAX_CALL']=='Y' && $_POST['auth_secret']=='Y' && $_POST['TYPE']=='AUTH') {
	$APPLICATION->RestartBuffer();
   }
   $dir = $APPLICATION->GetCurDir();
?>

<div class="bxreg_shadow">
<div class="bxreg_window">
<div class="cross2"></div>
<?//$arResult["AUTH_URL"] = "/personal/order/make/";?>
<form class="WorkArea_AuthBlock" id="WAAUTH" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<div class="successAuth">Вы успешно авторизованы</div>
<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']):?>
<div id="throw_exception" class="waErr" style="white-space: nowrap"><?ShowMessage($arResult['ERROR_MESSAGE']);?></div>
<?else:?>
<div id="throw_exception" class="throw_hidd"></div>
<?endif;?>
<?/*$APPLICATION->IncludeComponent(
	"bitrix:system.auth.registration",
	"flat",
	Array(
		"AUTH" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"REQUIRED_FIELDS" => array(),
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL"),
		"SUCCESS_PAGE" => "",
		"USER_PROPERTY" => array(),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y"
	)
);*/?>
<?if($arResult["BACKURL"] <> ''):?>
 	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input id="wa_input_auth" type="hidden" name="AUTH_FORM" value="Y" />
	<input id="wa_input_type" type="hidden" name="TYPE" value="AUTH" />
	<input id="wa_input_forgot" type="hidden" name="userForgot" value="N" />
	<input id="wa_start_forgot" type="hidden" name="userStartForgot" value="N" />
<!--<input type="hidden" name="backurl" value="/about/index.php">
<input type="hidden" name="AUTH_FORM" value="Y">	
<input type="hidden" name="TYPE" value="REGISTRATION"> -->
	<table width="95%" id="WAAUTH_DELIGATE">
		<tr>
			<td colspan="2" style="position: relative">
			<!-- <span class=""><?=GetMessage("AUTH_LOGIN")?></span><br /> -->
			<span class="">Войти или зарегистрироваться</span><br />
			<i id="wa_user_isset" class="fa fa-check-circle" aria-hidden="true"></i>
			<input tabindex="1" data-validate="email" class="wa_main_field" id="wa_user_login" type="text" name="USER_LOGIN" maxlength="50" value="<?//=$arResult["USER_LOGIN"]?>" size="17" placeholder="E-mail:"/></td>
		</tr>
		<tr>
			<td colspan="2" style="position: relative">
				<input data-validate="len-32 sym-all" autocomplete="off" tabindex="4" id="forgot_pass" class="wa_main_field wa_hidd" type="text" name="checkword" maxlength="255" placeholder="Введите код, присланный на E-mail" autocomplete="off" />
			</td>			
		</tr>
		<tr>	
			<td colspan="2" style="position: relative">
			<input data-validate="len-6 sym-all compare" tabindex="2" class="wa_main_field" id="wa_user_pass1" type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" placeholder="<?=GetMessage("AUTH_PASSWORD")?>"/>
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_PASSWORD")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
</script>
<?endif?>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="position: relative">
				<input data-validate="len-6 sym-all compare" tabindex="3" id="wa_user_pass2" class="wa_main_field wa_hidd" type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" placeholder="Повторный ввод пароля" autocomplete="off" />
			</td>
		</tr>

<!--		<tr>
			<td colspan="2" style="position: relative">
				<input tabindex="4" id="wa_forgot_pass" class="wa_main_field wa_hidd" type="text" name="USER_FORGOT_PASSWORD" maxlength="255" placeholder="Введите E-mail для " autocomplete="off" />
			</td>
		</tr> -->
<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
<!--		<tr>
			<td valign="top"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label></td>
		</tr> -->
<?endif?>

<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
		<tr class="wa_aureg">
				<td><a tabindex="3" id="wa_auth_btn" class="wa_auth_btn active">Войти</a></td>
				<td><a tabindex="4" id="wa_reg_btn" class="wa_auth_btn">Зарегистрироваться</a></td>
				<a class="wa_order_btn" id="wa_order_btn">Заказать без регистрации</a>
				<td><a tabindex="3" id="wa_pass_ressurection" class="wa_auth_btn active">Восстановить пароль</a></td>
<!--			<td><a id="wa_auth_btn" class="wa_auth_btn">Войти</a><input id="wa_submit" type="submit" class="wa_authhidd" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>"/></td>
			<td><a id="wa_reg_btn" class="wa_auth_btn disabar">Зарегистрироваться</a></td> --> 
		</tr>
<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
<!--		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a></noindex><br /></td>
		</tr>
<?endif?>
-->
<!--		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex></td>
		</tr>
-->
	</table>
	<div class="WorkArea_social">
	<?if($arResult["AUTH_SERVICES"]):?>
			<tr>
				<td colspan="2">
					<div class="bx-auth-lbl">Войти через социальную сеть</div>
	<?
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat", 
		array(
			"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
			"SUFFIX"=>"form",
		), 
		$component, 
		array("HIDE_ICONS"=>"Y")
	);
	?>
				</td>
			</tr>
	<?endif?>
	</div>
</form>
<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
<!--<a class="wa_forgot" href="http://jewmarket.ru/personal/profile/index.php?forgot_password=yes">Забыли свой пароль</a>-->
<a class="wa_forgot" id="wa_forgot">Забыли свой пароль</a>
<?endif?>
<?
elseif($arResult["FORM_TYPE"] == "otp"):
?>
<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />
	<table width="95%">
		<tr>
			<td colspan="2">
			<?echo GetMessage("auth_form_comp_otp")?><br />
			<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
		</tr>
<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
		<tr>
			<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
		</tr>
<?endif?>
		<tr>
			<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
		</tr>
		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
		</tr>
	</table>
</form>

</div>
</div>

<?else:?>

<form action="<?=$arResult["AUTH_URL"]?>" class="ifreg">
	<p>Вы вошли как:&nbsp;<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><i class="fa fa-user fa-2"></i> <span class="header_auth_login" title="admin">
	<?
	if (!empty($arResult["USER_NAME"]))
		{	
		echo $arResult["USER_NAME"];
		}
		else
		{
			echo $arResult["USER_LOGIN"];
		}
	?>
	</span></a></p>
	<p>
	<p>
	<a href="<?=$arResult["PROFILE_URL"]?>" class="header_auth_partner" title="<?=GetMessage("AUTH_PROFILE")?>">Личный кабинет</a>
		<?foreach ($arResult["GET"] as $key => $value):?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<span class="exit"><i class="header_red fa fa-power-off"></i><input type="submit" class="header_auth_partner" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" /></span>
	</p>
<!--	<a href="<?=$arResult["PROFILE_URL"]?>" class="header_auth_partner" title="<?=GetMessage("AUTH_PROFILE")?>">Личный кабинет</a> <a href="/index.php?logout=yes" title="Выход"><i class="header_red fa fa-power-off"></i>Выход</a>
	</p>
	<table width="95%">
		<tr>
			<td align="center">
				<?=$arResult["USER_NAME"]?><br />
				[<?=$arResult["USER_LOGIN"]?>]<br />
				<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
			</td>
		</tr>
		<tr>
			<td align="center">
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />
			</td>
		</tr>
	</table> --> 
</form>
<?endif?>
</div>
</div>
</div>
<script>
var waAuthForm = new authForm("#WAAUTH", "header");

/*function wa_wright_register (wamode, waFightForTheGlory)
{
	if (waFightForTheGlory)
	{
		$('#wa_input_type').attr('value','REGISTRATION');
		$("#wa_submit").attr('type','submit');
		$("#WAAUTH").submit();
		
	}
	else
	{
		$('#wa_input_type').attr('value','AUTH');
		$("#wa_submit").attr('type','submit');
		$("#WAAUTH").submit();
	}
}
function wa_error_pass (wapass,waevpass) 
{
	var str = waevpass.val();	
	if (str.length < 6)
	{
		$('#wa_user_pass1').addClass('wa_field_error');
		$('#throw_exception').text('Пароль должен быть не менее 6 символов');
		$('#throw_exception').removeClass('throw_hidd');
		
	}
	else
	{
		console.log(wapass);
		if (wapass == 'wa_auth_btn')
		{	
			var waAuthTogetherMU = false;
			wa_wright_register(wapass, waAuthTogetherMU);
		}
		else
		{
			var doublepass = $('#wa_user_pass2');	
			if (str != doublepass.val())
			{
				$('#wa_user_pass1').val('');
				$('#wa_user_pass2').val('');
				$('#throw_exception').text('Пароли не совпадают. Попробуйте еще раз');
				$('#throw_exception').removeClass('throw_hidd');
			}
			else
			{
				var waAuthTogetherMU = true;
				wa_wright_register(wapass, waAuthTogetherMU);
			}
		}
	}
}
function wa_error_login (walogin)
{
	var waloginval = walogin.val();

	var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
	if ( pat = pattern.test(waloginval))
	{
		return true;
	}
	else
	{
		return false;
	}
}
function wa_error_separate (waerr)
{
	var waaghid = waerr;
	var errlogin = $('#wa_user_login');
	var errpass1 = $('#wa_user_pass1');
	if (errlogin.val() != '')
	{
		if (!wa_error_login(errlogin))
		{
			 errlogin.addClass('wa_field_error');
			 $('#throw_exception').text('Неправильный логин.  Пример customer@awesomemail.com');
			 $('#throw_exception').removeClass('throw_hidd');
		}
		else
		{
			wafirsttest = true;
			if (wa_error_pass(waaghid, errpass1))
			{
				
			}
			else
			{
				return false
			}
		}
	}
	else
	{
		 errlogin.addClass('wa_field_error');
		 $('#throw_exception').text('Введите логин.  Пример customer@awesomemail.com');
		 $('#throw_exception').removeClass('throw_hidd');
	}
}
function waMetalUnion (waWillBeTrue) {
	var waSwordAndSorcery = waWillBeTrue;
	var waid = waSwordAndSorcery.attr('id');
	var wahid = $(".wa_hidd");
	var wacl = waWillBeTrue;
	if ( wacl.hasClass('disabar'))
		{
			$(".wa_auth_btn").addClass('disabar');
			wacl.removeClass('disabar');
			if (waid == 'wa_reg_btn') {wahid.show()} else (wahid.hide());
		}
		else
		{
			
			if( fwd = wa_error_separate(waid))
			{
				console.log(fwd);
			}
		}	
}
$(".wa_auth_btn").on("click", function() {
	if (!$("#throw_exception").hasClass('throw_hidd')) {$("#throw_exception").addClass('throw_hidd')};
	var waSwordAndSorcery = $(this);
	var waid = waSwordAndSorcery.attr('id');
	var wahid = $(".wa_hidd");
	var wacl = waSwordAndSorcery;
	if ( wacl.hasClass('disabar'))
		{
			$(".wa_auth_btn").addClass('disabar');
			wacl.removeClass('disabar');
			if (waid == 'wa_reg_btn') {wahid.show()} else (wahid.hide());
		}
		else
		{
			
			if( fwd = wa_error_separate(waid))
			{
				console.log(fwd);
			}
		}	
});

$('.wa_main_field').on('click', function () {
	if ($(this).hasClass('wa_field_error'))
	{
	$(this).removeClass('wa_field_error');
	$('#throw_exception').addClass('throw_hidd');		
	}	
});
$('.bx-system-auth-form .cross2').on('click', function() {
	$('.bxreg_shadow').fadeOut(300);
})
document.onkeyup = function (e) {
	    e = e || window.event;
	    if (e.keyCode === 13) {
			if ($('#wa_reg_btn').hasClass('disabar'))
			{
				waMetalUnion($('#wa_auth_btn'));
			}
			else
			{
				waMetalUnion($('#wa_reg_btn'));
			}
	       return false;
	    }
	    
	}*/

</script>