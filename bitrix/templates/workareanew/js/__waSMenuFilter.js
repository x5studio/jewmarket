var lastWait = [];
function _adjustWait()
{
	if (!this.bxmsg) return;

	var arContainerPos = BX.pos(this),
		div_top = arContainerPos.top;

	if (div_top < BX.GetDocElement().scrollTop)
//		div_top = BX.GetDocElement().scrollTop + 5;

//	this.bxmsg.style.top = (div_top + 5) + 'px';

	if (this == BX.GetDocElement())
	{
//		this.bxmsg.style.right = '5px';
	}
	else
	{
//		this.bxmsg.style.left = (arContainerPos.right - this.bxmsg.offsetWidth - 5) + 'px';
	}
}
BX.showWait = function(node, msg)
{
//	node = BX(node) || document.body || document.documentElement;
	node = BX('waJewmarket');
	msg = msg || BX.message('JS_CORE_LOADING');

	var container_id = node.id || Math.random();
	var waSize = BX.GetWindowSize();
	if ( waSize.innerWidth < 400 )
	{
		var back = "rgba(0,0,0,.6)";
	}
	else
	{
		var back = "rgba(0,0,0,.1)";		
	}
	var obMsg = node.bxmsg = document.body.appendChild(BX.create('DIV', {
		props: {
			id: 'wait_' + container_id
		},
		style: {
			position: 'fixed',
			width: '100%',
			height: '100%',
			top: '-10px', 
			left: '0', 
			bottom: '0', 
			right: '0',
			background: back,
			zIndex: '10000',	
			padding: '0px',
			border: '0px',
			fontSize: '0px',
		},
		text: msg
	}));

	setTimeout(BX.delegate(_adjustWait, node), 10);

	lastWait[lastWait.length] = obMsg;
	return obMsg;
};

BX.closeWait = function(node, obMsg)
{

	if(node && !obMsg)
		obMsg = node.bxmsg;
	if(node && !obMsg && BX.hasClass(node, 'bx-core-waitwindow'))
		obMsg = node;
	if(node && !obMsg)
		obMsg = BX('wait_' + node.id);
	if(!obMsg)
		obMsg = lastWait.pop();
	node = BX('waJewmarket');
	if (obMsg && obMsg.parentNode)
	{
		for (var i=0,len=lastWait.length;i<len;i++)
		{
			if (obMsg == lastWait[i])
			{
				lastWait = BX.util.deleteFromArray(lastWait, i);
				break;
			}
		}

		obMsg.parentNode.removeChild(obMsg);
		if (node) node.bxmsg = null;
		BX.cleanNode(obMsg, true);
	}
};
function waJSMobileMenu(ajaxURL, viewMode, params, sec)
{
	JCSmartFilter.apply(this, arguments);
/*	this.arr = [].slice.call(arguments);//console.log(arguments.slice);
	console.log(this.arr);*/
//	console.log(arguments);
//	this.menuFilter = BX("waSMFBlock");
	this.menuFilter = "waSMFBlock";
	this.bc = BX.util.htmlspecialcharsback(sec.BC);
//	this.sec = sec;
//	BX.util.htmlspecialcharsback(sec);
	this.onShowClassName = "wa_onShow";
	this.onSelectClassName = "checked";
	this.timer = null;
//	this.waAjaxMenu = { folder : "/offers/jewelry/"};
	this.waAjaxMenu = [];
	this.level = null;
	this.waAjaxHref = null;
	this.waClass = null;
	this.elem = null;
	this.rootHref = '/offers/jewelry/';
	this.isCheck = false;
	this.menuParent = "waSMSubFilterMenu";
	this.chbParent = "bx-filter-parameters-box-container";
	this.chb = null;
	this.waList = {};
	this.subItems = [];
	this.waSMInput = null;
	this.onCh = false;
	this.fSecID = [];
	
	this.current = null;
	this.currentParent = null;
	this.currentType = null;
	this.curHref = null;
//	var self = this;
//	console.dir(this);
	var self = this;
//	console.dir("this");
//	console.dir(this);
//	if ()
	var onInit = document.getElementById(this.menuFilter);
	if (!onInit)
	{
		this.init(BX.util.htmlspecialcharsback(sec.MENU));
	}
		BX.bindDelegate(
		BX('topNativeMenu'),
		'click',
		{
			className: 'getBackMobile'
		},
		function() {
//			event.preventDefault();		
//			console.dir(this);
			self.clickMenu(this);		
//			return false;
		}
	);
		BX.bindDelegate(
		BX('waSMCatalog'),
		'click',
		{
			tagName: 'a'
		},
		function() {
//			event.preventDefault();		
//			console.dir(this);
			self.clickMenu(this);		
//			return false;
		}
	);
		BX.bindDelegate(
		BX('waSMFBlock'),
		'click',
		{
			tagName: 'li'
		},
		function() {
//			event.preventDefault();		
//			console.dir(this);
			self.clickFilter(this);		
//			return false;
		}
	);
}
waJSMobileMenu.prototype = Object.create(JCSmartFilter.prototype);
waJSMobileMenu.prototype.constructor = waJSMobileMenu;

waJSMobileMenu.prototype.init = function (outer) {
//	var self = this;
//	console.dir(outer);
	var filter = document.createElement('ul');
	filter.setAttribute("id", this.menuFilter);	
	filter.classList.add("waSMSubFilterMenu", "not-ready", "waSMSubCatShow");
	filter.innerHTML = outer;
	var onOpenA = BX("waSMCatalog").querySelectorAll(".waSMMainCatOpen.wcheck");
	if (onOpenA.length !=0)
	{	
		var onOpen = BX(onOpenA[0]).parentElement;
//		console.dir(onOpen);
		if (onOpen) 
			{
				onOpen.appendChild(filter);
				BX.removeClass(filter, "not-ready");
			}
	}
}



waJSMobileMenu.prototype.breadCrumb	= function () {
				var bc = BX("waBC");
				var menuBcTitle = BX("getBack");
				var list = bc.querySelectorAll('.bx-breadcrumb-item');
				var len = list.length - 1;
				var curSec = BX(list[len]);
				var curHref = curSec.previousElementSibling.lastElementChild.getAttribute("href");
				if (curHref == "/offers/") curHref = "/offers/jewelry/";
				menuBcTitle.innerText = curSec.lastElementChild.innerText;
				menuBcTitle.setAttribute('href', curHref);
				this.curHref = menuBcTitle;
}
waJSMobileMenu.prototype.breadCrumbClick = function ()
{
	event.preventDefault();
//	console.log(this);
	var attr = BX("getBack").getAttribute("href");
	var obj = BX("waSMCatalog");
	var ajaxCall = obj.querySelector('[href="' + this.bc +'"]');
//	var ajaxCall = BX(ajaxCall1);
//	console.dir(ajaxCall);
	var event = new Event("click");
	ajaxCall.dispatchEvent(event);
//	ajaxCall.onclick();
//	BX.unbind(BX("getBack"), 'click', BX.proxy(this.breadCrumbClick,this));
/*	BX.addCustomEvent('onAjaxSuccessFinish', BX.proxy(this.processor,this));
	var attr = this.curHref.getAttribute("href");
	var ajaxCall = BX("staticSections").querySelector('[href="' + attr +'"]');
	ajaxCall.onclick();	*/
//	BX.addCustomEvent('onAjaxSuccessFinish', BX.proxy(this.processor,this));	
}

waJSMobileMenu.prototype.clickFilter = function (oncl)
{
	this.isCheck = false;
/*	console.dir("oncl");
console.dir(oncl);*/
	this.menuItem = oncl;
	
	this.waSMParent = BX("waSMFBlock"); // waSMSubFilterMenu
	if (BX.hasClass(this.menuItem, "felem"))
	{
		this.isCheck = true;
		var par = BX.findParent(this.menuItem, { className : "waSMFName"});
		BX.removeClass(par,"wa_onShow");
		var name = par.querySelector(".finput");
		BX.addClass(name,"inLoad");
		name.innerHTML = "";
		var filter = BX("waFilterForm");
		var attr = this.menuItem.getAttribute("data-lid");
//		var elem = filter.querySelector('#' + attr);		
		var elem = BX(attr);
		if (!BX.hasClass(elem,"selected"))
		{
			BX.adjust(elem, { props: { checked: "checked"}});		
		}	
		else
		{
			BX.adjust(elem, { props: { checked: ""}});
		}
//		setTimeout(function() {elem.onclick()}, 2000);
		BX.delegate(this.click(elem),this);
//		this.click(elem);
//		BX.showWait("wait_waJewmarket");
	}
	else
	{
//		console.dir(oncl);
		BX.delegate(this.filterVisual(oncl),this);
	}
/*	BX.unbindAll(BX('waSMFBlock'));

	this.isCheck = false;


	this.waSMItems = BX.findChildrenByClassName(this.waSMParent, this.onShowClassName, true);	

	var elem = this.waTarget(event) || menuItem;
	if (elem != this.menuItem)
	{

		this.subParent = BX.findParent(elem, {className : "subMenu"});
		this.waSMInput = this.subParent.previousElementSibling;
		this.subItems = BX.findChildrenByClassName(this.subParent, this.onSelectClassName, true);
	}
	this.update(elem);*/
}

// в разработке
/*waJSMobileMenu.prototype.clear = function (menuItem)
{
	var isSelected = (BX.hasClass(this.current,selected)) ? true : false;
}*/

waJSMobileMenu.prototype.filterVisual = function (oncl)
{
	var menuItem = oncl;
	if (!this.isCheck)
	{
//		var menuItem = oncl;
		if (BX.hasClass(menuItem, "wa_onShow"))
		{
			BX.toggleClass(menuItem, "wa_onShow")
		}
		else
		{
			var list = this.waSMParent.querySelectorAll("li.wa_onShow");
			for(var i = 0; i < list.length; i++)
			{
				var elem = BX(list[i]);
				BX.removeClass(elem, "wa_onShow");
			}
			BX.addClass(menuItem, "wa_onShow");
		}
	}
	else
	{
		var par = BX.findParent(menuItem, { className : "waSMFName"});
		var name = par.querySelector(".finput");
		BX.removeClass(name,"inLoad");
//		setTimeout(function(){BX.removeClass(name,"inLoad")}, 500);
		if (BX.hasClass(menuItem, "checked"))
		{
			BX.removeClass(menuItem, "checked");
			BX.removeClass(name, "inSelect");
			name.innerHTML = "не выбрано";
//			var name = "не выбрано";
		}
		else
		{
			var list = par.querySelectorAll("li.checked");
			for(var i = 0; i < list.length; i++)
			{
				var elem = BX(list[i]);
				BX.removeClass(elem, "checked");
			}			
			BX.addClass(menuItem, "checked");	
			BX.addClass(name, "inSelect");		
			name.innerHTML = menuItem.innerHTML;	
		}
//		BX.removeClass(par, "wa_onShow");
	}
//	BX.unbindAll(BX('waSMFBlock'));
}
waJSMobileMenu.prototype.postHandler = function (result, fromCache)
{
	console.dir(result);
	JCSmartFilter.prototype.postHandler.apply(this, arguments);
//	console.dir(result);
//	BX.unbindAll(BX('waSMFBlock'));
//	BX.delegate(this.filterVisual(this.menuItem),this);
	setTimeout(BX.delegate(this.filterVisual(this.menuItem),this), 2000);
}
waJSMobileMenu.prototype.click = function (checkbox)
{	
	JCSmartFilter.prototype.click.apply(this, arguments);
//		BX("wait_waJewmarket").style.display = "none!important;";
}
waJSMobileMenu.prototype.clickMenu	= function (oncl)
{
//	console.dir(oncl);
	event.preventDefault();
	if (BX.hasClass(oncl, "getBackMobile"))
	{
		this.current = BX("waSMCatalog").querySelector('[href="' + this.bc +'"]');
		BX.unbindAll(BX('topNativeMenu'));
	}
	else
	{
		this.current = oncl;
		BX.unbindAll(BX('waSMCatalog'));		
	}




	BX.addCustomEvent('onAjaxSuccessFinish', BX.proxy(this.processor,this));
	var attr = this.current.getAttribute("href");	
	if (BX.hasClass(this.current, "waSMMainCatOpen"))
	{
		this.currentParent = this.current;		
		if (BX.hasClass(this.current, "wcheck"))
		{
			attr = this.rootHref;			
			this.currentType = true;
		}
	}
	else
	{
		var par = BX.findParent(this.current, {className : "waSMSubCatMenu", tag : "ul"});
		this.currentParent = BX.findPreviousSibling(par, {tag : "a"});
		if (BX.hasClass(this.current,"wcheck"))
		{
			attr = this.currentParent.getAttribute("href");
		}
	}
	var ajaxCall = BX("staticSections").querySelector('[href="' + attr +'"]');
	ajaxCall.onclick();

}
waJSMobileMenu.prototype.processor = function ()
{

//	console.dir(this);
	BX.delegate(this.menuVisual(),this);
	this.breadCrumb();
	BX.removeCustomEvent('onAjaxSuccessFinish', BX.proxy(this.processor, this));	
}
waJSMobileMenu.prototype.menuVisual = function () {
	var cur = this.current;
	var par = this.currentParent;
	var menu = BX("waSMCatalog");
	var selected = "wcheck";
	var isSub = (this.current == this.currentParent) ? false : true; // проверка на подсекции
	var isSelected = (BX.hasClass(this.current,selected)) ? true : false;
	if (isSelected)
	{	
		if (isSub)
		{
			BX.removeClass(cur, selected);
			BX.toggleClass(cur.children[0],["fa-square-o", "fa-check-square-o"]);
		}
		else
		{
			BX.removeClass(cur, selected);
			BX.toggleClass(cur.children[0],["fa-chevron-right", "fa-chevron-down"]);	
			var list = cur.querySelectorAll("a."+selected);
			for(var i = 0; i < list.length; i++)
			{
				var elem = BX(list[i]);
				BX.removeClass(elem, selected);
				BX.toggleClass(elem,["fa-square-o", "fa-check-square-o"]);				
			}				
		}
	}
	else
	{
		if (!isSub)
		{
			var list = menu.querySelectorAll("a."+selected);		
		}
		else
		{
			var list = menu.querySelectorAll(".wa-active."+selected);
		}	
		for(var i = 0; i < list.length; i++)
		{
			var elem = BX(list[i]);
			BX.removeClass(elem, selected);
			var chevron = elem.firstElementChild;
			if (BX.hasClass(elem, "wa-active"))
			{				
				BX.toggleClass(chevron,["fa-square-o", "fa-check-square-o"]);
			}
			else
			{
				BX.toggleClass(chevron,["fa-chevron-right", "fa-chevron-down"]);		
			}
		}	
			BX.addClass(cur,selected);
			if (isSub) 
			{
				BX.toggleClass(cur.firstElementChild,["fa-square-o", "fa-check-square-o"]);			
			}
			else
			{
				BX.toggleClass(cur.firstElementChild,["fa-chevron-right", "fa-chevron-down"]);				
			}		
	}
}

/*waJSMobileMenu.prototype.gatherInputsValues = function (values, elements)
{
	JCSmartFilter.prototype.gatherInputsValues.apply(this, arguments);	
	console.dir(values);
	console.dir(elements);	
}*/
waJSMobileMenu.prototype.filterUpdate = function (href) {
/*	var nod = href;
	var waMenu = BX("waSMFBlock");
	var newMenu = nod.insertBefore(waMenu, nod.lastChild);
	BX.removeClass(newMenu, "not-ready");
	var waMenuItem = BX.findChild(newMenu, {className : "waSMFName"}, false, true);
	for( var i = 0; i < waMenuItem.length; i++)
	{
		var li = waMenuItem[i];
		BX.removeClass(waMenuItem[i], "disabled");
		var attr = waMenuItem[i].getAttribute("data-filter");
		var at = String('fBlock_' + attr);
			var lb = BX.findChild(BX(at), {tag : "label"}, true, true);
			var liul = BX.lastChild(waMenuItem[i]);
			if (lb)
			{
				for( var j = 0; j < lb.length; j++)
				{
					var id = lb[j].getAttribute("for");
					//var newLi = BX.create("li", {props : {className : "felem"}});
//					var newLi = document.createElement('li');
//					newLi.className = "felem";
//					newLi.innerHTML = lb[j].getAttribute("id");
//					newLi.setAttribute("data-lid", id);
//					var n = liul.appendChild(newLi);
	//				console.dir(label[j]);
				}
			}					
	}*/
}
/*waJSMobileMenu.prototype.updateItem = function (PID, arItem)
{
	JCSmartFilter.prototype.updateItem.apply(this, arguments);	
	console.dir('asdfasdfasdf');
}*/

waJSMobileMenu.prototype.update = function (elem)
{
	if (this.isCheck)
	{
		var chb = BX(elem.getAttribute('data-lid'));
		var obj = elem.parentNode;
		var sub = obj.querySelectorAll(".checked");
		var name = elem.innerHTML;
		if (sub.length != 0 && sub[0] != elem)
		{		
			for(var i = 0; i < sub.length; i++)
			{
				var el = sub[i];
				BX.removeClass(el, this.onSelectClassName);
				BX.adjust(BX(el.getAttribute('data-lid')), { props: { checked: ""}});				
			}				
		}
	
		var cl = BX.toggleClass(elem, this.onSelectClassName);
		var inp = this.waSMInput;
		inp.innerHTML = name;
		if (BX.hasClass(cl, this.onSelectClassName))
		{
			BX.adjust(chb, { props: { checked: "checked"}});
			this.onCh = true;
		}
		else
		{
			BX.adjust(chb, { props: { checked: ""}});
			this.onCh = false;
		}
			BX.removeClass(inp,"inSelect");
			BX.addClass(inp,"inLoad");
			inp.innerHTML = "";
			this.click(chb);

	}
	var mm = BX.toggleClass(this.menuItem, this.onShowClassName);
	if (BX.hasClass(mm, this.onShowClassName))
	{
		for (var k in this.waSMItems)
		{
			BX.removeClass(BX(this.waSMItems[k]),this.onShowClassName);
		}

	}//*[@id="comp_5d6245fb0dd35db694c2391d73785b7e"]/div/div[1]/div/div/form/div[6]/div/div[1]
}	
waJSMobileMenu.prototype.waTarget = function (e)
{
	var attr = e.target.getAttribute('data-lid');
	if (attr)
	{
		e.stopPropagation();
		this.isCheck = true;
		this.waClass = "bx-filter-parameters-box-container";
		this.chb = BX(attr);
		return e.target;
	}	
	else
	{
//		this.waSubItems = [];
		this.chb = null;
		return;
	}
}
//    ajaxHref.onclick();
//	BX.addCustomEvent('onAjaxSuccess', BX.proxy(this.breadCrumb(), this));
//	BX.addCustomEvent('onAjaxSuccess', BX.delegate(function(data){ console.dir(this) }, this) );
//	this.breadCrumb();
/*waJSMobileMenu.prototype.refresh = function ()
{
	
}*/
/*waJSMobileMenu.prototype.updateItem = function (PID, arItem)
{
	JCSmartFilter.prototype.updateItem.apply(this, arguments);
	var mainItems = this.waSMItems;
		for(var i = 0; i < mainItems.length; i++)
		{
			var subItems = mainItems[i].querySelectorAll(".disabled");
//			if ()
			console.dir(subItems);
		}
}*/
waJSMobileMenu.prototype.classReload = function ()
{
if (this.isCheck)
	{	
		var inp = this.waSMInput;	
		var par = this.subParent;
		var menu = this.waSMParent.children;
//		console.dir(menu);
		for( var i = 0; i < menu.length; i++)
			{
				var menuItems = menu[i].querySelectorAll(".felem").length;
				var disabled = menu[i].querySelectorAll(".felem.disabled").length;
				if (menuItems == disabled)
				{
					BX.addClass(menu[i],"disabled");
				}
				else
				{
					BX.removeClass(menu[i],"disabled");
				}
/*				for (var m in menuItems)
				{
					console.dir(menuItems.length);
				}*/

			}	
		var tpar = BX.findChildrenByClassName(par, "checked", true);
		var t = (tpar.length != 0) ? tpar[0].innerHTML : "не выбрано";
//		console.dir(t);	
		inp.innerHTML = t;
		BX.removeClass(inp, "inLoad");
		var tx = (this.onCh) ? BX.addClass(inp, "inSelect") : false ;
			
/*		var tx = (BX.hasClass(inp, "inSelect") && ) ?   : ;
		{
			
		}
		else
		{
			
		}*/
//		inp.style["display"] = "block";
//		BX.addClass(inp, "inSelect");
//		console.dir(this.waSMInput);
//		this.waChecked.style["display"] = "block";

/*		var tg = BX.toggleClass(this.waSMInput,"inSelect");
		var sp = this.subParent;
		var t = null;
		if (BX.hasClass(tg, "inSelect"))
		{
			var isch = sp.querySelector(".checked");
			t = isch.innerHTML;
		}
		else
		{
			t = "не выбрано";
		}
		this.waSMInput.innerHTML = t;*/
/*		if (BX.hasClass(this.waSMInput, this.onSelectClassName))
		BX.addClass(this.waSMInput, "inSelect");*/
	}
//	console.dir(arguments);
//	this.waSMInput.innerHTML = "КЛАСС";
//	console.log(this);	
}
/*waJSMobileMenu.prototype.click = function (checkbox)
{
	
//	JCSmartFilter.prototype.click.apply(this, arguments);
//	console.log(this);
	if(!!this.timer)
	{
		clearTimeout(this.timer);
	}

	this.timer = setTimeout(BX.delegate(function(){
		this.reload(checkbox);
	}, this), 500);
}*/


BX.ready( function() {
//	var waReady = "fff";
//	waJSMM.init();
});
/*		BX.addCustomEvent('onAjaxSuccess', function(){
				
				var bc = document.querySelector('.bx-breadcrumb');
				var menuBcTitle = BX("topNativeMenu").querySelector('.getBackMobile');
				var bcchild = bc.children;
				var rootHref = '/offers/jewelry/';
			//	bc = bx.childNodes;
				for( var i = 0; i < bcchild.length; i++)
				{
					if (!bcchild[i].getAttribute('id') && bcchild[i].classList.length != 0)
					{
						menuBcTitle.innerText = '<' + bcchild[i].innerText;
						var prev = bcchild[i].previousElementSibling;
						rootHref = prev.querySelector('a').getAttribute("href");
						if (rootHref == "/offers/") rootHref = '/offers/jewelry/';
						
						menuBcTitle.setAttribute('href', rootHref);
//					console.dir(e);
					}
			//		console.dir(bcchild[i]);
				}
				window.waJSMobileMenu.rl();
//				this.classReload();		
		//	console.dir(bcchild);	
			});*/

$(document).ready( function () {
//	BX.showWait();
/*var wait = BX.showWait('available_action_wrapper');
BX.closeWait('available_action_wrapper', wait);//вторым параметром надо передать результат вызова show*/
/*BX.showWait = function('test_ajax_elem', msg) {
   $("#waAjaxShadow").show();
};*/
//	console.log(BX.GetWindowSize());
	var size = BX.GetWindowSize();
//	console.log(size.innerWidth);
//	var waMenuFilter = new waFiltrer()
//	console.log($(this));
	var fids = [];
//	var menuFTitles = $("body .waSMSubFilterMenu.waSMSubCatShow li");
	if ( size.innerWidth < 10 )
	{

	}
/*	$("body").on("click",".waSMFName", function (e) {
//		var t = e.target;
			$("body .waSMFName.green").removeClass("green");
			$("body menu.catalog li.waOpened").removeClass("waOpened");
			$("body menu.catalog ul.waOpened").removeClass("waOpened");
			$("body #waInnerShadow").show();
			$(this).addClass("green");
			$(this).addClass("waOpened");
			$(this).find("ul").addClass("waOpened");
			$(this).parent().addClass("waOpened");
	});	
	$("body").on("click","#waInnerShadow", function () {
		$("body .waSMFName.green").removeClass("green");
		$("body menu.catalog li.waOpened").removeClass("waOpened");
		$("body menu.catalog ul.waOpened").removeClass("waOpened");	
		$(this).hide();
	});
	$("body").on("click",".waOpened li.felem", function () {
//		var sMenu = $(this).parent().find('li.waChecked');
		var sChecked = $(this).parent().find('li.waChecked');
		var id = $(this).attr("data-lid");

		if (sChecked)
		{
			if (sChecked.attr("data-lid") != id)
			{
				sChecked.removeClass("waChecked");
				$(this).addClass("waChecked");
				$("body #" + sChecked.attr("data-lid")).click();
				$("body #" + id).click();
//				console.log($(this));
			}
			else
			{
				$(this).removeClass("waChecked");				
				$("body #" + id).click();
				$(this).parents(".waSMSubFilterMenu").removeClass("waFactive");
//				console.log($(this));				
			}
		}
		else
		{
			$("body #" + id).click();
			$(this).addClass("waChecked");
			$(this).parents(".waSMFName").addClass("waFactive");
//			console.log($(this));
		}
//		$(this).parents(".waSMFName.green").removeClass("green");
//		$(this).parents("li.waOpened").removeClass("waOpened");
//		$(this).parent().removeClass("waOpened");
//		$("#waInnerShadow").hide();
//		console.log($(this).parents(".waSMFName.green"));
		
	});*/
//{
/*	$.each( menuFTitles,  function (index) {
	var li = $(this);		
		var chb1 = $('body .bx-filter-parameters-box .checkbox[data-flabel^="'+ $( this ).attr("data-filter") +'"]');
		$.each( chb1,  function (intg) {
			if (!$(this).children().hasClass("disabled"))	
			{
//				console.log($(this).children());
				var drtext = $(this).find(".bx-filter-param-text").text();
				var drid = $(this).find("input").attr("id");
				li.find("ul").append('<li data-lid="' + drid + '">'+ drtext +'</li>');
				li.find("a span").text(intg+1);
				//console.log($(this));	
			}
		});

//		$.each( chb1.find("label"),  function (intg) {			
//			if (!chb1.children().hasClass("disabled"))
//			{
//				console.log(chb1);
//				var dr = chb1.find("input");
//				$(this).children().append('<li data-lid="' + dr.attr("id") + '">'+ dr.text() +'</li>');
//			}
//		});	
//		fids[index] = 
//		console.log(chb1);
//		var elem = 
	//	fids[index] = $( this ).attr("data-filter");
//		console.log( index + ": " + $( this ).attr("data-filter") );
	});	*/
//}	
//	console.log(fids);
//	$.each( chb,  function (index) {
//		chb[index] = $( this ).attr("data-filter");
//		console.log( index + ": " + $( this ).text("") );
//	});
	$("body").on("click",".spoil_btn", function () {
		var box = $(this).prev();
		var span = $(this).find("span");
		var i = $(this).find("i");
		var b = $(this).find("b");
		var _this = $(this);
		box.slideToggle("300", function()
		{
			if ($(this).hasClass("closed"))
				{
					$(this).toggleClass("closed");
					i.removeClass("fa-chevron-down").addClass("fa-chevron-up");
					span.text("Закрыть");
					b.hide();
					
				}
				else
				{
					$(this).toggleClass("closed");
					i.removeClass("fa-chevron-up").addClass("fa-chevron-down");
					span.text("еще");
					b.show();					
				}
		});
/*		if (box.hasClass("closed"))
		{
			
		}*/
	});
/*	$("body").on("click","a.waSMMainCatOpen", function (e) {
		var id = $(this).attr("data-secid");
		$("#" + id + "_parent").click();
	});*/
/*	$("body").on("click",".waDropdown li", function () {

	$(this).toggleClass("waSMFActive");
	$(this).parents(".waSMFName").children().find("div").text("(" + $(this).text() + ")");
	$("#" + $(this).attr("data-lid")).click();
	});*/
	
	
	
	
	$("body").on("click",".waSMSubFilterMenu li a", function (e) {
	e.preventDefault();
/*	$(".waDropdown").removeClass("waDropdown");
	$(this).next().addClass("waDropdown");*/
		return false;
	});
});