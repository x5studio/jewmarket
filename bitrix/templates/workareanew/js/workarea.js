//BX.showWait();
/*(function(window){
BX.showWait = function(node, msg)
{
//	node = BX(node) || document.body || document.documentElement;
	node = BX('waJewmarket');
	msg = msg || BX.message('JS_CORE_LOADING');

	var container_id = node.id || Math.random();

	var obMsg = node.bxmsg = document.body.appendChild(BX.create('DIV', {
		props: {
			id: 'wait_' + container_id
		},
		style: {
			position: 'fixed',
			width: '100%',
			height: '100%',
			top: '-10px', 
			left: '0', 
			bottom: '0', 
			right: '0',
			background: 'rgba(0,0,0,.8)',
			zIndex: '10000',	
			padding: '0px',
			border: '0px',
			fontSize: '0px',
		},
		text: msg
	}));

//	setTimeout(BX.delegate(_adjustWait, node), 10);

//	lastWait[lastWait.length] = obMsg;
	return obMsg;
};

BX.closeWait = function(node, obMsg)
{
	node = BX('waJewmarket');
	if(node && !obMsg)
		obMsg = node.bxmsg;
	if(node && !obMsg && BX.hasClass(node, 'bx-core-waitwindow'))
		obMsg = node;
	if(node && !obMsg)
		obMsg = BX('wait_' + node.id);
	if(!obMsg)
		obMsg = lastWait.pop();

	if (obMsg && obMsg.parentNode)
	{
		for (var i=0,len=lastWait.length;i<len;i++)
		{
			if (obMsg == lastWait[i])
			{
				lastWait = BX.util.deleteFromArray(lastWait, i);
				break;
			}
		}

		obMsg.parentNode.removeChild(obMsg);
		if (node) node.bxmsg = null;
		BX.cleanNode(obMsg, true);
	}
};
})(window);*/
$(document).ready(function() {
	$("body").on("click",".spoil_btn", function () {
		var box = $(this).prev();
		var span = $(this).find("span");
		var i = $(this).find("i");
		var b = $(this).find("b");
		var _this = $(this);
		box.slideToggle("300", function()
		{
			if ($(this).hasClass("closed"))
				{
					$(this).toggleClass("closed");
					i.removeClass("fa-chevron-down").addClass("fa-chevron-up");
					span.text("Закрыть");
					b.hide();
					
				}
				else
				{
					$(this).toggleClass("closed");
					i.removeClass("fa-chevron-up").addClass("fa-chevron-down");
					span.text("еще");
					b.show();					
				}
		});
/*		if (box.hasClass("closed"))
		{
			
		}*/
	});	
	    var $menu = $(".sections_all");
        $(window).scroll(function(){
            if ( $(this).scrollTop() > 100 && $menu.hasClass("sec_default") ){
                $menu.removeClass("sec_default").addClass("sec_fixed");
            } else if($(this).scrollTop() <= 100 && $menu.hasClass("sec_fixed")) {
                $menu.removeClass("sec_fixed").addClass("sec_default");
            }
        });//scroll
    var $menu2 = $("nav");
        $(window).scroll(function(){
            if ( $(this).scrollTop() > 100 && $menu2.hasClass("sec_default") ){
                $menu2.removeClass("sec_default").addClass("sec_fixed2");
				$(".scrollout").hide();
				$("nav .bx-basket").removeClass("wahiddencart");
            } 
			else if($(this).scrollTop() <= 100 && $menu2.hasClass("sec_fixed2")) {
				$(".scrollout").show();
				$("nav .bx-basket").addClass("wahiddencart");
                $menu2.removeClass("sec_fixed2").addClass("sec_default");
            }
        });//scroll
	
 /*           var snapper = new Snap({
                element: document.getElementById('content1'),
                dragger: document.getElementById('do-drag'),
				disable: 'right'
            });*/
/*	  var slideout = new Slideout({
    'panel': document.getElementById('ppanel'),
    'menu': document.getElementById('menu'),
	"shadow": document.getElementById('waShadowLM'),
	"fix": document.getElementById('topNativeMenu'),
    'padding': 256,
    'tolerance': 170
  });*/
  
/*$("#waJewmarket .waNavIcoBlock").on("click", ".fa-bars", function () {
	slideout.toggle();
//		$(".topFix").toggleClass("waDN");
	});
$("#waJewmarket #topFixID").on("click", ".fa-bars", function () {
	slideout.toggle();
});*/
/*	$('#waMenuSlider').slick({
		speed: 300,
	  
	});*/
	$('#slider').slick({
		speed: 300,
//		adaptiveHeight: true,
		infinite: true,
		swipe: false
	});
	$('#waVertAction').slick({
	  dots: false,
	  infinite: true,
	  vertical: true,
//	  swipe: true,
//	  draggable: true,
//	  adaptiveHeight: true,
//	  verticalSwiping: true,
//	  speed: 300,
	  slidesToShow: 2,
	  slidesToScroll: 2,
	  swipe: false,
	  draggable: false
	});
	$('#waVertWinner').slick({
	  dots: false,
	  infinite: true,
	  vertical: true,
//	  adaptiveHeight: true,
//	  verticalSwiping: true,
//	  speed: 300,
	  slidesToShow: 2,
	  slidesToScroll: 2,
	  swipe: false,
	  draggable: false
	});
	
	$('#waCarousel').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 6,
	  slidesToScroll: 6,
	  swipe: true,
	  draggable: true,
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 5,
			slidesToScroll: 5,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4
		  }
		},
		{
		  breakpoint: 500,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	});
	$('.sizeSlider').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 8,
	  slidesToScroll: 3,
	  swipe: true,
	  draggable: true,
	  responsive: [	  
	  		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 6,
			slidesToScroll: 2,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 500,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		}
	  ]
	});
	$('#waCarousel2').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 6,
	  slidesToScroll: 6,
	  swipe: true,
	  draggable: true,
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 5,
			slidesToScroll: 5,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4
		  }
		},
		{
		  breakpoint: 500,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	});	
			$('#rowCarousel').show();
			$('#slider').show();
//			slideout.menu.style["visibility"] = "visible";
			/*slideout.open();*/
//			if (!!slideout.isOpen()) slideout.open();
//			$('#menu').show();
/*			$('#waVertAction').show();*/
/*	this.type = {
		waHover: "mouseover",
		waClick: "click"
	}*/
waMPFilter = {
/*	this.type = [];
	this.type[0] = "click";
	this.type[0] = "mouseover";*/
/*	ax: function (data)
	{

	},*/
	loadData: function (obj)
	{
		var nodeIdNum = addCode;		
		var nodeId = "waNode_" + nodeIdNum;
		this.waNode = "waNode_" + obj.ID + "_" + nodeIdNum;
//		var nodeId = this.waNodeId;
		var url = "/jewcat/MPFilter.php?ID=" + obj.ID + "&TYPE=" + obj.TYPE + "&waNode=" + this.waNode;
//		this.waLog(this.waNode);
//		console.log(url);
		BX.ajax.insertToNode(
		url,
		nodeId
		);
		
//		this.dataSH();
//	$(".jewRowHidden").show();
/*		this.dd = null;
//		this.waLog(e);
			BX.ajax.post (
				"/jewcat/MPFilter.php",
				obj,
				axResp
			)	
			function axResp(data)
			{
//				this.dd = data;
//				var d = data;
				//console.log(data);
				
				waMPFilter.fdata = data;
//				console.log(waMPFilter.fdata);
//				this.data.ID;
//				waMPFilter.filterData = data;
//				return data;
			}
		console.log(waMPFilter.fdata);
//		this.waLog(this.dd);*/
		return nodeId;
	},
	getParams: function (e)
	{
		
	},
	dataSH: function (e, obj, nodeResult)
	{
		/*
		После того, как в waNode была перемещена обработанная по камням информация следует создать
		дополнительную waNode_ с измененным идентификатором ( +1 прибавить) для того, чтобы 
		сохранить и закэшировать изображения и не перезагружать их заново. Просто в дисплей ноне
		*/
		$(".jewRowHidden").hide();
		$("#" + nodeResult).show();
		$(".jewElemFocus").removeClass("waRGB");
		e.addClass("waRGB");
	},
	execute:  function (e, type)
	{
		var id = e.attr("data-elem");
		if ( type == "mouseenter")
		{
			/* На потом - облегчение и информирование */
//			this.count++;
//			if (this.count > 2) // если наведений мышью больше чем 2
//			{
//				this.hovered[id] = this.count;
//			}
//			else
//			{
//				this.waLog(this.count); // выполнение тяжелых скриптов
//			}
			
		}
		if ( type == "click")
		{
			this.type = 1; // 1 -> грузим данные
			var obj = {
				ID : id,
				TYPE : this.type
			}
			var nodeResult = this.loadData(obj);
			this.SH = this.dataSH(e, obj, nodeResult);
			//var data = this.loadData(e);
		}
		
		
	},
	waLog:  function (e)
	{
		console.log(e);
	}
}
//waMPFilterInit();
//waMPFilter.hovered = []; 
$("#waJewmarket").on("mouseenter", ".jewElemFocus", function(e) {
//	var type = this.type.waHover;
//	var count = this.count;
//	this.count = this.count + 1;
//	var c = this.count;
//	waMPFilter.execute($(this), "mouseenter");
//	var id = $(this).attr("data-elem");
//	console.log(id);
});
$("#waJewmarket").on("click", ".jewElemFocus", function(e) {
	waMPFilter.execute($(this), "click");
});
waMPFilter.count = 0;
waMPFilter.hovered = [];
//waMPFilter.filterData = null;
});