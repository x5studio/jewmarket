<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$imgArr = array (
	0 => '/bitrix/templates/jew_market/components/bitrix/news.list/resize/images/',
	1 => '/bitrix/templates/jew_market/components/bitrix/news.list/resize/images/',
	2 => '/bitrix/templates/jew_market/components/bitrix/news.list/resize/images/'
);
$i = -1;
?>
<div class="resize_cont">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?$i++;?>
	<div class="resize_row">
		<div class="resize_img x500">
			<a class="a_pic2" href="" style="background-image: url(/bitrix/templates/jew_market/components/bitrix/news.list/resize/images/1280_1.jpg);"></a><br />
			<span class="">Несжатое изображение (контейнер 500х500)</span>
		</div>
		<div class="resize_img x500">
			<a class="a_pic2" href="" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"></a>
			<span class="">Сжатое изображение (контейнер 500х500, размер 500х500)</span>
		</div>
		<div class="resize_img x173">
			<a class="a_pic2" href="" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
			<span class="">Сжатое изображение (контейнер 173х173, размер 400х400)</span>
		</div>
	</div>
<?endforeach?>
</div>
