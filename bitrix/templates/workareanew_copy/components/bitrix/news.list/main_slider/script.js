$(document).ready(function() {
    $('.panel_big').slick({
	  autoplay: true,
        dots: true,
        infinite: true,
        speed: 800,
		autoplaySpeed: 8000,
        slidesToShow: 1,
        slidesToScroll: 1,
		cssEase: 'ease-in-out',
		arrows: false
    });
});	
