<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$i = -1;?>
<!-- Перебор элементов из компонента новостей -->
<?
	
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$i++; 
	$arMap[$i] = $arItem["ID"];
	?>
<?endforeach;?>
<style>
	svg {
    position: absolute;
    left: 50%;
    margin-left: -423px;
    top: 56px; }
	section > div {
    background: url('images/map_fon.png') center center no-repeat; 
	background-size: 100%;
	height:500px; }
</style>

<section>
<div style="width:980px;margin:0 auto;">

<svg version="1.1" id="msvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="820px" height="340px" viewBox="0 0 595.3 283.5" style="enable-background:new 0 0 595.3 283.5;" xml:space="preserve"
	>
<rect id="<?=$arMap[0]?>" x="39.2" y="30.6" class="st0 tnode" width="16.9" height="28.5"/>
<rect id="<?=$arMap[1]?>"  x="21.8" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[2]?>"  x="74.2" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[3]?>"  x="56.8" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[4]?>" x="109.4" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[5]?>" x="91.9" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[6]?>" x="144.4" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[7]?>" x="126.9" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[8]?>" x="179.3" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[9]?>" x="161.8" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[10]?>" x="214.3" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[11]?>" x="196.8" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[12]?>" x="21.8" y="70.4" class="st0" width="20.6" height="27.9"/>
<rect id="<?=$arMap[13]?>" x="21.8" y="98.8" class="st0" width="20.6" height="28.4"/>
<rect id="<?=$arMap[14]?>" x="21.8" y="138.2" class="st0" width="20.6" height="27.8"/>
<rect id="<?=$arMap[15]?>" x="21.8" y="166.9" class="st0" width="20.6" height="28.3"/>
<polygon class="st0" points="42.3,240.1 21.8,238.2 21.8,204.6 42.3,204.6 "/>
<rect id="<?=$arMap[16]?>" x="78.2" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[17]?>" x="60.8" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[18]?>" x="113.4" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[19]?>" x="95.9" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[20]?>" x="148.4" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[21]?>" x="130.9" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[22]?>" x="183.3" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[23]?>" x="165.8" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[24]?>" x="218.3" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[25]?>" x="200.8" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[26]?>" x="78" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[27]?>" x="60.6" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[28]?>" x="113.2" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[29]?>" x="95.8" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[30]?>" x="148.2" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[31]?>" x="130.8" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[32]?>" x="183.1" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[33]?>" x="165.7" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[34]?>" x="218.1" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[35]?>" x="200.7" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[36]?>" x="278.5" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[37]?>" x="261.1" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[38]?>" x="313.5" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[39]?>" x="296.1" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[40]?>" x="348.7" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[41]?>" x="331.3" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[42]?>" x="383.7" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[43]?>" x="366.3" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[44]?>" x="407" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[45]?>" x="442" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[46]?>" x="424.5" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[47]?>" x="278.3" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[48]?>" x="260.9" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[49]?>" x="313.3" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[50]?>" x="295.9" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[51]?>" x="348.5" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[52]?>" x="331.1" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[53]?>" x="383.5" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[54]?>" x="366.1" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[55]?>" x="406.8" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[56]?>" x="441.8" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[57]?>" x="424.4" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[58]?>" x="459.9" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[59]?>" x="494.9" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[60]?>" x="477.5" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[61]?>" x="459.7" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[62]?>" x="494.7" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[63]?>" x="512.6" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[64]?>" x="512.4" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[65]?>" x="530" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[66]?>" x="529.9" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[67]?>" x="477.3" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[68]?>" x="279" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[69]?>" x="261.5" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[70]?>" x="314" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[71]?>" x="296.5" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[72]?>" x="331.7" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[73]?>" x="278.2" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[74]?>" x="260.7" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[75]?>" x="313.2" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[76]?>" x="295.7" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[77]?>" x="330.9" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[78]?>" x="279.1" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[79]?>" x="314.1" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[80]?>" x="296.7" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[81]?>" x="331.9" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[82]?>" x="236" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[83]?>" x="235.2" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[84]?>" x="236.4" y="70.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[85]?>" x="235.6" y="98.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[86]?>" x="236.4" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[87]?>" x="261.5" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[88]?>" x="43" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[89]?>" x="78" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[90]?>" x="60.6" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[91]?>" x="113.2" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[92]?>" x="95.8" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[93]?>" x="148.2" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[94]?>" x="130.8" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[95]?>" x="183.1" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[96]?>" x="165.7" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[97]?>" x="218.1" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[98]?>" x="200.7" y="138.1" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[99]?>" x="42.8" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[100]?>" x="77.8" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[101]?>" x="60.4" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[102]?>" x="113" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[103]?>" x="95.6" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[104]?>" x="148" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[105]?>" x="130.6" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[106]?>" x="182.9" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[107]?>" x="165.5" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[108]?>" x="217.9" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[109]?>" x="200.5" y="166.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[110]?>" x="42.8" y="204.6" class="st0" width="16.9" height="34.3"/>
<rect id="<?=$arMap[111]?>" x="77.8" y="204.6" class="st0" width="16.9" height="36.8"/>
<rect id="<?=$arMap[112]?>" x="60.4" y="204.6" class="st0" width="16.9" height="34.3"/>
<rect id="<?=$arMap[113]?>" x="113" y="204.6" class="st0" width="16.9" height="44.3"/>
<rect id="<?=$arMap[114]?>" x="95.6" y="204.6" class="st0" width="16.9" height="39.8"/>
<rect id="<?=$arMap[115]?>" x="148" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[116]?>" x="130.6" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[117]?>" x="182.9" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[118]?>" x="165.5" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[119]?>" x="217.9" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[120]?>" x="200.5" y="204.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[121]?>" x="249.5" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[122]?>" x="232" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[123]?>" x="284.5" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[124]?>" x="267" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[125]?>" x="319.7" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[126]?>" x="302.2" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[127]?>" x="354.7" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[128]?>" x="337.2" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[129]?>" x="389.9" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[130]?>" x="372.4" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[131]?>" x="424.9" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[132]?>" x="407.4" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[133]?>" x="459.8" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[134]?>" x="442.3" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[135]?>" x="494.8" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[136]?>" x="477.3" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[137]?>" x="530" y="30.6" class="st0" width="16.9" height="28.5"/>
<rect id="<?=$arMap[138]?>" x="512.5" y="30.6" class="st0" width="16.9" height="28.5"/>
<polygon class="st0" points="560.3,58.1 547.5,59.1 547.5,30.6 573,30.6 "/>
</svg>


</div>
</section>  
<div id="svgWindow" class="window">
	<div class="block">
		<form id="svgForm" action="">
			<!-- Выаод свойств для просмотра -->
<!--			<div id="winPrice" class="param">Цена : <span></span></div>
			<div id="winM2" class="param">Полщадь : <span></span></div> -->
			<!-- Вывод активных полей для редактирования -->
			<input id="winEmail" type="text" name="email" value="" placeholder="E-mail">
			<input id="winFIO" type="text" name="fio" value="" placeholder="ФИО">
			<input id="winPhone" type="text" name="phone" value="" placeholder="Телефон">
			
				<!-- Невидимые поля с передаваемыми параметрами -->
				<input id="winELID" type="hidden" name="ELID" value="">
				<input id="winName" type="hidden" name="name" value="">
<!--			<input id="winIBID" type="hidden" name="IBID" value="32"> -->
			<!-- Кнопка для передачи данных через ajax -->
			<div id="winSubmit" class="submit">Отправить</div>
		</form>
		
		<!-- Крестик закрытия -->
		<div class="cross"></div>
		
		<!--Выводимое сообщение после отправки -->
		<div class="success">Сообщение успешно отправлено</div>
	</div>
</div>
<script>
/*$("body").on("click","svg rect", function () {
	var id = $(this).attr("id");
	if (id)
	{
		var win = $('#svgWindow');	
		win.fadeIn(300, function () {
			var winForm = $(this).find("#svgForm");
			winForm.find("#winELID").attr("value", id);
			
		});
	}
});*/
$("body").on("click","svg rect", function () {
	var id = $(this).attr("id");
	if (id)
	{
		var win = $('#svgWindow');	
		var obj = { elid: id};
		win.fadeIn(300, function () {
			BX.ajax.loadJSON (
			"/form/show.php",
			obj,
			response
			)
			function response(data){
				var form = $('#svgForm');
				form.find("#winELID").attr("value", data.MAIN["ID"]);
				form.find("#winName").attr("value", data.MAIN["NAME"]);
				form.prepend('<div class="param price">'+ data.PROPS.COST_LAND.NAME +' : <span>' + data.PROPS.COST_LAND.VALUE +'</span></div>');
				form.prepend('<div class="param">'+ data.PROPS.AREA_LAND.NAME +' : <span>' + data.PROPS.AREA_LAND.VALUE +'</span></div>');
				form.prepend('<div class="param">'+ data.PROPS.NUMB_LAND.NAME +' : <span>' + data.PROPS.NUMB_LAND.VALUE +'</span></div>');
				form.prepend('<div class="param">'+ data.PROPS.STATUS_LAND.NAME +' : <span>' + data.PROPS.STATUS_LAND.VALUE +'</span></div>');
				form.prepend('<div class="param">'+ data.PROPS.STREET_LAND.NAME +' : <span>' + data.PROPS.STREET_LAND.VALUE +'</span></div>');
				

			}
/*			var winForm = $(this).find("#svgForm");
			winForm.find("#winELID").attr("value", id);*/
			
		});
	}
});
$("body #svgWindow").on("click","#winSubmit", function () {
	var win = $("#svgWindow");
	var obj = $(this).parent().serialize();
	BX.ajax.post (
		"/form/add.php",
		obj,
		response
		)
		function response(data){

//				$("#svgWindow").fadeOut(300);
				}		
});
	$("body #svgWindow").on("click","#winSubmit", function () {
		var obj = $(this).parents("#svgForm").serialize();
		var win = $(this).parents("#svgWindow");
			BX.ajax.post (
				"/form/add.php",
				obj,
				response
			)
			function response(data){
//				console.log(data)
/*				$('#svgWindow .success').prev().hide();
				$('#svgWindow .success').show();*/
				$("#svgWindow").fadeOut(300);
				$('#svgWindow #svgForm').find("div").detach();
				
				}	
	});
	$(document).keydown( function (esc) {
     if( esc.keyCode === 27 ) {
        $("#svgWindow").fadeOut(300, function () {
			$(this).find("input").attr("value","");
			$(this).find("textarea").text(" ");			
		});
        }
        });
</script>