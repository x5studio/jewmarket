<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?echo SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta http-equiv="cleartype" content="on">
    <meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<?$APPLICATION->ShowHead()?>

	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHeadStrings();?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/jquery-1.9.1.min.js');?>
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/custom-theme/jquery.ui.1.10.3.ie.css">
    <![endif]-->
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/jquery-migrate-1.2.1.min.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/bootstrap.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/jquery-ui-1.10.3.custom.min.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/holder.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/google-code-prettify/prettify.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/docs.js"');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slideout_def.js');?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]> 
	<?
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/html5shiv.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/ui/assets/js/vendor/respond.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/workarea.js');
	 ?>
    <![endif]-->
    <!-- Le styles -->
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/ui/assets/css/bootstrap.min.css');?>
	<?$APPLICATION->SetAdditionalCSS('/bitrix/css/main/font-awesome.css');?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick.css');?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick-theme.css');?>
	<script>
	  var slideout = new Slideout({
    'panel': document.getElementById('ppanel'),
    'menu': document.getElementById('menu'),
	"shadow": document.getElementById('waShadowLM'),
	"fix": document.getElementById('topNativeMenu'),
    'padding': 256,
    'tolerance': 70
  });
	</script>
</head>
	<?$APPLICATION->ShowPanel();?>
<body id="waJewmarket">
<header class="hidden-sm hidden-xs">
	<div class="waFluid">
		<div class="container">
			<div class="row" style="background: transparent;">
				<div class="col-md-8 info" style="padding-left: 0px; padding-right: 0px;">
					<div>
					Работаем на выезде! Выбирайте и покупайте не выходя из дома.
					</div>
					<a href="">Как это работает</a>
				</div>
				<div class="col-md-4 cart" style="padding-left: 0px; padding-right: 0px;">
				
<?
if (CModule::IncludeModule('sale')){
$cart_count = CSaleBasket::GetList(false, array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL"), array(), false, array("ID"));
}
?>
					<div class="text">В корзине <a href=""><?=$cart_count?></a> товаров</div>
					<a href="/personal/cart/" class="buttonOrange">Корзина</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row" style="margin-top: 10px; margin-bottom: 10px">
			<div class="col-xs-4 waClearPadding">
				<a href="/" class="logo">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" class="img-responsive"/>
				</a>
			</div>
			<div class="col-xs-4 waClearPadding">
				<menu class="row topMenu">
					<div class="col-xs-6">
						<li><a href="/catalog/jewelry/?set_filter=y&amp;arrFilter_135_4261170317=Y">Новинки</a></li>
						<li><a class="/delivery">Оплата и доставка</a></li>	
						<li><a href="/catalog/jewelry/?set_filter=y&amp;arrFilter_132_4261170317=Y">Акции</a></li>
					</div>
					<div class="col-xs-6">
						<li><a href="/warranty/">Гарантии и качество</a></li>
						<li><a href="/wholesale/">Оптовые продажи</a></li>
						<li><a href="/contacts/">Контакты</a></li>	
					</div>
				</menu>
			</div>
			<div class="col-xs-4 rightBlock waClearPadding">
				<div class="phone">
					+7 (812) 339 25 37
				</div>
				<a class="callBack">Заказать обратный звонок</a>
				<div class="authPanel">
<?
	$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"FORGOT_PASSWORD_URL" => "",
		"USE_BACKURL" => "/about/index.php",
		"SUCCESS_PAGE" => "/about/index.php",
		"PROFILE_URL" => "/personal/",
		"REGISTER_URL" => "/catalog/",
		"SHOW_ERRORS" => "Y"
	),
	false
	);?>
<!-- 					<form class="ifreg">
						<p>Вы вошли как:&nbsp;
						<a href="/personal/" title="Мой профиль">
							<i class="fa fa-user fa-2"></i> 
							<span class="header_auth_login" title="admin">Покупатель</span></a>
						</p>
						<p>
							<a href="/personal/" class="header_auth_partner" title="Мой профиль">Личный кабинет</a>
								<span class="exit"><i class="header_red fa fa-power-off"></i>
									<input type="submit" class="header_auth_partner" name="logout_butt" value="Выйти">
								</span>
						</p>
					</form> -->
				</div>
			</div>
		</div>
	</div>
	<div class="waFluid" style="background: #fff">
		<div class="container">	
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-xs-2 waClearPadding">
<?
$APPLICATION->IncludeComponent(
	"jewmarket:search.title", 
	"visual", 
	array(
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0_iblock_catalog" => array(
			0 => "29",
		),
		"CHECK_DATES" => "N",
		"CONTAINER_ID" => "title-search",
		"CONVERT_CURRENCY" => "N",
		"INPUT_ID" => "title-search-input",
		"NUM_CATEGORIES" => "1",
		"ORDER" => "date",
		"PAGE" => "#SITE_DIR#search/index.php",
		"PREVIEW_HEIGHT" => "75",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PREVIEW_WIDTH" => "75",
		"PRICE_CODE" => array(
			0 => "ACTUAL",
		),
		"PRICE_VAT_INCLUDE" => "N",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"SHOW_PREVIEW" => "Y",
		"TOP_COUNT" => "5",
		"USE_LANGUAGE_GUESS" => "Y",
		"COMPONENT_TEMPLATE" => "visual"
	),
	false
);
?>
<!--					<div id="title-search" class="bx-searchtitle">
						<form action="/search/index.php">
							<div class="bx-input-group">
								<input id="title-search-input" type="text" name="q" value="" placeholder="Поиск..." autocomplete="off" class="bx-form-control">
								<span class="bx-input-group-btn">
									<button class="btn btn-default" type="submit" name="s"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
					</div> -->
				</div>
				<div class="col-xs-10 waClearPadding">
					<menu class="navMenu">
						<li class="scrollout"><a href="/about/">О JEW MARKET</a></li>
						<li><a href="/catalog/jewelry/">ЮВЕЛИРНЫЕ УКРАШЕНИЯ</a></li>
						<li><a href="/gift/">ПОДОБРАТЬ ПОДАРОК</a></li>
						<li class="scrollout"><a href="/partners/">ПАРТНЕРАМ</a></li>
						<li class="scrollout"><a href="/wherebuy/">ГДЕ КУПИТЬ</a></li>
					</menu>
				</div>
			</div>	
		</div>
	</div>
<?
$curDir = explode('/',$APPLICATION->GetCurDir());
$bread[0]["PATH"] = $curDir[0].'/';
$bread[0]["NAME"] = "Главная страница";
$bread[1]["PATH"] = $bread[0]["PATH"].'catalog/jewelry/';
$bread[1]["NAME"] = "Ювелирный каталог";
$prefix = "/catalog/jewelry/";
$arWASecTitle = array (
	"Тип изделия",
	"Технология",
	"Вставка",
	"&nbsp;"
);
$sLvlCount = 4;
$arWASecFilter = array(
	"/?set_filter=y&arrFilter_163_3653329462=Y",
	"/?set_filter=y&arrFilter_163_1233026983=Y",
	"/?set_filter=y&arrFilter_163_1048137521=Y",
	"/?set_filter=y&arrFilter_163_1589561044=Y",
);
if (CModule::IncludeModule('iblock')){
$curType = $curDir[3];
$curSubType = $curDir[4];
	$firstSec = array();
		$Section = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 2, "IBLOCK_ID" => 29, "ACTIVE" => "Y"), array("ID","NAME","CODE"));
	$i = -1; $j = -1;
   while ($arSect = $Section->GetNext())
   {
	   $i++;
	   $j = -1;
	   if (!empty($curType) && $curType == $arSect["CODE"])
		{
			$bread[2]["NAME"] = $arSect["NAME"];
			$bread[2]["PATH"] = $bread[1]["PATH"]."".$arSect["CODE"]."/";
		}
        $firstSec[$i]["NAME"] = $arSect["NAME"];
		$firstSec[$i]["ID"] = $arSect["ID"];
		$firstSec[$i]["CODE"] = $arSect["CODE"];
				$Sec1 = CIBlockSection::GetList(array("SORT"=>"ASC"),array("DEPTH_LEVEL" => 3, "SECTION_ID" => $arSect["ID"]), array("ID","NAME","CODE"));
			   while ($arSect1 = $Sec1->GetNext())
			   {	
					$j++;
				   if (!empty($curSubType) && $curSubType == $arSect1["CODE"])
					{
						$bread[3]["NAME"] = $arSect1["NAME"];
						$bread[3]["PATH"] = $bread[2]["PATH"]."".$arSect1["CODE"]."/";
					}
					$arAllSecs[] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["NAME"] = $arSect1["NAME"];
					$firstSec[$i]["SUB"][$j]["ID"] = $arSect1["ID"];
					$firstSec[$i]["SUB"][$j]["CODE"] = $arSect1["CODE"];
			   }		   
   }
}
?>
<div class="waFluid waCatSections">
	<div class="container">	
		<div class="row" style="background: transparent">
			<div class="col-xs-12 waClearPadding">
				<menu class="firstLvl">	
					<?foreach($firstSec as $value):?>
					<?$insert_fil++;?>
						<li>
							<a href="/catalog/jewelry/<?=$value["CODE"]?>/"><?=$value["NAME"]?></a>
							<?if (!empty($value["SUB"])):?>
								<menu class="secondLvl">
									<li class="link">
										<a href="<?=$prefix . $value["CODE"]?>/">Смотреть все ></a>
									</li>
									<?$c = 0;?>
									<?foreach($arWASecTitle as $k => $v):?>
									<?$c++;?>
										<li>
											<span class="title"><?=$v?></span>
											<ul>
											<?if ($c == 1):?>
												<?foreach ($value["SUB"] as $item):?>
													<li><a href="<?=$prefix . $value["CODE"]?>/<?=$item["CODE"]?>/" rel="canonical"><? echo $item["NAME"]?></a></li>
												<?endforeach?>												
											<?endif?>
											<?if ($c == 2):?>
												<li><a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
												<li><a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_1233026983=Y">Алмазная грань</a></li>
												<li><a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_1048137521=Y">Родирование</a></li>
												<li><a href="/catalog/jewelry/<?=$value["CODE"]?>/?set_filter=y&arrFilter_163_1589561044=Y">Эмаль</a></li>
											<?endif?>
											</ul>
											<?if ($c == 4):?>	
												<img src="" />
											<?endif?>
										</li>
									<?endforeach?>
								</menu>
							<?endif?>
						</li>
					<?endforeach?>
				</menu>
			</div>
		</div>
	</div>
</div>
<!--<div class="waFluid waCatSections">
	<div class="container">	
		<div class="row" style="background: transparent">
			<div class="col-xs-12 waClearPadding">
				<menu class="firstLvl">
					<li>
						<a href="">Кольца</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Печатки</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Серьги</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Пуссеты</a></li>
									<li><a href="">Каффы</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Подвески</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Шармы</a></li>
									<li><a href="">На браслет и цепь</a></li>
									<li><a href="">Знаки зодиака</a></li>	
									<li><a href="">Кресты</a></li>

								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Браслеты</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Браслет для шармов-кожаный</a></li>
									<li><a href="">Браслет для шармов-цепь</a></li>
									<li><a href="">Жесткие</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Цепи</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Ромб</a></li>
									<li><a href="">Корда</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Брошь, булавка</a>
					</li>
					<li>
						<a href="">Сувениры</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Ложка загребушка</a></li>
									<li><a href="">Монетка</a></li>
									<li><a href="">Ложка на крестины</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Технология</span>
								<ul>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_3653329462=Y">Покрытие серебром 999 пробы</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1233026983=Y">Алмазная грань</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1048137521=Y">Родирование</a></li>
									<li><a href="/catalog/jewelry/sergi/?set_filter=y&amp;arrFilter_163_1589561044=Y">Эмаль</a></li>
								</ul>
							</li>
							<li>
								<span class="title">Вставка</span>
								<ul>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
					<li>
						<a href="">Оборудование CARAMEL</a>
						<menu class="secondLvl">
							<li class="link"><a href="">Смотреть все ></a></li>
							<li>
								<span class="title">Тип изделия</span>
								<ul>
									<li><a href="">Подставки</a></li>
								</ul>
							</li>
							<li>
								<img src="" />
							</li>							
						</menu>
					</li>
				</menu>
			</div>
		</div>
	</div>
</div> -->
</header>
<div class="container" id="topNativeMenu" style="display: none;">
	<div class="row visible-xs headMobile">
		<div class="col-xs-12 clearRowPadd">
				<div class="mobileMenu">
					<a href="" class="logoMobile">Jewmarket</a>
						<div class="waNavIcoBlock" id="waNavIcoBlockID">				
								<i class="fa fa-bars" aria-hidden="true"></i>		
						</div>
				</div>
		</div>
	</div>
	<div class="row hidden-xs">
		
	</div>
</div>
<nav id="menu" class="waSMenu">
	<div class="topFix" id="topFixID">
		<form>
				<input type="text" name="waSearchText" placeholder="Поиск">
				<i class="fa fa-search" aria-hidden="true"></i>
				<input type="submit" value="">
		</form>
		<div class="cancel">Отменить</div>
		<div class="waNavIco">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</div>
	</div>
		<div class="club">
		<table>
			<tr>
				<td rowspan="2">
					<div class="uPhoto">
						<i class="fa fa-user-plus" aria-hidden="true"></i>
						<img src="" />
					</div>				
				</td>
				<td>
					<div class="uName"><a href="">Безымянный <br /> <small>редактировать ...</small></a></div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="uLogin">
						<a href="" class="vkontakte bx-authform-social-icon"></a>
						<a href="" class="facebook bx-authform-social-icon"></a>
						<a href="" class="twitter bx-authform-social-icon"></a>
						<a href="" class="plus bx-authform-social-icon" style="">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<span>1</span>
						</a>
						<!-- Личный кабинет <span>Сообщение : 1</span> -->
					</div>				
				</td>
			</tr>
		</table>
		</div>
		<menu class="catalog">
				<li><a href="">Кольца</a></li>
				<li><a href="">Серьги</a><span>new</span></li>
				<li><a href="">Подвески</a></li>
				<li><a href="">Браслеты</a><span>Розыгрыш</span></li>
				<li><a href="">Цепи</a></li>
				<li><a href="">Сувениры</a></li>
		</menu>
</nav>
<div id="ppanel" class="container">
	<div id="waShadowLM" class="waSMShadow"></div>