'use strict';

class WAToolsApp
{
	constructor (module)
	{
		this.module = module;
		this.app = "WATools";
		this.toolBar = null;
		this.toolWindow = {};
		this.createToolBar();
		this.createWindow();
		this.bindModule();
  	}
	createToolBar ()
	{
		let ToolBarId = document.getElementById("WATools"),
		toolBar = null;
		if (!ToolBarId)
		{
			toolBar = document.createElement('div');	
				toolBar.className = this.app;
				toolBar.id = this.app;	
			this.toolBar = document.body.appendChild(toolBar);		
		}
		else
		{
			this.toolBar = ToolBarId;
		}
		toolBar = document.createElement('div');	
			toolBar.className = "elem " + this.module.name;
			toolBar.id = this.app + "_" + this.module.name;	
		this.toolBar.appendChild(toolBar);	
		
	}
	createWindow ()
	{
		BX.delegate(
			this.toolWindow = new BX.CDialog({
				title: this.module.title,
				head: this.module.head,
				content: "",
				icon: 'head-block',
				resizable: true,
				draggable: true,
				height: '300',
				width: '640',
			}),this);	
//		BX.delegate(this.toolWindow,this);
	}
	bindModule (events)
	{
		BX.bindDelegate(
			  document.body, 'click', {className: this.module.name },
				 BX.proxy(function(e){
					if(!e)
					   e = window.event;
					this.toolWindow.Show();
					return BX.PreventDefault(e);
				 }, this)
		   );	
		switch (this.module.name) {
			case "APIHelper":
/*				BX.addCustomEvent('onAjaxSuccess', function(){
					WAToolsMenu

				});*/			
			break;
			default:				
			break;
		}
	}
}
class WAConsole extends WAToolsApp
{
	constructor ()
	{
		const text = {
			name : "console",
			title : "PHP консоль",
			head : "PHP DATA"
		}
		super(text);
		this.exec();
	}
	exec ()
	{
		let pre = document.querySelectorAll("pre"),
			cont = [],
			html = "",
			elem = null;
		cont["title"] = '<div class="WATools_title">';
		cont["open"] = '<pre class="waPre">';
		cont["close"] = '</pre>';
		for (var i=0; i < pre.length; i++)
		{
			elem = BX(pre[i]);

				if (elem.previousElementSibling)
				{
					BX.addClass(elem.previousElementSibling,"hidden_pre");
					html += cont["title"] + "Название: " + elem.previousElementSibling.innerHTML + "</div>";
				}
				if (elem.parentElement.className)
				{
					html += cont["title"] + "Класс: " + elem.parentElement.className + "</div>";
				}
				BX.addClass(elem, "hidden_pre");
				html += cont["open"];
				html += pre[i].innerHTML;
				html += cont["close"];
		}
		this.toolWindow.SetContent(html);
	}
}

class WAPIHelper extends WAToolsApp
{
	constructor ()
	{
		const text = {
			name : "APIHelper",
			title : "API Bitrix",
			head : "Меню"
		}
		super(text);
		console.dir(this.toolWindow);
		this.exec();
	}
	exec ()
	{
		let link = '/local/workarea/ajax.php';
		BX.ajax.insertToNode(
		link,
		this.toolWindow.PARTS.CONTENT_DATA
		);
		
	}	
}
BX.ready(function(){
var wa_console = new WAConsole();
var wa_APIHelper = new WAPIHelper();
console.dir(wa_APIHelper);
});